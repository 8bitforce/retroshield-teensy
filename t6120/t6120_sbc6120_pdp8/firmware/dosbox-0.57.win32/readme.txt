0) Do a virus scan on your computer to make sure you are happy.
1) Review/modify SBC6120 fw (BTS6120.PLX) in 05_sbc6120_271\
2) click on 05_sbc6120_271.bat to start environment for sbc6120 fw v271.
3) dosbox window will pop up.
4) type BUILD [ENTER] to kick off the build process.
5) once done, you can convert ROM271XH.BIN and ROM271XL.bin to C array form
   and paste it into the arduino/teensy code. See arduino/teensy code for
   how to do it using online converter.
