SBC6120 Utilities:

  These three Windows Command Prompt utilities make quick work out of copying
  SBC6120-style OS/8 partitions, located on any type of ATA compatible drive,
  to and from files on any Windows system.  The "InstallOS8.exe" utility even
  includes the entire, bootable two megabyte PDP-8 OS/8 operating system, all
  set and ready for the SBC6120.

  Each utility was written in 32-bit Intel assembly language since that's my
  current programming language of choice for Windows work.  The WinToATA and
  AtaToWin utilities are 13k and 12k bytes each, respectively.

  The utility's names should be clear and each utility fully explains itself.
  Just launch any utility program in a Windows "DOS box" and you'll find all
  the help and instructions you'll require.

  These utilities are FREEWARE, written and placed into the PUBLIC DOMAIN by
  Steve Gibson (grc.com) for the benefit of all SBC6120 users.  (11/16/2009)



Image files from Steve Gibson, located at his website:

https://www.grc.com/pdp-8/os8utils-sbc.htm


OS8 disk image was obtained from installos8 by SteveG.
=========================================================================
 Copyright:  This program contains a built-in image of Digital Equipment
             Corporation's PDP-8 OS/8 operating system. Although OS/8 is,
             and remains, the copyrighted property of DEC/Compaq/HP, DEC
             has previously granted permission for its use with Bob Supnik's
             PDP-8 emulator, from which this image was indirectly derived.
             It is my firm and evidence-based belief that the current
             copyright holder would have no objection to this re-purposing
             of this OS/8 operating system image for non-commercial use.

 FREEWARE by Steve Gibson (grc.com) to benefit the PDP-8 / SBC6120 community.



OS8 disk image was obtained from installos8 by SteveG. This is the help.
=========================================================================
InstallOS8 - for Bob Armstrong's SBC6120 PDP-8 Single Board Computer. (ver 1.1) 

 Usage:      InstallOS8 pppp     : Installs a bootable instance of the PDP-8
                                 : OS/8 operating system to the PREFORMATTED
                                 : SBC6120 partition.  For safety, pppp MUST
                                 : be exactly FOUR octal digits 0-7, and the
                                 : target partition (usually 0000) MUST HAVE
                                 : been preformatted with Bob's "DF" command.

             InstallOS8 help     : Outputs detailed instructions.Set console
                                 : to many lines, or redirect output to text
                                 : file for reading and/or printing.

 Operation:  Installs a bootable instance of the PDP-8 OS/8 operating system
             onto any "partition" of any type of physical drive attached, by
             any means - IDE, SATA, USB, Firewire, etc.- to a Windows system
             (NT & later). Partition in this context refers to Bob's meaning
             of the way his BIOS "partitions" large ATA drives into separate
             1 megaword (12-bit words) blocks of 4096, 256-word OS/8 blocks.

             Since SBC6120 and OS/8 compatible ATA drives will not contain a
             Windows-style filesystem, this utility operates at the physical
             level below the file system. This, in turn, means that attached
             drives will not have a "drive letter" assigned by the operating
             system. To facilitate recognizing the special SBC6120 drive and
             to prevent inadvertently installing OS/8 onto a Window's drive,
             YOU MUST FIRST FORMAT the drive's target partition by using the
             SBC6210's "DF" (drive format) command. This will write a highly
             specific recognition pattern to the indicated target partition,
             which will allow this utility to uniquely identify the physical
             drive unit and partition ... to which the built-in copy of OS/8
             will be written.

             Step 1: Attach the ATA drive to the SBC6120 and boot the SBC.
                     The drive should be acknowledged with its make/model
                     as the SBC6120 BIOS completes its boot.

             Step 2: Issue the command:  DF 0000
                     Confirm that you wish to format the device partition.
                     You can also format any other valid partition(s) to
                     prepare them for receipt of copies of OS/8.

             Step 3: Attach the ATA drive - by any means - to Windows and
                     wait a moment for the system to recognize the drive's
                     attachment. (No drive letter will be created, but the
                     "Safely Remove Hardware" icon should appear in the
                     Windows system tray.)  You should also be able to see
                     the attached drive under Windows Drive Management:
                     Right-click "My Computer", choose "Manage" then select
                     "Disk Management".  If a "Initialize and Convert Disk
                     Wizard appears, just cancel Windows helpfulness.

             Step 4: Issue the command:  InstallOS8 0000
                     The physical drive will be located and the indicated
                     "partition" will be verified using the formatting from
                     step 2 above, once this has been confirmed, OS/8 will
                     be decompressed from within this executable and written
                     to the indicated physical "partition" of the ATA drive.

             Step 5: Since Windows may "write cache" the writing, be sure to
                     wait until all drive writing activity has stopped. You
                     should "Eject" the drive by left-clicking on the "Safely
                     Remove Hardware" icon and choosing to eject the drive.

             Step 6: That's all there is to it.  Reattach the drive to Bob's
                     SBC6120 board, fire it up, and use the "boot" command
                     "b" to boot the OS/8 operating system in partition 0.

 Copyright:  This program contains a built-in image of Digital Equipment
             Corporation's PDP-8 OS/8 operating system. Although OS/8 is,
             and remains, the copyrighted property of DEC/Compaq/HP, DEC
             has previously granted permission for its use with Bob Supnik's
             PDP-8 emulator, from which this image was indirectly derived.
             It is my firm and evidence-based belief that the current
             copyright holder would have no objection to this re-purposing
             of this OS/8 operating system image for non-commercial use.

 FREEWARE by Steve Gibson (grc.com) to benefit the PDP-8 / SBC6120 community.
