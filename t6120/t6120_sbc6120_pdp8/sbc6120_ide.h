#ifndef _IDE_H
#define _IDE_H

////////////////////////////////////////////////////////////////////
// Emulate IDE in the most minimalist way.
//
// A big mess of functions to emulate 8255 on SBC6120 and how the IDE works.
// Sorry :(
//
// TODO: there must be a better way to support hdd for PDP-8/6120.
//       study hdd driver code for sbc6120 monitor.
//
////////////////////////////////////////////////////////////////////

//6633                   ;   In the SBC6120, the IDE interface is implemented via a standard 8255 PPI,
//6634                  ; which gives us 24 bits of general purpose parallel I/O.  Port A is connected
//6635                  ; the high byte (DD8..DD15) of the IDE data bus and port B is connected to
//6636                  ; the low byte (DD0..DD7).  Port C supplies the IDE control signals as follow:
//6637                  ;
//6638                  ;   C0..C2 -> DA0 .. 2 (i.e. device address select)
//6639                  ;   C.3*   -> DIOR L (I/O read)
//6640                  ;   C.4*   -> DIOW L (I/O write)
//6641                  ;   C.5*   -> RESET L
//6642                  ;   C.6*   -> CS1Fx L (chip select for the 1Fx register space)
//6643                  ;   C.7*   -> CS3Fx L ( "     "     " "  3Fx    "  "   )
//6644                  ;
//6645                  ; * These active low signals (CS1Fx, CS3Fx, DIOR, and DIOW) are inverted in
//6646                  ; the hardware so that writing a 1 bit to the register asserts the signal.

//6648                   ;   One nice feature of the 8255 is that it allows bits in port C to be
//6649                  ; individually set or reset simply by writing the correct command word to the
//6650                  ; control register - it's not necessary to read the port, do an AND or OR,
//6651                  ; and write it back.  We can use this feature to easily toggle the DIOR and
//6652                  ; DIOW lines with a single PWCR IOT.
//6653             0222 IDEINP=222  ; set ports A and B as inputs, C as output
//6654             0200 IDEOUT=200  ; set ports A and B (and C too) as outputs
//6655             0007 SETDRD=007  ; assert DIOR L (PC.3) in the IDE interface
//6656             0006 CLRDRD=006  ; clear    "  "   "    "    "  "   "  "
//6657             0011 SETDWR=011  ; assert DIOW L (PC.4) in the IDE interface
//6658             0010 CLRDWR=010  ; clear    "  "   "    "    "  "   "  "
//6659             0013 SETDRE=013  ; assert DRESET L (PC.5) in the IDE interface
//6660             0012 CLRDRE=012  ; clear     " "    "    "  "   "    "    "

//6662                   ; Standard IDE registers...
//6663                  ;   (Note that these are five bit addresses that include the two IDE CS bits,
//6664                  ; CS3Fx (AC4) and CS1Fx (AC5).  The three IDE register address bits, DA2..DA0
//6665                  ; correspond to AC9..AC11.
//6666             0100 CS1FX=100 ; PC.6 selects the 1Fx register space
//6667             0200 CS3FX=200 ; PC.7    " "  3Fx   "   "    "
//6668             0100 REGDAT=CS1FX+0  ; data  (R/W)
//6669             0101 REGERR=CS1FX+1  ; error (R/O)
//6670             0102 REGCNT=CS1FX+2  ; sector count (R;W)
//6671             0103 REGLB0=CS1FX+3  ; LBA byte 0 (or sector number) R/W
//6672             0104 REGLB1=CS1FX+4  ; LBA byte 1 (or cylinder low) R/W
//6673             0105 REGLB2=CS1FX+5  ; LBA byte 2 (or cylinder high) R/W
//6674             0106 REGLB3=CS1FX+6  ; LBA byte 3 (or device/head) R/W
//6675             0107 REGSTS=CS1FX+7  ; status (R/O)
//6676             0107 REGCMD=CS1FX+7  ; command (W/O)
//6677                  
//6678                  ; IDE status register (REGSTS) bits...
//6679             0200 STSBSY=0200 ; busy
//6680             0100 STSRDY=0100 ; device ready
//6681             0040 STSDF= 0040 ; device fault
//6682             0020 STSDSC=0020 ; device seek complete
//6683             0010 STSDRQ=0010 ; data request
//6684             0004 STSCOR=0004 ; corrected data flag
//6685             0001 STSERR=0001 ; error detected
//6686                  
//6687                  ; IDE command codes (or at least the ones we use!)...
//6688             0220 CMDEDD=220  ; execute device diagnostic
//6689             0354 CMDIDD=354  ; identify device
//6690             0040 CMDRDS=040  ; read sectors with retry
//6691             0060 CMDWRS=060  ; write sectors with retry
//6692             0341 CMDSUP=341  ; spin up
//6693             0340 CMDSDN=340  ; spin down

#define SECTOR_SIZE (512)

byte  PPI_04_PPA;
byte  PPI_15_PPB;
byte  PPI_26_PPC,
      prev_PPI_26_PPC;  // prev for edge detection
byte  PPI_37_CC;

byte IDE_DA210  = 0;
bool IDE_DIOR   = false;
bool IDE_DIOW   = false;
bool IDE_RESET  = false;
bool IDE_CS1Fx  = false;
bool IDE_CS3Fx  = false;

#define IDE_STATUS_BSY 00200      // Busy
#define IDE_STATUS_RDY 00100      // Ready
#define IDE_STATUS_DF  00040      // Device Fault
#define IDE_STATUS_DSC 00020      // Device Seek Complete
#define IDE_STATUS_DRQ 00010      // Data Request
#define IDE_STATUS_COR 00004      // Corrected Data Flag
#define IDE_STATUS_ERR 00001      // Error Detected

#define IDE_CMD_EDD 0220          // 0x90 - Execute Device diagnostic 
#define IDE_CMD_IDD 0354          // 0xEC - Indentify Device
#define IDE_CMD_RDS 0040          // 0x20 - Read Sectors w/ retry
#define IDE_CMD_WRS 0060          // 0x30 - Write Sectors w/ retry
#define IDE_CMD_UP  0341          // 0xE1 - Spin up
#define IDE_CMD_DN  0340          // 0xE0 - Spin down

byte IDE_0_DATA;
byte IDE_1_ERROR;
byte IDE_2_SECTOR;
byte IDE_3_LBA0;
byte IDE_4_LBA1;
byte IDE_5_LBA2;
byte IDE_6_LBA3;
byte IDE_7_STATUS_R = IDE_STATUS_RDY | IDE_STATUS_DSC;
byte IDE_7_COMMAND_W;

#define IDE_LBA_MODE 0x40         // bit 6 CHS=0 or LBA=1
#define IDE_LBA_GET()  ( ((IDE_6_LBA3 & 0x0F) << 24) | (IDE_5_LBA2 << 16 ) | (IDE_4_LBA1 << 8 ) | (IDE_3_LBA0 ) )
#define IDE_LBA_SET(A) { IDE_6_LBA3 = (IDE_6_LBA3 & 0xF0) | ((D >> 24) & 0x0F); IDE_5_LBA2 = (D >> 16) & 0xFF; IDE_4_LBA1 = (D >> 8) & 0xFF; IDE_3_LBA0 = D & 0xFF; }


// OS8 image by Steve Gibson from https://www.grc.com/
const char *ide_img_fname       = "gcr_sbc6120.ide";
const char *ide_img_orig_fname  = "gcr_sbc6120_orig.ide";

// OS8 Disk image by Will Sowerbutts (2017-12-28)
// const char *ide_img_fname       = "will_sbc6120.ide";
// const char *ide_img_orig_fname  = "will_sbc6120_orig.ide";

bool ide_sdcard_img_present = false;
File dataFile = NULL;
byte ide_sector_buffer[SECTOR_SIZE];
word ide_sector_buffer_cnt = 0;       // This will set Data transfer request in drive status register
word ide_sector_buffer_ptr = 0;       // Points to where the next data.
unsigned long ide_LBA;                // Placeholder for read/write buffer access


//
// SBC6120 reuqires this to detect IDE HDD correctly.
//

byte ide_idd_struct[] = {           // Big endian so serial #, version, name, model, etc. look correct.
  0x01, 0x42,     // 00: rate <5Mbs, fixed drive, hard-sectored
  0x00, 0x00,     // 01: #cylinders
  0x00, 0x00,     // 02: reserved
  0x00, 0x00,     // 03: #heads
  0x00, 0x00,     // 04: unformated #bytes/track
  0x00, 0x00,     // 05: unformatted #bytes/sector
  0x00, 0x00,     // 06: #sectors/track
  0x00, 0x00,     // 07: VU
  0x00, 0x00,     // 08: VU  
  0x00, 0x00,     // 09: VU
  0x00, 0x00,     // 10: Serial Number. 000h = not specified.
  0x00, 0x00,     // 11: ""
  0x00, 0x00,     // 12: ""
  0x00, 0x00,     // 13: ""
  0x00, 0x00,     // 14: ""
  0x00, 0x00,     // 15: ""
  0x00, 0x00,     // 16: ""
  0x00, 0x00,     // 17: ""
  0x00, 0x00,     // 18: ""
  0x00, 0x00,     // 19: ""
  0x00, 0x00,     // 20: Buffer type
  0x00, 0x00,     // 21: Buffer size in 512 byte increments, 0000h = unspecified.
  0x00, 0x00,     // 22: #ECC bytes available. 0000h = not specified.
  0x00, 0x00,     // 23: FW version
  0x00, 0x00,     // 24: ""
  0x00, 0x00,     // 25: ""
  0x00, 0x00,     // 26: ""
  0x00, 0x00,     // 27: Model number.  0000h = not specified.
  0x00, 0x00,     // 28: ""
  0x00, 0x00,     // 29: ""
  0x00, 0x00,     // 30: ""
  0x00, 0x00,     // 31: ""
  0x00, 0x00,     // 32: ""
  0x00, 0x00,     // 33: ""
  0x00, 0x00,     // 34: ""
  0x00, 0x00,     // 35: ""
  0x00, 0x00,     // 36: ""
  0x00, 0x00,     // 37: ""
  0x00, 0x00,     // 38: ""
  0x00, 0x00,     // 39: ""
  0x00, 0x00,     // 40: ""
  0x00, 0x00,     // 41: ""
  0x00, 0x00,     // 42: ""
  0x00, 0x00,     // 43: ""
  0x00, 0x00,     // 44: ""
  0x00, 0x00,     // 45: ""
  0x00, 0x00,     // 46: ""
  0x00, 0x00,     // 47: 15-8: VU, 7-0: 00h read/write multiple commands not implemented.
  0x00, 0x00,     // 48: 0000h cannot perform doubleword I/O
  0x00, 0x20,     // 49: Capabilities 0x20 LBA Supported. 0x10 DMA Supported.
  0x00, 0x00,     // 50: Reserved
  0x00, 0x00,     // 51: PIO data transfer cycle timing mode
  0x00, 0x00,     // 52: DMA data transfer cycle timing mode
  0x00, 0x00,     // 53: bit0: 0=fields 54-58 may be valid.  1=are valid.
  0x00, 0x00,     // 54: #current cylinders
  0x00, 0x00,     // 55: #current heads
  0x00, 0x00,     // 56: #current sectors/track
  0x40, 0x40,     // 57: Current capacity in sectors  (4096 * 512 = 2MBytes).  Low/High Word
  0x00, 0x00,     // 58: ""
  0x00, 0x00,     // 59: #sectors that can be transferred per interrupt.
  0x40, 0x40,     // 60: #total number of addressable LBA Sectors.            // sbc6120-combined.img is 4 x 4096 secotrs * 512 = 8MB
  0x00, 0x00,     // 61: ""
  0x00, 0x00,     // 62: Single word DMA active/supported.
  0x00, 0x00,     // 63: Multi word DMA active/supported.
                  //
                  // 64 - 127 : Reserved                  // Filled by zero during command execution
                  // 128 - 159: Vendor unique             // Filled by zero during command execution
                  // 160 - 255: Reserved.                 // Filled by zero during command execution
                  // 
};


void check_for_ide_event()
{
  IDE_DA210  = (PPI_26_PPC & 0b00000111);
  IDE_DIOR   = (PPI_26_PPC & 0b00001000) && !(prev_PPI_26_PPC & 0b00001000);
  IDE_DIOW   = (PPI_26_PPC & 0b00010000) && !(prev_PPI_26_PPC & 0b00010000);
  IDE_RESET  = (PPI_26_PPC & 0b00100000) && !(prev_PPI_26_PPC & 0b00100000);
  IDE_CS1Fx  = (PPI_26_PPC & 0b01000000);
  IDE_CS3Fx  = (PPI_26_PPC & 0b10000000);

  if (IDE_RESET)
  {
    if (outputDEBUG || outputINFO || outputERROR) { Serial.println("IDE_RESET"); }

    IDE_0_DATA      = 0x00;
    IDE_1_ERROR     = 0x01;       // By spec - POR Diagnostics Pass
    IDE_2_SECTOR    = 0x01;       // Default by spec
    IDE_3_LBA0      = 0x01;       // Default by spec
    IDE_4_LBA1      = 0x00;
    IDE_5_LBA2      = 0x00;
    IDE_6_LBA3      = 0x00;
    IDE_7_STATUS_R  = IDE_STATUS_RDY | IDE_STATUS_DSC | IDE_STATUS_DRQ;      // Device Ready and Seek Complete and Data Request
    IDE_7_COMMAND_W = 0x00;

    // ide_idle = false;
  }
  else
  if (IDE_DIOR)
  {
    // IDE Read
    word ide_data_output = 0;
 
    if (IDE_CS1Fx)
    {
      switch (IDE_DA210) {
          case 0: // IDE_0_DATA:
              
              if (ide_sector_buffer_cnt == 0)
              {              
                ide_data_output = IDE_0_DATA;       // buffer not used, just return what was already in register.
              }
              else 
              {
                // Read from buffer
                ide_data_output  = ide_sector_buffer[ide_sector_buffer_ptr];
                ide_sector_buffer_ptr++;
                ide_data_output |= ide_sector_buffer[ide_sector_buffer_ptr] << 8;    
                ide_sector_buffer_ptr++;       

                ide_sector_buffer_cnt--;
                ide_sector_buffer_cnt--;

                IDE_0_DATA = ide_data_output;    
                
                if (ide_sector_buffer_cnt == 0)
                {
                  // IDE_7_STATUS_R = IDE_7_STATUS_R & ~IDE_STATUS_DRQ;

                  IDE_2_SECTOR--;       // TODO: if >0, repeat write, i.e. ide_sector_buffer_cnt = 512
                }
              }
              break;

          case 1: // IDE_1_ERROR:
              ide_data_output = IDE_1_ERROR;
              IDE_1_ERROR     = 0x00;               // Reset Error register when read. [ERTURK]
              break;

          case 2: // IDE_2_SECTOR:
              ide_data_output = IDE_2_SECTOR;
              break;

          case 3: // IDE_3_LBA0:
              ide_data_output = IDE_3_LBA0;
              break;

          case 4: // IDE_4_LBA1:
              ide_data_output = IDE_4_LBA1;
              break;

          case 5: // IDE_5_LBA2:
              ide_data_output = IDE_5_LBA2;
              break;

          case 6: // IDE_6_LBA3:
              ide_data_output = IDE_6_LBA3;
              break;

          case 7: // IDE_7_STATUS_R:
              ide_data_output = IDE_7_STATUS_R;
              break;
      }
    }
    else
    if (IDE_CS3Fx)
    {

    }

    // Drive 8255 portA & portB
    PPI_04_PPA = (ide_data_output >> 8);
    PPI_15_PPB = (ide_data_output & 0xFF);
      
    if (outputDEBUG || outputINFO) // || outputERROR)
    { 
      // Serial.println("IDE_WRITE ");
      sprintf(tmp, "IDE_READ ADDR: %.2X DATA: %.4X LEFT: %0.4d\n", IDE_DA210, ide_data_output, ide_sector_buffer_cnt);  
      Serial.write(tmp);
    }
    
    // we are in READ cycle
    // ide_idle = false;
  }
  else
  if (IDE_DIOW)
  {
    // IDE Write

    word          data_to_hdd;
    data_to_hdd = (PPI_04_PPA << 8 | PPI_15_PPB);
    
    if (outputDEBUG || outputINFO) // || outputERROR) 
    { 
      // Serial.println("IDE_WRITE ");
      sprintf(tmp, "IDE_WRITE ADDR: %.2X DATA: %.4X\n", IDE_DA210, data_to_hdd);  
      Serial.write(tmp);
    }
    
    if (IDE_CS1Fx)
    {     
      switch (IDE_DA210) {
          case 0: // IDE_0_DATA:
              IDE_0_DATA = data_to_hdd;
            
              if (ide_sector_buffer_cnt == 0)
              {              
                IDE_0_DATA = data_to_hdd;       // buffer not used, just save 8bit.
              }
              else {
                // Write to buffer
                ide_sector_buffer[ide_sector_buffer_ptr] = data_to_hdd; // &  0xFF;
                ide_sector_buffer_ptr++;
                ide_sector_buffer[ide_sector_buffer_ptr] = data_to_hdd >> 8;
                ide_sector_buffer_ptr++;

                // // Read from buffer
                // ide_data_output  = ide_sector_buffer[ide_sector_buffer_ptr];
                // ide_sector_buffer_ptr++;
                // ide_data_output |= ide_sector_buffer[ide_sector_buffer_ptr] << 8;    
                // ide_sector_buffer_ptr++;    

                ide_sector_buffer_cnt--;
                ide_sector_buffer_cnt--;

                if (ide_sector_buffer_cnt == 0)
                {
                  // EDK File dataFile = SD.open(ide_img_fname, FILE_WRITE);
                  dataFile.seek(ide_LBA*SECTOR_SIZE);
                  dataFile.write(ide_sector_buffer, SECTOR_SIZE);
                  dataFile.flush();
                  // EDK dataFile.close();                  // Assume all went ok.
                  
                  // IDE_7_STATUS_R = IDE_7_STATUS_R & ~IDE_STATUS_DRQ;

                  IDE_2_SECTOR--;       // TODO: if >0, repeat write, i.e. ide_sector_buffer_cnt = 512
                }
              }
              break;

          case 1: // IDE_1_ERROR:
              IDE_1_ERROR = data_to_hdd & 0xFF;              
              break;
          case 2: // IDE_2_SECTOR:
              IDE_2_SECTOR = data_to_hdd & 0xFF;
              break;
          case 3: // IDE_3_LBA0:
              IDE_3_LBA0 = data_to_hdd & 0xFF;
              break;
          case 4: // IDE_4_LBA1:
              IDE_4_LBA1 = data_to_hdd & 0xFF;
              break;
          case 5: // IDE_5_LBA2:
              IDE_5_LBA2 = data_to_hdd & 0xFF;
              break;
          case 6: // IDE_6_LBA3:
              IDE_6_LBA3 = data_to_hdd & 0xFF;
              break;
          case 7: // IDE_7_COMMAND_W:
              IDE_7_COMMAND_W = data_to_hdd & 0xFF;

              // Act on the command:
              switch(IDE_7_COMMAND_W)
              {
                case IDE_CMD_EDD: // 0220          // 0x90 - Execute Device diagnostic 
                  {
                    IDE_1_ERROR     = 0x01;        //        01h - No error detected.
                    ide_sector_buffer_cnt = 0;
                    break;
                  }
                case IDE_CMD_IDD: // 0354          // 0xEC - Identify Device
                  {
                    // Copy idd struct to sector buffer and setup for reading.
                    word i;
                    for (i=0; i < SECTOR_SIZE; i++) ide_sector_buffer[i] = 0x00;
                    for (i=0; i < sizeof(ide_idd_struct); i++) ide_sector_buffer[i] = ide_idd_struct[i];
                    ide_sector_buffer_cnt = SECTOR_SIZE;
                    ide_sector_buffer_ptr = 0;      // start from beginning of buffer
                    IDE_7_STATUS_R = IDE_7_STATUS_R | IDE_STATUS_DRQ;
                    
                    break;
                  }
                case IDE_CMD_RDS: // 0040          // 0x20 - Read Sectors w/ retry
                  {
                    ide_LBA = IDE_LBA_GET();

                    // EDK File dataFile = SD.open(ide_img_fname);
                    dataFile.seek(ide_LBA*SECTOR_SIZE);
                    dataFile.read(ide_sector_buffer, SECTOR_SIZE);
                    // EDK dataFile.close();                           // Expect all to be good.

                    ide_sector_buffer_cnt = SECTOR_SIZE;
                    ide_sector_buffer_ptr = 0;      // start from beginning of buffer
                    IDE_7_STATUS_R = IDE_7_STATUS_R | IDE_STATUS_DRQ;
                    
                    if (outputDEBUG || outputINFO) // || outputERROR) 
                    {
                      sprintf(tmp, "IDE_READ  LBA: %0.8X\n", ide_LBA);  
                      Serial.write(tmp);
                      // delay(2000);
                    }
                    if (IDE_2_SECTOR >1) { Serial.println("NON-ZERO #sector to read. halted.");  while(1); }
                    break;
                  }
                case IDE_CMD_WRS: // 0060          // 0x30 - Write Sectors w/ retry
                  {
                    ide_LBA = IDE_LBA_GET();

                    // File dataFile = SD.open(ide_img_fname, FILE_WRITE);
                    // dataFile.seek(ide_LBA*512);
                    // dataFile.read(ide_sector_buffer, 512);
                    // dataFile.close();

                    ide_sector_buffer_cnt = SECTOR_SIZE;
                    ide_sector_buffer_ptr = 0;      // start from beginning of buffer
                    IDE_7_STATUS_R = IDE_7_STATUS_R | IDE_STATUS_DRQ;

                    if (outputDEBUG || outputINFO) // || outputERROR) 
                    {
                      sprintf(tmp, "IDE_WRITE LBA: %0.8X\n", ide_LBA);  
                      Serial.write(tmp);
                      // delay(2000);
                    }
                                     
                    if (IDE_2_SECTOR >1) { Serial.println("NON-ZERO #sector to write. halted.");  while(1); }
                    break;
                  }
                case IDE_CMD_UP:  //  0341          // 0xE1 - Spin up
                    ide_sector_buffer_cnt = 0;
                    break;

                case IDE_CMD_DN:  //  0340          // 0xE0 - Spin down
                    ide_sector_buffer_cnt = 0;
                    break;

                default:
                    Serial.println("IDE : UNKNOWN CMD. Halted.");
                    while(1);
              }                            
              break;
      }
    }
    else
    if (IDE_CS3Fx)
    {

    }    
    // We are in WRITE cycle
    // ide_idle = false;
  }
  
  prev_PPI_26_PPC = PPI_26_PPC;
}

void ide_check_for_sdimage()
{
  
  // Check for OS8 image file on SDCARD
  ide_sdcard_img_present = false;

  if (SD_Card_Present)
  {
    bool do_you_want_to_refresh_os_image = false;

    if (do_you_want_to_refresh_os_image = false) // Refresh disk image
    {
      const unsigned int  BUFFER_SIZE = SECTOR_SIZE*4;
      byte                bfr[BUFFER_SIZE];
      unsigned int        bytes_read;

      // Serial.println("\nSDCard: Card present!");
      Serial.println("\nSDCard: Refreshing os8 image from original !");
      Serial.flush();

      SD.remove(ide_img_fname);

      // Open files
      File src  = SD.open(ide_img_orig_fname, FILE_READ);
      File dest = SD.open(ide_img_fname,      FILE_WRITE);

      src.seek(0);
      dest.seek(0);

      while( bytes_read = src.read(bfr, BUFFER_SIZE))
      {
        dest.write(bfr, bytes_read);
        Serial.write("#");
      }
      dest.flush();

      dest.close();
      src.close();
      Serial.write("\n");
    }

    // open the file.
    dataFile = SD.open(ide_img_fname, FILE_WRITE);

    if (dataFile) {
      ide_sdcard_img_present = true;
      Serial.print("- IDE Image:      "); Serial.print("\""); Serial.print(ide_img_fname); Serial.println("\"");

      // Update file size in ide_identify_struct
      unsigned long fsize = dataFile.size() /SECTOR_SIZE;

      Serial.print("- Image Size:      0x"); Serial.println(dataFile.size(), HEX);
      Serial.print("- Identify Table:  0x"); Serial.println(fsize, HEX);
      Serial.flush();
      // delay(1500);
      
      ide_idd_struct[2*57]    = fsize & 0xFF;
      ide_idd_struct[2*57+1]  = fsize >> 8;
      ide_idd_struct[2*60]    = fsize & 0xFF;
      ide_idd_struct[2*60+1]  = fsize >> 8;

      // dataFile.close();
    }  
    // if the file can't be open, ignore disk:
    else {
      Serial.println("SDCard: Can not open OS8 disk image.");
      // dataFile.close();
      ide_sdcard_img_present = false;
      dataFile = NULL;
    } 
  }  
}
  
#endif
