////////////////////////////////////////////////////////////////////
// RetroShield 8008 for Teensy 3.5/3.6/4.1
//
// 2024/09/13
// Version 0.1

// The MIT License (MIT)

// Copyright (c) 2019 Erturk Kocalar, 8Bitforce.com

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

// Date         Comments                                            Author
// -----------------------------------------------------------------------------
// 09/13/2024   Bring-up on Teensy 4.1.                             Erturk
// 09/29/2024   8008 executes NOP, NOP, JMP 0 :)                    Erturk


////////////////////////////////////////////////////////////////////
// Options
//   outputDEBUG: Print memory access debugging messages.
////////////////////////////////////////////////////////////////////
#define outputDEBUG     0

////////////////////////////////////////////////////////////////////
// BOARD DEFINITIONS
////////////////////////////////////////////////////////////////////

#include "memorymap.h"      // Memory Map (ROM, RAM, PERIPHERALS)
#include "portmap.h"        // Pin mapping to cpu
#include "setuphold.h"      // Delays required to meet setup/hold
#include "buttons.h"        // Functions to read 2 buttons on teensy adapter board
#include "soft_uart.h"      // Soft UART
#include "intellec.h"       // Intellec 8/Mod 8 IO+Serial board emulation

char tmp[200];                // for debug sprintf buffer

////////////////////////////////////////////////////////////////////
// CLOCK functions

#define CPU_CLOCK_STATE_1HL   1
#define CPU_CLOCK_STATE_1LL   2
#define CPU_CLOCK_STATE_2LH   3
#define CPU_CLOCK_STATE_2LL   4
#define CPU_CLOCK_STATE_RST   5

byte cpu_clock_state = CPU_CLOCK_STATE_RST;

void cpu_clock_advance()
{
  switch(cpu_clock_state)
  {
    case CPU_CLOCK_STATE_2LL: CLK1_HIGH();  cpu_clock_state = CPU_CLOCK_STATE_1HL; break;
    case CPU_CLOCK_STATE_1HL: CLK1_LOW();   cpu_clock_state = CPU_CLOCK_STATE_1LL; break;
    case CPU_CLOCK_STATE_1LL: CLK2_HIGH();  cpu_clock_state = CPU_CLOCK_STATE_2LH; break;
    case CPU_CLOCK_STATE_2LH: CLK2_LOW();   cpu_clock_state = CPU_CLOCK_STATE_2LL; break;
    case CPU_CLOCK_STATE_RST:
    default:                  CLK1_LOW(); CLK2_LOW(); cpu_clock_state = CPU_CLOCK_STATE_2LL; break;
  }
}

inline __attribute__((always_inline))
void cpu_clock_print_state()
{
  switch(cpu_clock_state)
  {
    case CPU_CLOCK_STATE_2LL:   Serial.write("00"); break;
    case CPU_CLOCK_STATE_1HL:   Serial.write("10"); break;
    case CPU_CLOCK_STATE_1LL:   Serial.write("00"); break;
    case CPU_CLOCK_STATE_2LH:   Serial.write("01"); break;
    case CPU_CLOCK_STATE_RST:   Serial.write("--"); break;
    default:                    Serial.write("??"); break;
  }
}

////////////////////////////////////////////////////////////////////
void uP_assert_reset()
{
  // Drive RESET conditions
  digitalWriteFast( uP_CLK1,      LOW  );
  digitalWriteFast( uP_CLK2,      LOW  );
  cpu_clock_state = CPU_CLOCK_STATE_RST;

  digitalWriteFast( uP_READY,     HIGH );   
  digitalWriteFast( uP_INTERRUPT, LOW  ); 

}

void uP_release_reset()
{
  // 8008 does not have a dedicated RESET pin.
  // Instead, it powers up in STOPPED state and
  // interrupt must be triggered to jam a start
  // instruction.

  // To assert reset in during normal operation,
  // we must assert interrupt and restart the
  // monitor code. This is not technically a 
  // hardware reset. Consider this as part of
  // learning process in microprocessor history.

  // The "deasserting the reset" action
  // is done in cpu_tick(), by deasserting interrupt
  // after 8008 acknowledges it.  
}

////////////////////////////////////////////////////////////////////

byte INTERRUPT_RESPONSE_RST00 = 0b00000101;   // RST 0x00 instruction
byte INTERRUPT_RESPONSE_RST08 = 0b00001101;   // RST 0x08 instruction
byte INTERRUPT_RESPONSE_RST10 = 0b00010101;   // RST 0x10 instruction
byte INTERRUPT_RESPONSE_RST18 = 0b00011101;   // RST 0x18 instruction
byte INTERRUPT_RESPONSE_RST20 = 0b00100101;   // RST 0x20 instruction
byte INTERRUPT_RESPONSE_RST28 = 0b00101101;   // RST 0x28 instruction
byte INTERRUPT_RESPONSE_RST30 = 0b00110101;   // RST 0x30 instruction
byte INTERRUPT_RESPONSE_RST38 = 0b00111101;   // RST 0x38 instruction
byte INTERRUPT_RESPONSE_HALT  = 0b00000000;   // HALT instruction
byte INTERRUPT_RESPONSE_NOP   = 0b11000000;   // NOP

byte INTERRUPT_RESPONSE_TO_USE = INTERRUPT_RESPONSE_RST00;
bool INTERRUPT_ASSERT         = false;        // if true, cpu_tick() will assert interrupt line (sync'ed to leading edge of clk1/clk2)

void i8008_assert_interrupt()
{
  // Tell cpu_tick() we want to kick off an interrupt cycle.
  INTERRUPT_ASSERT = true;
}

////////////////////////////////////////////////////////////////////

void uP_init()
{
  // Set directions for ADDR & DATA Bus.
  configure_PINMODE_ADDR();
  configure_PINMODE_DATA();

  pinMode( uP_CLK1,       OUTPUT); digitalWrite( uP_CLK1,       LOW  );
  pinMode( uP_CLK2,       OUTPUT); digitalWrite( uP_CLK2,       LOW  );
  pinMode( uP_READY,      OUTPUT); digitalWrite( uP_READY,      HIGH );
  pinMode( uP_INTERRUPT,  OUTPUT); digitalWrite( uP_INTERRUPT,  LOW  );

  pinMode( uP_SYNC, INPUT);
  pinMode( uP_S0,   INPUT);
  pinMode( uP_S1,   INPUT);
  pinMode( uP_S2,   INPUT);  

  // 74LS245
  pinMode( DATA_BUF_DIR, OUTPUT );  digitalWrite( DATA_BUF_DIR, LOW );

  // RESET Button
  pinMode( KEY_RESET, INPUT );
  pinMode( LED_DEBUG, OUTPUT);   digitalWrite(LED_DEBUG, HIGH);

  uP_assert_reset();

}

void board_init()
{
  // Initialize "hw" before cpu starts executing, such as
  // filling memory or modify vectors or modifying rom functions.

  // const byte myprogram[] = {
  //   0b01010101,           // dummy
  //   0b11000000,           // NOP
  //   0b11000000,           // NOP
  //   0b00000000,           // HALT
  // };
  // for (int i=0; i<sizeof(myprogram); i++)
  //   RAM[0x0 + i] = myprogram[i];

  // copy the first few bytes for bootstrapping.
  for (int i=0; i<0x20; i++)
    memory_write_byte(i, memory_read_byte(0x3800+i, 0x00));     // RAM[i] = ROM[i];
}

inline __attribute__((always_inline))
bool is_reset_btn_pressed()
{
  static int debounce_cnt = 16;
  bool reset_btn_pressed;

  reset_btn_pressed = !digitalReadFast(KEY_RESET);

  if (!reset_btn_pressed)
  {
    debounce_cnt = 16;
    // digitalWriteFast(LED_DEBUG, true);
    // DBG_LED_On(LED_DEBUG);
  }
  else
  if (reset_btn_pressed && (debounce_cnt > 0) )
  {
    debounce_cnt--;

    if (debounce_cnt == 0)
    {
      // digitalWriteFast(LED_DEBUG, false);
      DBG_LED_On(LED_DEBUG);
      return true;
      // i8008_assert_interrupt();
    }
  }
  return false;
}

////////////////////////////////////////////////////////////////////
// DEBUG functions
////////////////////////////////////////////////////////////////////

inline __attribute__((always_inline))
void DEBUG_PULSE_INT()
{
    cli();    // disable interrupts so RDY low doesn't get clocked in
    digitalWriteFast( uP_INTERRUPT,      HIGH );
    DELAY_UNIT();
    digitalWriteFast( uP_INTERRUPT,      LOW );
    sei();
}

inline __attribute__((always_inline))
void DEBUG_PULSE_READY()
{
    cli();    // disable interrupts so RDY low doesn't get clocked in
    digitalWriteFast( uP_READY,      LOW );
    DELAY_UNIT();
    digitalWriteFast( uP_READY,      HIGH );
    sei();
}

inline __attribute__((always_inline))
void DEBUG_PULSE_READY(unsigned int dly)
{
    cli();    // disable interrupts so RDY low doesn't get clocked in
    digitalWriteFast( uP_READY,      LOW );
    delayNanoseconds(dly);
    digitalWriteFast( uP_READY,      HIGH );
    sei();
}


////////////////////////////////////////////////////////////////////
// Processor Control Loop
////////////////////////////////////////////////////////////////////

word uP_ADDR;
byte uP_DATA;

byte DATA_OUT = 0x00;
byte DATA_IN  = 0x00;

byte uP_STATE   = 0;
byte uP_STATE_PREV  = 0;

byte uP_CCC     = 0;
byte uP_DATA_T1 = 0;                // Save data at each machine cycle.
byte uP_DATA_T2 = 0;                // They are used for different purposes
byte uP_DATA_T3 = 0;                // based on machine cycle.
byte uP_IO_RRMMM = 0;               // IO instruction (01 RRM MM1) decoded to RRMM

#define uP_CCC_PCI        (0b00)    // Instruction read
#define uP_CCC_PCR        (0b10)    // Memory read
#define uP_CCC_PCC        (0b01)    // I/O operation
#define uP_CCC_PCW        (0b11)    // Memory write

#define uP_STATE_T1       (0b010)
#define uP_STATE_T1_INT   (0b011)
#define uP_STATE_T2       (0b001)
#define uP_STATE_WAIT     (0b000)
#define uP_STATE_T3       (0b100)
#define uP_STATE_STOPPED  (0b110)
#define uP_STATE_T4       (0b111)
#define uP_STATE_T5       (0b101)
                                    // 8008 has 8x input ports and 24x output ports, instr: 01RR_MMM1
                                    // INP  covers 00000..00111
byte uP_i8008_ioports[32];          // OUTP covers 01000..11000
bool INTERRUPT_CYCLE          = false;        // true if 8008 is fetching interrupt instruction (at uP_STATE_T1_INT)

inline __attribute__((always_inline))
void print_proc_state(byte state)
{
  switch(state)
  {
    case uP_STATE_T1:         Serial.print("T1     "); break;
    case uP_STATE_T1_INT:     Serial.print("T1_INT "); break;
    case uP_STATE_T2:         Serial.print("T2     "); break;
    case uP_STATE_WAIT:       Serial.print("WAIT   "); break;
    case uP_STATE_T3:         Serial.print("T3     "); break;
    case uP_STATE_STOPPED:    Serial.print("STOPPED"); break;
    case uP_STATE_T4:         Serial.print("T4     "); break;
    case uP_STATE_T5:         Serial.print("T5     "); break;
    default:                  Serial.print("??STATE"); break;
  }
}

inline __attribute__((always_inline))
void print_cpu_debug(byte note)
{
  if (outputDEBUG)
  {      
    sprintf(tmp, "[%c] CLK %01X%01X / SYNC: %01X DATA: %02X ", note, digitalReadFast(uP_CLK1), digitalReadFast(uP_CLK2), digitalReadFast(uP_SYNC), xDATA_IN());  Serial.write(tmp);
    // sprintf(tmp, "[%c] CLK %01X%01X / SYNC: %01X ", note, digitalRead(uP_CLK1), digitalRead(uP_CLK2), digitalReadFast(uP_SYNC));  Serial.write(tmp);
    // sprintf(tmp, " S0=%01X%01X%01X", digitalReadFast(uP_S0), digitalReadFast(uP_S1), digitalReadFast(uP_S2)); Serial.write(tmp);
    Serial.write(" STATE= "); print_proc_state(uP_STATE); 
    // sprintf(tmp, " CCC= %01X", uP_CCC);  Serial.write(tmp);
    // sprintf(tmp, " pci=%01X pcr=%01X pcc=%01X pcw=%01X ", ccc_pci, ccc_pcr, ccc_pcc, ccc_pcw);  Serial.write(tmp);
    // sprintf(tmp, " 245=%01X AL=%01X AH=%01X MR=%01X MW=%01X IOR=%01X IOW=%01X IOCLR=%01X ", control_dbd, control_all, control_alh, control_mer, control_mew, control_ior, control_iow, control_iclr);  Serial.write(tmp);
    // sprintf(tmp, " AL=%01X%01X DL=%01X ", control_all, control_alh, control_dbd);  Serial.write(tmp);
    // sprintf(tmp, " PENDING=%01X", INTERRUPT_ASSERT); Serial.write(tmp);
    sprintf(tmp, " ASSERT=%01X", INTERRUPT_ASSERT); Serial.write(tmp);
    // sprintf(tmp, " PINT=%01X", processor_interrupted); Serial.write(tmp);
    Serial.write(" INTERRUPT= "); Serial.print(digitalReadFast(uP_INTERRUPT));
    // Serial.write(" READY= "); Serial.print(digitalReadFast(uP_READY));
    sprintf(tmp, " ## ADDR %04X // DATA: %02X ##", uP_ADDR, uP_DATA);  Serial.write(tmp);

    Serial.write("\n\r");

    if (0) delay(100);
  }
}

////////////////////////////////////////////////////////////////////
// 
inline __attribute__((always_inline))
void cpu_tick()
{ 

  //////////////////////////////////////////////////
  // 0) Wait for SYNC up
  if (!digitalReadFast(uP_SYNC))
  {
    Serial.println("ERROR - cputick() requires SYNC high");
    while(1);
  }

  //////////////////////////////////////////////////
  // SYNC = HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
  // 1) Wait for CLK1 up  
  CLK1_HIGH();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();

  // tri-state if not going into T3. Otherwise, don't touch it.
  if( uP_STATE_PREV != uP_STATE_T2)
  { 
    xDATA_DIR_IN(); 
    /*DEBUG_PULSE();*/ 
  }

  if (INTERRUPT_ASSERT)                           // We need to assert interrupt pin to 8008
  {
    digitalWriteFast(uP_INTERRUPT, HIGH);         // Synchronize w/ clk1 rising edge
    INTERRUPT_ASSERT = false;
  }

  //////////////////////////////////////////////////
  // SYNC = HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
  // 2) Wait for CLK1 down
  CLK1_LOW();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();

    // WARNING:
    // TODO: 
    // 1) T1 & T1i state takes about 500ns longer to stabilize. (from CLK1 rising edge vs CLK2 falling edge)
    // it maybe good idea to wait here a bit before reading the state.
    // 2) The catch is datasheet shows DATA_IN must be driven before CLK1 falling edge,
    // which needs to happen at previous step. but we are reading uP_STATE here.
    // Chicken and egg :(
    // State lines should be stabilized after CLK1 goes down.
    //
    // Moved state read to CLK2=up.
    //

  //////////////////////////////////////////////////
  // SYNC = HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
  // 3) Wait for CLK2 up
  CLK2_HIGH();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /* 50ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  
  uP_STATE = ( (digitalReadFast(uP_S0) << 2) | (digitalReadFast(uP_S1) << 1) | (digitalReadFast(uP_S2) << 0));

  // We process states at different clock phases.
  // That's why some states are commented out here.

  switch(uP_STATE)
  {
    // case uP_STATE_T1:         break;
    case uP_STATE_T1_INT:     digitalWriteFast(uP_INTERRUPT, LOW);      // 8008 acknowledged interrupt request. 
                              INTERRUPT_CYCLE = true;
                              // DEBUG_PULSE();
                              break;
    // case uP_STATE_T2:         break;
    // case uP_STATE_WAIT:       break;
    case uP_STATE_T3:
                              // Outputs are already outputting
                              // at the end of T2 cycle. (see end of this function)
                              //

                              // Data-IN activity depends on CCC
                              if (INTERRUPT_CYCLE)
                              {
                                // uP_DATA = DATA_OUT = INTERRUPT_RESPONSE;
                                // xDATA_DIR_OUT();
                                // SET_DATA_OUT(DATA_OUT);
                                // DEBUG_PULSE();
                                // DELAY_FOR_BUFFER();           // Let level shifter stabilize.     
                                INTERRUPT_CYCLE = false;         

                                // TODO: 
                                // 2025/01/29:
                                // Does 8008 support multi-byte instruction jamming?
                                // Rethinking this, right way to implement interrupt cycle could be
                                // to get out of interrupt cycle when processor goes through T1 state.  
                                // So we would stay in interrupt cycle through multiple T1_INT states.                               
                              } 
                              break;
    // case uP_STATE_STOPPED:    break;
    // case uP_STATE_T4:         break;
    // case uP_STATE_T5:         break;
    default:                  break;
  }

  //////////////////////////////////////////////////
  // SYNC = HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
  // 4) Wait for CLK2 down
  CLK2_LOW();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();

  //////////////////////////////////////////////////
  // SYNC = LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
  // 5) Wait for SYNC down
  while (digitalReadFast(uP_SYNC));       // Actually wait here, instead of blind delay.
  // DEBUG_PULSE();
  
  xDATA_DIR_IN();     // DATA_IN hold time >= SYNC OUT delay.
  // DEBUG_PULSE();

  //////////////////////////////////////////////////
  // SYNC = LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
  // 6) Wait for CLK1 up
  CLK1_HIGH();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();

  //////////////////////////////////////////////////
  // SYNC = LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
  // 7) Wait for CLK1 down
  CLK1_LOW();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();

    switch(uP_STATE)
    {
      case uP_STATE_T1:         
      case uP_STATE_T1_INT:
                                uP_DATA_T1 = xDATA_IN();
                                // DEBUG_PULSE();
                                uP_ADDR    = uP_DATA_T1;
                                break;
      case uP_STATE_T2:         uP_DATA = xDATA_IN();
                                // DEBUG_PULSE();
                                uP_CCC      = (uP_DATA & 0b11000000) >> 6;    // T2 ccc
                                uP_IO_RRMMM = (uP_DATA & 0b00111110) >> 1;    // I/O cycle
                                uP_DATA_T2  = (uP_DATA & 0b00111111);         // T2 addr
                                uP_ADDR     = uP_ADDR | (uP_DATA_T2 << 8);    // add addr high bits.
                                
                                break;
      default:
                                break;
    }

  //////////////////////////////////////////////////
  // SYNC = LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
  // 8) Wait for CLK2 up
  CLK2_HIGH();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /* 50ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS();

  // THIS IS WHEN THE ADDR TRANSACTIONS HAPPEN
  //
  switch(uP_STATE)
  {
    // case uP_STATE_T1:         
    // case uP_STATE_T1_INT:
    //                           uP_DATA_T1 = xDATA_IN();
    //                           DEBUG_PULSE();
    //                           uP_ADDR    = uP_DATA_T1;
    //                           break;
    // case uP_STATE_T2:         uP_DATA = xDATA_IN();
    //                           DEBUG_PULSE();
    //                           uP_DATA_T2 =  uP_DATA & 0b00111111; 
    //                           uP_CCC     = (uP_DATA & 0b11000000) >> 6;
    //                           // Contruct address
    //                           uP_ADDR    = (uP_DATA_T2 << 8) | (uP_DATA_T1);
    //                           break;
    // case uP_STATE_WAIT:       break;
    case uP_STATE_T3:         uP_DATA_T3 = uP_DATA = xDATA_IN();
                              // DEBUG_PULSE_INT();
                              if (uP_CCC == uP_CCC_PCW)
                              {
                                // RAM1?
                                #ifdef RAM1_START
                                if ( (RAM1_START <= uP_ADDR) && (uP_ADDR <= RAM1_END) )
                                { 
                                  memory_write_byte(uP_ADDR, uP_DATA); 
                                }        
                                else
                                #endif
                                // RAM2?
                                #ifdef RAM2_START
                                if ( (RAM2_START <= uP_ADDR) && (uP_ADDR <= RAM2_END) )
                                { 
                                  memory_write_byte(uP_ADDR, uP_DATA);
                                }        
                                else
                                #endif
                                
                                // Unknown
                                {
                                  Serial.write("?W RAM ");            
                                  Serial.println(uP_ADDR, HEX);
                                }
                              } 
                              else
                              if (uP_CCC == uP_CCC_PCC)
                              {
                                // Save IO data to the local store.
                                uP_i8008_ioports[uP_IO_RRMMM] = uP_DATA_T1;
                                
                                // Act on it.
                                if ((uP_IO_RRMMM >= 0x00) && (uP_IO_RRMMM <= 0x0B))
                                {
                                  intellec_write_io(uP_IO_RRMMM, uP_DATA_T1);
                                }                                  
                                else
                                // Unknown
                                {
                                  Serial.write("unknown IOW? "); 
                                  Serial.print(uP_IO_RRMMM, HEX); 
                                  Serial.write("=");
                                  Serial.println(uP_DATA_T1, HEX);
                                }
                              }      
                              break;
    // case uP_STATE_STOPPED:    break;
    // case uP_STATE_T4:         break;
    // case uP_STATE_T5:         break;
    default:                  break;
  }

  //////////////////////////////////////////////////
  // SYNC = LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
  // 9) Wait for CLK2 down
  CLK2_LOW();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();

  uP_STATE_PREV = uP_STATE;
  // DEBUG_PULSE();

  //////////////////////////////////////////////////
  // WARNING:
  // 
  // For T3 (data activity)
  // Datasheet requires >=0 time from CLK1 falling edge
  // to data_in (data_out from arduino).
  // Our output data needs to go thru two buffers (TXB0108 & 74HC245)
  // so we need to start driving the bus
  // as soon as we know next state will be T3, which is here.
  //
  // There will be a slight bus contention as 8008 changes its output
  // and we start enabling outputs. 680 Ohm series resistors should prevent damage.
  // TODO: reduce 680 Ohm to improve timing??

  if( uP_STATE_PREV == uP_STATE_T2)
  {

    // Data-IN activity depends on CCC 
    if (INTERRUPT_CYCLE)
    {
      // An interrupt cycle is same as regular instruction fetch except instruction is externally "jammed"
      // Option 1: Jam a RST instruction, jump to one of known locations.
      // Option 2: Jam a regular instruction, which 8008 will read one or multiple bytes for the full instruction.

      // TODO: 
      // Do we need to support multi-byte interrupt instruction?

      // DEBUG_PULSE_INT();
      xDATA_DIR_OUT();

      // Two ways to implement interrupt response: 
         uP_DATA = DATA_OUT = INTERRUPT_RESPONSE_TO_USE;        // - send a fixed response, 
      // uP_DATA = DATA_OUT = memory_read_byte(0x0000, 0x00);   // - send first byte of ROM.

      // DEBUG_PULSE();
      // xDATA_DIR_OUT();
      SET_DATA_OUT(DATA_OUT);
      // DEBUG_PULSE();
      // DELAY_FOR_BUFFER();           // Let level shifter stabilize.     
    } 
    else
    if ( (uP_CCC == uP_CCC_PCI) || (uP_CCC == uP_CCC_PCR) )
    {
      
      // DEBUG_PULSE_INT();
      xDATA_DIR_OUT();
      
      // ROM1?
      #ifdef ROM1
      if ( (ROM1_START <= uP_ADDR) && (uP_ADDR <= ROM1_END) )
      { 
        // DEBUG_PULSE();
        DATA_OUT = memory_read_byte(uP_ADDR, 0xFF);
        // if (outputDEBUG) { Serial.print("ROM "); }
        // DEBUG_PULSE();
      }
      else 
      #endif
      // ROM2?
      #ifdef ROM2
      if ( (ROM2_START <= uP_ADDR) && (uP_ADDR <= ROM2_END) )
      { 
        // DEBUG_PULSE();
        DATA_OUT = memory_read_byte(uP_ADDR, 0xFF);
        // if (outputDEBUG) { Serial.print("ROM "); }
        // DEBUG_PULSE();
      }
      else 
      #endif
      // RAM1?
      #ifdef RAM1_START
      if ( (RAM1_START <= uP_ADDR) && (uP_ADDR <= RAM1_END) )
      { 
        // DEBUG_PULSE();
        DATA_OUT = memory_read_byte(uP_ADDR, 0x00);
        // DEBUG_PULSE();
        // if (outputDEBUG) { Serial.print("RAM "); }
      }        
      else
      #endif
      // RAM2?
      #ifdef RAM2_START
      if ( (RAM2_START <= uP_ADDR) && (uP_ADDR <= RAM2_END) )
      { 
        // DEBUG_PULSE();
        DATA_OUT = memory_read_byte(uP_ADDR, 0x00);
        // DEBUG_PULSE();
        // if (outputDEBUG) { Serial.print("RAM "); }
      }        
      else
      #endif
      // unknown read
      DATA_OUT = 0x00;      // 0x00 or HALT instruction            

      SET_DATA_OUT(DATA_OUT);
      // DEBUG_PULSE();                                  
    } 
    else
    if (uP_CCC == uP_CCC_PCC)
    {
      if ((uP_IO_RRMMM >= 0x00) && (uP_IO_RRMMM <= 0x0B))
      {
        // IO Read
        /* DEBUG_PULSE_INT() */;
        xDATA_DIR_OUT();
        DATA_OUT = intellec_read_io(uP_IO_RRMMM);    
        SET_DATA_OUT(DATA_OUT);
      }      
      else
      {
        Serial.write("unknown IOR? "); 
        Serial.println(uP_IO_RRMMM, HEX); 
      }      
    }
  }
}

////////////////////////////////////////////////////////////////////
// Soft UART callback functions to drive TXD/RXD pins.
// - i8008_txd = pin driven by 8008 to transmit uart
// - i8008_rxd = pin read by 8008 to receive UART
//
// Unused on Intellec 8/Mod 8.  
// Left as reference for future.
// 
////////////////////////////////////////////////////////////////////

// From monitor.asm
// INPORT      equ 0                   ; serial input port address
// OUTPORT     equ 08H                 ; serial output port address

byte i8008_rxd_inport  = 0;
byte i8008_txd_outport = 8;

inline __attribute__((always_inline))
bool read_uP_TXD_pin_i8008()
{
  return (uP_i8008_ioports[i8008_txd_outport] & 0x01);
}

inline __attribute__((always_inline))
void write_uP_RXD_pin_i8008(bool value)
{
  if (value)
    uP_i8008_ioports[i8008_rxd_inport] = 0x01;
  else
    uP_i8008_ioports[i8008_rxd_inport] = 0x00;
}

////////////////////////////////////////////////////////////////////
// Setup
////////////////////////////////////////////////////////////////////

void setup() 
{
#if   (ARDUINO_AVR_MEGA2560)  
  Serial.begin(115200);
#elif (ARDUINO_TEENSY35 || ARDUINO_TEENSY36 || ARDUINO_TEENSY41) 
  Serial.begin(0);
  while (!Serial);         // Wait for serial on Teensy.
#endif
  
  Serial.write(27);       // ESC command
  Serial.print("[2J");    // clear screen command
  Serial.write(27);
  Serial.print("[H");
  Serial.println("\n");
  Serial.println("Configuration:");
  Serial.println("==============");
  print_teensy_version();
  Serial.print("Debug:      ");   Serial.println(outputDEBUG, HEX);
  Serial.print("--------------"); Serial.println();
#ifdef ROM1
  Serial.print("ROM1 Size:   ");   Serial.print(ROM1_END - ROM1_START + 1, DEC); Serial.println(" Bytes");
  Serial.print("ROM1_START:  0x"); Serial.println(ROM1_START, HEX); 
  Serial.print("ROM1_END:    0x"); Serial.println(ROM1_END, HEX);
#endif
#ifdef ROM2
  Serial.print("ROM2 Size:   ");   Serial.print(ROM2_END - ROM2_START + 1, DEC); Serial.println(" Bytes");
  Serial.print("ROM2_START:  0x"); Serial.println(ROM2_START, HEX); 
  Serial.print("ROM2_END:    0x"); Serial.println(ROM2_END, HEX);
#endif
  Serial.print("--------------"); Serial.println(); 
#ifdef RAM1_START
  Serial.print("SRAM1 Size:  ");   Serial.print(RAM1_END - RAM1_START + 1, DEC); Serial.println(" Bytes");
  Serial.print("SRAM1_START: 0x"); Serial.println(RAM1_START, HEX); 
  Serial.print("SRAM1_END:   0x"); Serial.println(RAM1_END, HEX); 
#endif
#ifdef RAM2_START
  Serial.print("SRAM2 Size:  ");   Serial.print(RAM2_END - RAM1_START + 1, DEC); Serial.println(" Bytes");
  Serial.print("SRAM2_START: 0x"); Serial.println(RAM2_START, HEX); 
  Serial.print("SRAM2_END:   0x"); Serial.println(RAM2_END, HEX); 
#endif
  Serial.print("--------------"); Serial.println(); 
  Serial.println();
  Serial.println("=======================================================");
  Serial.println("; Intellec 8/Mod 8 Monitor, Version 3.0, 14 April 1975");
  Serial.println(";                                                     ");
  Serial.println("; Copyright (C) 1975                                  ");
  Serial.println("; Intel Corporation                                   ");
  Serial.println("; 3065 Bowers Avenue                                  ");
  Serial.println("; Santa Clara, CA 95051                               ");
  Serial.println(";                                                     ");
  Serial.println("; 8BitForce                                           ");
  Serial.println("; =========                                           ");
  Serial.println("; Monitor source code from pdf file, located at:  ");
  Serial.println("; https://mark-ogden.uk/files/intel/publications/98-022B%20intellec%208-MOD%208%20MONITOR%20V3.0-Apr75.pdf");
  Serial.println(";");
  Serial.println("=======================================================");
  Serial.flush();

  uP_init();
  board_init();
  intellec_init();
  DBG_LED_Init(LED_DEBUG);

  // // Enable soft UART
  // soft_uart_setup(2400, 250000, read_uP_TXD_pin_i8008, write_uP_RXD_pin_i8008, 1/1);    // Code sends every cpu clock; 1/1 sampling rate
  // //                    ^^^
  // //                    500kHz / 2 because cpu_tick() advances two system clock cycles.
  // //

  Serial.println("RESET Sequence - Initiate.");

  // 8008 Initialization
  // - set pins to known state.
  // - feed clock until we synchronize on SYNC signal.
  // - feed a few more clocks until we assert interrupt.
  // - assert interrupt and jam RST00 instruction.
  //
  uP_assert_reset();
  while (!digitalReadFast(uP_SYNC))
  {
    cpu_clock_advance();
    /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
    /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
    /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
    /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
    /*100ns*/ DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS(); DELAY_UNIT_25NS();
  }
  for(int i=0;i<20;i++)       // Let 8008 reset internally.
    cpu_tick();
  i8008_assert_interrupt();   // first interrupt kicks off 8008

}

////////////////////////////////////////////////////////////////////
// Loop
////////////////////////////////////////////////////////////////////

void loop()
{
  while(1)
  {
    cpu_tick();

    // UART
    serialEventIntellec(false);
    // soft_uart_loop();

    // LED Debug handler
    DBG_LED_Loop(LED_DEBUG);

    if (is_reset_btn_pressed())
    {
      board_init();               // copy first 20bytes from ROM to RAM for bootstrapping
      i8008_assert_interrupt();   // Reset is done by triggering interrupt
    }
  }
}
