; ================================================================================
; 8BitForce
; 
; Original monitor code PDF:
; https://mark-ogden.uk/files/intel/publications/98-022B%20intellec%208-MOD%208%20MONITOR%20V3.0-Apr75.pdf
;
; Monitor code itself is Copyright (C) 1975 Intel Corporation.
;
;            The contents of this text file belongs to Intel. However,
;            For historical and educational purposes, I assume Intel is ok
;            for us (community) to use the resulting binary code.
;
; 2025-01-24 I hand-typed this text from the original in pdf.
;            My goal was to generate the binary for Retroshield 8008,
;            so 99.99% of the comments are not included.
;
;            You are free to use this text file for any purpose, including
;            commercial use. No need to mention my name. As always,
;            use at your own risk; no guarantee nor warranty provided :)
;
;            If you want the full text and don't need the commercial license,
;            see the link below. Author released the file as CC BY-NC.
;            I don't satisfy that license hence hand-typed my version.
;
;            https://github.com/ComputingMongoose/Simulations/tree/main
; 
; Use asmx to compile
; http://xi6.com/projects/asmx/
; ================================================================================

 
; ================================================================================
; Intellec 8/Mod 8 Monitor, Version 3.0, 14 April 1975
;
; Copyright (C) 1975
; Intel Corporation
; 3065 Bowers Avenue
; Santa Clara, CA 95051
;
; ================================================================================


        CPU 8008
	OPT LIST
	OPT MACRO
	OPT EXPAND
	OPT SYM
	OPT NOOPT

VER     EQU 30

FALSE   EQU (0)
TRUE    EQU (~FALSE)
DEBUG   EQU false               ; Debugged for the 8080 Version 1.0 Monitor

        if DEBUG
BIAS    EQU 0
        else
BIAS    equ 8
        endif

        ; I/O Command Constants
RBIT    equ 1
PCMD    equ 2
RCMD    equ 4
DSB     equ 8
PBITA   equ 80H

        ; TTY I/O Constants
TTI     equ 0
TTO     equ BIAS+0
TTS     equ 1
TTC     equ BIAS+1
TTYGO   equ RBIT | DSB
TTYNO   equ DSB
TTYDA   equ 1
TTYBE   equ 4

        ; CRT I/O Constants
CRTI    equ 4
CRTS    equ 5
CRTO    equ BIAS+4
CRTDA   equ 1
CRTBE   equ 4

        ; PTR I/O Constants
PTRI    equ 3
PTRS    equ TTS
PTRC    equ TTC
PTRGO   equ RCMD | DSB
PTRNO   equ TTYNO
PTRDA   equ 20H

        ; PTP I/O Constants
PTPO    equ BIAS+3
PTPS    equ TTS
PTPC    equ TTC
PRDY    equ 40H
PTPGO   equ PCMD | DSB
PTPNO   equ TTYNO

        ; PROM Programmer Constants
PAD     equ BIAS+2
PDO     equ PTPO
PDI     equ 2
PROMC   equ TTC
PROGO   equ PBITA
PRONO   equ 0
ENB     equ 0

        ; Global Constants
TOUT    equ 250                 ; 250ms counter for readeer timeout
LDLY    equ 20                  ; counnter for 20ms delay
        if DEBUG == true
DLY     equ 111                 ; 1ms delay for 8080 debug
        else
DLY     equ 23
        endif
CR      equ 0DH
LF      equ 0AH

        ; Skip Macro definition
FIRST   SET TRUE
MODIO   MACRO TABLE,MASK
        LXI D,TABLE
        MVI C,MASK
        IF FIRST
IOMOD:
FIRST   SET FALSE
        CALL NOISE

        MVI B,4
        MOV H,D
        MOV L,E
        CALL TEST
        CALL INCHL
        MOV E,M
SCANOUT:
        CALL TI
        CPI CR
        JNZ SCANOUT
        LXI H,IOBYT
        MOV A,M
        ANA C
        ORA E
        MOV M,A
        JMP START
TEST:
        CMP M
        RZ
        CALL INCHL
        CALL INCHL
        DCR B
        JNZ TEST
        JMP LER
        ENDIF
        IF (!FIRST)
        JMP IOMOD
        ENDIF
        ENDM

        ; I/O Status Byte Masks and Values
CMSK    equ %11111100 ; Console
RMSK    equ %11110011 ; Reader
PMSK    equ %11001111 ; Punch
LMSK    equ %00111111 ; List

CTTY    equ %00000000 ; TTY
CCRT    equ %00000001 ; CRT
BATCH   equ %00000010 ; BATCH (in=Reader, Out=List)

CUSE    equ %00000011 ; User
RTTY    equ %00000000 ; Reader=TTY
RPTR    equ %00000100 ; Reader=PTR
RUSE1   equ %00001000 ; Reader=User1
RUSE2   equ %00001100 ; Reader=User2
PTTY    equ %00000000 ; Punch=TTY
PPTP    equ %00010000 ; Punch=PTP
PUSE1   equ %00100000 ; Punch=User1
PUSE2   equ %00110000 ; Punch=User2
LTTY    equ %00000000 ; List=TTY
LCRT    equ %01000000 ; List=CRT
LUSE1   equ %10000000 ; List=User1
LUSE2   equ %11000000 ; List=User2

        ; User Defined Device Entry Points
CILOC   equ 3700H ; User Console 1
COLOC   equ 3703H ; User Console 2
R1LOC   equ 3706H ; User Reader 1
R2LOC   equ 3709H ; User Reader 2
P1LOC   equ 370CH ; User Punch 1
P2LOC   equ 370FH ; User Punch 2
L1LOC   equ 3712H ; User List 1
L2LOC   equ 3715H ; User List 2
CSLOC   equ 3718H ; User Console Status

        ; Pointers to RAM
        if (DEBUG == FALSE)
        org 3
        else
        org $100
        endif
        
IOBYT   DS 1  ; I/O Statue Byte
HPRIME  DS 2  ; Temp for HL
CBRCH   DS 2  ; Case Branch Location
ADR1    DS 2  ; First Parameter
ADR2    DS 2  ; Second Parameter
ADR3    DS 2  ; Third Parameter

        if DEBUG == TRUE
        org 0800H
        else
        org 3800H
        endif
        
        jmp BEGIN       ; Reset 
        jmp CI          ; Console Input
        jmp RI          ; Reader Input
        jmp CO          ; Console Output
        jmp PO          ; Punch Output
        jmp LO          ; List Output
        jmp CSTS        ; Console Input Status
        jmp IOCHK       ; I/O System Status
        jmp IOSET       ; Set I/O Configuration
        jmp MEMCK       ; Compute size of memory

        ; Initial conditions for I/O system
INIT    equ 0           ; CONSOLE=READER=PUNCH=LIST = TTY

VERS    DB CR, LF, "8008 V"
        if DEBUG == FALSE
        DB (VER/10)+$30, '.', (VER % 10)+$30
        else
        DB "X.X"
        endif
LVER    equ $-VERS

        ; Program Entry Point
        
BEGIN:
        LXI H, IOBYT
        MVI M,INIT      ; Initial value of I/O Status Byte

        LXI H, VERS    ; Pointer to version string
        MVI C, LVER    ; Length of version string
VER0:
        MOV B,M
        CALL INCHL
        MOV D,H
        MOV E,L
        CALL CO
        MOV H,D
        MOV L,E
        DCR C
        JNZ VER0


	; Main Command Loop
START:
	MVI A, TTYNO
	OUT TTC
	CALL CRLF
	MVI B,'.'
	CALL CO
	CALL TI
	SUI 'A'
	JM START
	CPI 'W'-'A'+1
	JP LER
	ADD A
	LXI H,TBL
	ADD L
	MOV L, A
	MOV E,M
	INR L
	MOV D,M
GO:
	LXI H,CBRCH
	MVI M,44H	; JMP 0 opcode
	INR L
	MOV M,E
	INR L
	MOV M,D
	MVI C,2
	JMP CBRCH

	; Command Branch Table
TBL:
	DW ASSIGN
	DW BNPF
	DW COMP
	DW DISP
	DW EOF
	DW FILL
	DW GOTO
	DW HEXN
	DW LER	; I
	DW LER	; J
	DW LER	; K
	DW LOAD
	DW MOVE
	DW NULL
	DW LER	; O
	DW PROG
	DW LER	; Q
	DW READ
	DW SUBS
	DW TRAN
	DW LER	; U
	DW LER	; V
	DW WRITE

	; Process I/O Device Assignment Comands

ASSIGN:
	CALL TI
	CPI 'C'
	JNZ AS0
	MODIO ICT,CMSK
AS0:
	CPI 'R'
	JNZ AS1
	MODIO IRT,RMSK
AS1:
	CPI 'P'
	JNZ AS2
	MODIO OPT,PMSK
AS2:
	CPI 'L'
	JNZ LER
	MODIO OLT,LMSK

	; Punch a BNPF Tape
BNPF:
	CALL EXPR
	CALL CRLF
	CALL LEAD
BN0:
	CALL PEOL
	CALL GETAD
	MVI C,' '
	LXI D, 10000
	CALL DIGIT
	LXI D, 1000
	CALL DIGIT
	LXI D, 100
	CALL DIGIT
	LXI D, 10
	CALL DIGIT
	LXI D, 1
	MVI C,'0'
	CALL DIGIT
	MVI B, ' '
	CALL PO
	CALL GETAD
BN1:
	CALL ENCODE
	CALL GETAD
	CALL HILO
	JC NULL
	CALL SAVIT
	CALL GETAD
	MOV A,L
	ANI 03H
	JNZ BN1
	JMP BN0

	; COMPARE PROM WITH MEMORY
COMP:
	DCR C
	CALL EXPR
	CALL CRLF
	CALL GETAD
	MVI C,0
	MVI A,ENB
	OUT PROMC
CM0:
	MOV A,C
	XRI FFH
	OUT PAD
	CALL DELAY
	IN PDI
	XRI FFH
	CMP M
	JZ CM1
	MOV E,C
	CALL SAVIT
	CALL GETAD
	CALL LADR
	MOV A,M
	CALL LBYTE
	CALL BLK
	IN PDI
	XRI FFH
	CALL LBYTE
	CALL CRLF
	CALL GETAD
	MOV C,E
CM1:
	CALL INCHL
	INR C
	JNZ CM0
	JMP START

	; DISPLAY MEMORY IN HEX ON CONSOLE DEVICE
DISP:
	CALL EXPR
DI0:
	CALL CRLF
	CALL GETAD
	CALL LADR
DI1:
	MOV A,M
	CALL LBYTE
	CALL BLK
	CALL GETAD
	CALL HILO
	JC START
	CALL SAVIT
	CALL GETAD
	MOV A,L
	ANI 0FH
	JNZ DI1
	JMP DI0

	; END OF FILE COMMAND
EOF:
	DCR C
	CALL EXPR
	CALL PEOL
	MVI B,':'
	CALL PO
	XRA A
	MOV D,A
	CALL PBYTE
	MOV A,D
	CALL GETAD
	MOV D,A
	MOV E,L
	MOV A,H
	CALL PBYTE
	MOV A,E
	CALL PBYTE
	MVI A,1
	CALL PBYTE
	XRA A
	SUB D
	CALL PBYTE
	JMP NULL

	; FILL RAM MEMORY BLOCK WITH CONSTANT
FILL:
	INR C
	CALL EXPR
	MOV A,E
	CALL GETAD
	MOV B,A
FI0:
	MOV M,B
	CALL HILO
	JNC FI0
	JMP START

	; GOTO ADDRESS
GOTO:
	DCR C
	CALL EXPR
	JMP GO

	; COMPUTE HEXADECIMAL SUM AND DIFFERENCE
HEXN:
	CALL EXPR
	CALL CRLF
	CALL GETAD
	MOV A,L
	ADD E
	MOV E,A
	MOV A,H
	ADC D
	CALL LBYTE
	MOV A,E
	CALL LBYTE
	CALL BLK
	CALL GETAD
	MOV A,L
	SUB E
	MOV E,A
	MOV A,H
	SBB D
	MOV H,A
	CALL LBYTE
	MOV A,E
	CALL LBYTE
	JMP START

	; LOAD A BNPF TAPE INTO RAM MEMORY
LOAD:
	CALL EXPR
	CALL CRLF
LO0:
	CALL DECODE
	MOV A,C
	CALL GETAD
	MOV M,A
	CALL HILO
	JC START
	CALL SAVIT
	JMP LO0

	; MOVE A BLOCK OF RAM MEMORY
MOVE:
	INR C
	CALL EXPR
MV0:
	CALL GETAD
	MOV A,M
	LXI H,ADR3
	MOV B,M
	INR L
	MOV L,M
	MOV H,B
	MOV M,A
	CALL INCHL
	MOV B,H
	MOV C,L
	LXI H,ADR3
	MOV M,B
	INR L
	MOV M,C
	CALL GETAD
	CALL HILO
	JC START
	CALL SAVIT
	JMP MV0

	; PUNCH LEADER OR TRAILER
NULL:
	CALL LEAD
	JMP START

	; PROGRAM A 1702A PROM WITH FAST ALGORITHM
PROG:
	INR C
	CALL EXPR
	DCR L
	MOV C,M
	DCR L
	DCR L
	MOV E,M
	DCR L
	MOV D,M
	DCR L
	MOV B,M
	DCR L
	MOV H,M
	MOV L,B
	MOV A,E
	SUB L
	MOV E,A
	INR E
	MOV A,D
	SBB H
	JC LER
PR0:
	MVI A,ENB
	OUT PROMC
	MOV A,C
	XRI FFH
	OUT PAD
	IN PDI
	XRI FFH
	CMP M
	JZ PR4
	MVI D,-16
PR1:
	CALL PGRM
	IN PDI
	XRI FFH
	CMP M
	JZ PR2
	INR D
	JNZ PR1
	CALL CRLF
	MVI B,'$'
	CALL CO
	CALL BLK
	MOV A,C
	CALL LBYTE
	JMP LER
PR2:
	MOV A,D
	ADI 17
	ADD A
	ADD A
	MOV D,A
PR3:
	CALL PGRM
	DCR D
	JNZ PR3
PR4:
	INR C
	JZ START
	CALL INCHL
	DCR E
	JNZ PR0
	JMP START

	; READ ROUTINE
READ:
	DCR C
	CALL EXPR
RED0:
	CALL RIX
	MVI B,':'
	SUB B
	JNZ RED0
	MOV D,A
	CALL BYTE
	JZ RED2
	MOV E,A
	CALL BYTE
	LXI H,ADR2
	MOV M,A
	CALL BYTE
	LXI H,ADR2+1
	MOV M,A
	CALL BYTE
	LXI H,ADR2+1
	MOV A,M
	MVI L,(ADR1+1) & FFH
	ADD M
	MOV C,A
	DCR L
	MOV A,M
	MVI L,ADR2 & FFH
	ADC M
	LXI H,HPRIME
	MOV M,A
	INR L
	MOV M,C
RED1:
	CALL BYTE
	LXI H,HPRIME
	MOV C,M
	INR L
	MOV L,M
	MOV H,C
	MOV M,A
	CALL INCHL
	MOV B,H
	MOV C,L
	LXI H,HPRIME
	MOV M,B
	INR L
	MOV M,C
	DCR E
	JNZ RED1
	CALL BYTE
	JNZ LER
	JMP RED0
RED2:
	CALL BYTE
	MOV E,A
	CALL BYTE
	MOV D,E
	MOV E,A
	ORA D
	JNZ GO
	JMP START

	; SUBSTITUE MEMORY CONTENTS ROUTINE
SUBS:
	DCR C
	CALL EXPR
	CPI CR
	JZ START
	CALL GETAD
SU0:
	MOV A,M
	CALL LBYTE
	MVI B,'-'
	CALL CO
	CALL TI
	CPI ' '
	JZ SU1
	CPI ','
	JZ SU1
	CPI CR
	JZ START
	MVI C,1
	MVI E,0
	LXI H,HPRIME
	MVI M,(ADR2>>8)
	INR L
	MVI M,(ADR2 & FFH)
	CALL EX1		; todo
	CALL GETAD
	MOV M,E
	CPI CR
	JZ START
SU1:
	CALL GETAD
	CALL INCHL
	CALL SAVIT
	CALL GETAD
	JMP SU0

	; TRANSFER CONTENTS OF A PROM TO MEMORY
TRAN:
	DCR C
	CALL EXPR
	MVI A,ENB
	OUT PROMC
	CALL GETAD
	MVI E,0
TR0:
	MOV A,E
	XRI FFH
	OUT PAD
	CALL DELAY
	IN PDI
	XRI FFH
	MOV M,A
	CALL INCHL
	INR E
	JNZ TR0
	JMP START

	; WRITE ROUTINE
WRITE:
	CALL EXPR
WRI0:
	CALL GETAD
	MOV A,L
	ADI 16
	MOV C,A
	MOV A,H
	ACI 0
	MOV B,A
	MOV A,E
	SUB C
	MOV C,A
	MOV A,D
	SBB B
	JM WRI1
	MVI A,16
	JMP WRI2
WRI1:
	MOV A,C
	ADI 17
WRI2:
	ORA A
	JZ START
	MOV E,A
	MVI D,0
	CALL PEOL
	MVI B,' '
	CALL PO
	MVI B,':'
	CALL PO
	MOV A,E
	CALL PBYTE
	LXI H,ADR1
	MOV A,M
	CALL PBYTE
	LXI H,ADR1+1
	MOV A,M
	CALL PBYTE
	XRA A
	CALL PBYTE
WRI3:
	LXI H,ADR1
	MOV A,M
	INR L
	MOV L,M
	MOV H,A
	MOV A,M
	CALL INCHL
	MOV B,H
	MOV C,L
	LXI H,ADR1
	MOV M,B
	INR L
	MOV M,C
	CALL PBYTE
	DCR E
	JNZ WRI3
	XRA A
	SUB D
	CALL PBYTE
	JMP WRI0

	; ERROR EXIT
LER:
	MVI B,'*'
	CALL CO
	JMP START

	; SUBROUTINES

BLK:
	MVI B, ' '
	
	; CONSOLE OUTPUT
CO:
	LXI H,IOBYT
	MOV A,M
	ANI ~CMSK
	JNZ CO0
TTYOUT:
	IN TTS
	ANI TTYBE
	JNZ TTYOUT
	MOV A,B
	XRI FFH
	OUT TTO
	RET
CO0:
	CPI CCRT
	JNZ CO1
CRTOUT:
	IN CRTS
	ANI CRTBE
	JNZ CRTOUT
	MOV A,B
	XRI FFH
	OUT CRTO
	RET
CO1:
	CPI BATCH
	JZ LO
	JMP COLOC

	; READ TWO ASCII CHARACTERS
BYTE:
	CALL RIX
	CALL NIBBLE
	RLC
	RLC
	RLC
	RLC
	MOV C,A
	CALL RIX
	CALL NIBBLE
	ORA C
	MOV C,A
	ADD D
	MOV D,A
	MOV A,C
	RET

	; CONSOLE INPUT
CI:
	LXI H,IOBYT
	MOV A,M
	ANI ~CMSK
	JNZ CI1
TTYIN:
	IN TTS
	ANI TTYDA
	JNZ TTYIN
	IN TTI
CI0:
	XRI FFH
	RET
CI1:
	CPI CCRT
	JNZ CI2
CRTIN:
	IN CRTS
	ANI CRTDA
	JNZ CRTIN
	IN CRTI
	JMP CI0
CI2:
	CPI BATCH
	JZ RI
	JMP CILOC
	
	; CONVERT 4BIT HEX VALUE TO ASCII CHARACTER
CONV:
	CPI 10
	JM CN0
	ADI 'A'-'0'-10
CN0:
	ADI '0'
	MOV B,A
	RET

	; TYPE CARRIAGE RETURN AND LINEFEED
CRLF:
	MVI B,CR
	CALL CO
	MVI B,LF
	JMP CO

	; CONSOLE INPUT STATUS CODE
CSTS:
	LXI H,IOBYT
	MOV A,M
	ANI ~CMSK
	JNZ CS0
	IN TTS
	JMP CS1
CS0:
	CPI CCRT
	JNZ CS3
	IN CRTS
CS1:
	ANI TTYDA
	MVI A,FALSE
CS2:
	RNZ		; RETURN ON ZERO
	XRI FFH
	RET
CS3:
	CPI BATCH
	MVI A,TRUE
	JZ CS2
	JMP CSLOC

	; READ BNPF TAPE RECORD
DECODE:
	CALL RIX
	CPI 'B'
	JNZ DECODE
	MVI C,1
DC0:
	CALL RIX
	CPI 'N'
	JNZ DC2
DC1:
	MOV A,C
	RAL
	MOV C,A
	JNC DC0
	CALL RIX
	CPI 'F'
	JNZ LER
	RET
DC2:
	ADI -('P')
	JNZ LER
	JMP DC1

	; 1.0MS DELAY
DELAY:
	MVI B,DLY
DL0:
	DCR B
	JNZ DL0
	RET

	; CONVERT BINARY NUMBER TO A STRING OF ASCII DIGITS
DIGIT:
	MVI B,'0'
DG0:
	MOV A,L
	SUB E
	MOV L,A
	MOV A,H
	SBB D
	MOV H,A
	JC DG1
	INR B
	JMP DG0
DG1:
	MOV A,L
	ADD E
	MOV L,A
	MOV A,H
	ADC D
	MOV H,A
	MOV A,B
	CPI '0'
	JNZ DG3
	MOV B,C
DG2:
	MOV D,H
	MOV E,L
	CALL PO
	MOV H,D
	MOV L,E
	RET
DG3:
	MVI C,'0'
	JMP DG2

	; ENCODE A BPNF WORD AND PUNCH IT
ENCODE:
	MOV D,H
	MOV E,L
	MVI B,'B'
	CALL PO
	MVI C,8
EN0:
	MOV H,D
	MOV L,E
	MVI A,9
	SUB C
	MOV B,A
	MOV A,M
EN1:
	RLC
	DCR B
	JNZ EN1
	JNC EN3
	MVI B,'P'
EN2:
	CALL PO
	DCR C
	JNZ EN0
	MVI B,'F'
	CALL PO
	MVI B,' '
	JMP PO
EN3:
	MVI B,'N'
	JMP EN2

	; EVALUATE EXPRESSION
EXPR:
	MOV D,H
	MOV E,L
	LXI H,HPRIME
	MOV M,D
	INR L
	MOV M,E
	MVI D,0
	MOV E,D
EX0:
	CALL TI
EX1:
	MOV B,A
	CALL NIBBLE
	JC EX2
	MOV B,A
	CALL SLDE
	CALL SLDE
	CALL SLDE
	CALL SLDE
	MOV A,B
	ORA E
	MOV E,A
	JMP EX0
EX2:
	LXI H,HPRIME
	MOV A,M
	INR L
	MOV L,M
	MOV H,A
	MOV M,D
	INR L
	MOV M,E
	INR L
	MOV A,B
	CPI ','
	JZ EX3
	CPI ' ' 
	JZ EX3
	CPI CR
	JNZ LER
	DCR C
	JNZ LER
	RET
EX3:
	DCR C
	JNZ EXPR
	RET

	; GET ADDRESS FROM MEMORY AND PUT IN HL AND DE
GETAD:
	LXI H,ADR1
	MOV B,M
	INR L
	MOV C,M
	INR L
	MOV D,M
	INR L
	MOV E,M
	MOV H,B
	MOV L,C
	RET

	; COMPARE HL WITH DE
HILO:
	CALL INCHL
	MOV A,E
	SUB L
	MOV A,D
	SBB H
	RET

	; CONVERT NIBBLE IN A-REGISTER TO ASCII IN A-REGISTER
HXD:
	CALL CONV
	JMP CO

	; INCREMENT H AND L
INCHL:
	INR L
	RNZ
	INR H
	RET

	; I/O SYSTEM STATUS CODE
IOCHK:
	LXI H,IOBYT
	MOV A,M
	RET

	; SET I/O CONFIGURATION
IOSET:
	LXI H,IOBYT
	MOV M,B
	RET

	; PRINT CONTENTS OF HL IN HEX ON CONSOLE DEVICE
LADR:
	MOV D,H
	MOV E,L
	MOV A,D
	CALL LBYTE
	MOV A,E
	CALL LBYTE
	CALL BLK
	MOV H,D
	MOV L,E
	RET

	; LIST A BYTE AS 2 ASCII CHARACTERS
LBYTE:
	MOV C,A
	RRC
	RRC
	RRC
	RRC
	ANI 0FH
	CALL HXD
	MOV A,C
	ANI 0FH
	JMP HXD

	; PUNCH 6 INCHES OF LEADER
LEAD:
	MVI C,60
LE0:
	MVI B,0
	CALL PO
	DCR C
	JNZ LE0
	RET

	; LIST OUTPUT CODE
LO:
	LXI H,IOBYT
	MOV A,M
	ANI ~LMSK
	JZ TTYOUT
	CPI LCRT
	JZ CRTOUT
	CPI LUSE1
	JZ L1LOC
	JMP L2LOC

	; RETURN ADDRESS OF END OF MEMORY TO USER
MEMCK:
	LXI H,0
M0:
	MOV B,M
	MVI M,AAH
	MOV A,M
	MOV M,B
	INR H
	CPI AAH
	JZ M0
	DCR H
	DCR H
	DCR L
	MOV A,L
	MOV C,H
	RET

	; DECODE ASCII IN A-REGISTER INTO HEX DIGIT IN A-REGISTER
NIBBLE:
	SUI '0'
	RC
	ADI '0'-'G'
	RC
	ADI 6
	JP NI0
	ADI 7
	RC
NI0:
	ADI 10
	ORA A
	RET

	; DISREGARD NOISE CHARACTERS
NOISE:
	CALL TI
	CPI '='
	JNZ NOISE
NO0:
	CALL TI
	CPI ' '
	JZ NO0
	RET

	; PUNCH A BYTE AS 2 ASCII CHARACTERS
PBYTE:
	MOV C,A
	RRC
	RRC
	RRC
	RRC
	ANI 0FH
	CALL CONV
	CALL PO
	MOV A,C
	ANI 0FH
	CALL CONV
	CALL PO
	MOV A,C
	ADD D
	MOV D,A
	RET

	; PUNCH CR,LF
PEOL:
	MVI B,CR
	CALL PO
	MVI B,LF
	JMP PO

	; PULSE A PROM LOCATION
PGRM:
	MOV A,M
	XRI FFH
	OUT PDO
	MVI A,PROGO
	OUT PROMC
	MVI A,PRONO
	OUT PROMC
	MOV A,D
	MVI D,LDLY
PG0:
	CALL DELAY
	DCR D
	JNZ PG0
	MOV D,A
	RET

	; PUNCH OUTPUT CODE
PO:
	LXI H,IOBYT
	MOV A,M
	ANI ~PMSK
	JZ TTYOUT
	CPI PPTP
	JNZ PO1
PO0:
	IN PTPS
	ANI PRDY
	JZ PO0
	MOV A,B
	OUT PTPO
	MVI A,PTPGO
	OUT PTPC
	MVI A,PTPNO
	OUT PTPC
	RET
PO1:
	CPI PUSE1
	JZ P1LOC
	JMP P2LOC

	; READ INPUT CODE
RI:
	LXI H,IOBYT
	MOV A,M
	ANI ~RMSK
	JNZ RI3
	MVI A,TTYGO
	OUT TTC
	MVI A,TTYNO
	OUT TTC
	MVI H,TOUT
RI0:
	IN TTS
	ANI TTYDA
	JZ RI2
	CALL DELAY
	DCR H
	JNZ RI0
RI1:
	ORA A
	MVI A,1
	RAR
	RET
RI2:
	IN TTI
	XRI FFH
	RET
RI3:
	CPI RPTR
	JNZ RI6
	MVI A,PTRGO
	OUT PTRC
	MVI A,PTRNO
	OUT PTRC
	MVI H,TOUT
RI4:
	IN PTRS
	ANI PTRDA
	JNZ RI5
	CALL DELAY
	DCR H
	JNZ RI4
	JMP RI1
RI5:
	IN PTRI
	ORA A
	RET
RI6:
	CPI RUSE1
	JZ R1LOC
	JMP R2LOC

	; GET CHARACTER FROM READER
RIX:
	CALL RI
	JC LER
	ANI 7FH
	RET

	; SAVE HL AND DE IN MEMORY
SAVIT:
	MOV B,H
	MOV C,L
	LXI H,ADR1
	MOV M,B
	INR L
	MOV M,C
	INR L
	MOV M,D
	INR L
	MOV M,E
	RET

	; SHIFT DE LEFT 1 PLACE
SLDE:
	MOV A,E
	ADD A
	MOV E,A
	MOV A,D
	ADC A
	MOV D,A
	RET

	; INPUT FROM CONSOLE W/ ECHO
TI:
	CALL CI
	ANI 7FH
	MOV B,A
	CALL CO
	MOV A,B
	RET

	; I/O SYSTEM PHYSICAL DEVICE TABLES
ICT:
	DB 'T', CTTY
	DB 'C', CCRT
	DB 'B', BATCH
	DB '1', CUSE
IRT:
	DB 'T', RTTY
	DB 'P', RPTR
	DB '1', RUSE1
	DB '2', RUSE2
OPT:
	DB 'T', PTTY
	DB 'P', PPTP
	DB '1', PUSE1
	DB '2', PUSE2
OLT:
	DB 'T', LTTY
	DB 'C', LCRT
	DB '1', LUSE1
	DB '2', LUSE2

	END
















