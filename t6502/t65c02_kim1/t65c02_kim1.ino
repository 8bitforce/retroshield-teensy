////////////////////////////////////////////////////////////////////
// RetroShield 6502 for Teensy 3.5
// KIM 1
//
// 2019/04/25
// Version 0.1

// The MIT License (MIT)

// Copyright (c) 2019 Erturk Kocalar, 8Bitforce.com

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
// Date         Comments                                            Author
// -----------------------------------------------------------------------------
// 03/10/2025   Added Teensy 4.1 support.                           Erturk

////////////////////////////////////////////////////////////////////
// Options
//   outputDEBUG: Print memory access debugging messages.
////////////////////////////////////////////////////////////////////
#define outputDEBUG       0
#define DEBUG_SPEW_DELAY  1000
unsigned long clock_cycle_count;

////////////////////////////////////////////////////////////////////
// BOARD DEFINITIONS
////////////////////////////////////////////////////////////////////

#include "memorymap.h"      // Memory Map (ROM, RAM, PERIPHERALS)
#include "portmap.h"        // Pin mapping to cpu
#include "setuphold.h"      // Delays required to meet setup/hold
#include "6530.h"           // 6530 Emulation


void uP_assert_reset()
{
  // Drive RESET conditions
  digitalWriteFast(uP_RESET_N,  LOW);
  digitalWriteFast(uP_IRQ_N,    HIGH);
  digitalWriteFast(uP_NMI_N,    HIGH);
  digitalWriteFast(uP_RDY,      HIGH);
  digitalWriteFast(uP_SO_N,     HIGH);
}

void uP_release_reset()
{
  // Drive RESET conditions
  digitalWriteFast(uP_RESET_N,  HIGH);
}

void uP_assert_nmi()
{
  // Drive RESET conditions
  digitalWrite(uP_NMI_N, LOW);
}

void uP_release_nmi()
{
  // Drive RESET conditions
  digitalWrite(uP_NMI_N, HIGH);
}

void uP_init()
{
  // Set directions for ADDR & DATA Bus.
 
  configure_PINMODE_ADDR();
  configure_PINMODE_DATA();

  pinMode(uP_RESET_N, OUTPUT);
  pinMode(uP_RW_N,    INPUT);
  pinMode(uP_RDY,     OUTPUT);
  pinMode(uP_SO_N,    OUTPUT);
  pinMode(uP_IRQ_N,   OUTPUT);
  pinMode(uP_NMI_N,   OUTPUT);
  pinMode(uP_CLK_E,   OUTPUT);
  pinMode(uP_GPIO,    INPUT);
    
  digitalWriteFast(uP_CLK_E, LOW); 
  uP_assert_reset();

  clock_cycle_count = 0;    
}

void kim_init() {       
  // 2025-0310: We are going to modify the ROM before 6502 starts executing
  // we usually speed up or optimize uart routines.
  //

  // Modify ROM to work w/ RetroShield
             
  // this is what user has to enter manually when powering KIM on. Why not do it here.
  RAM002[(0x17FA)-(0x17C0)]=0x00;
  RAM002[(0x17FB)-(0x17C0)]=0x1C;
  RAM002[(0x17FE)-(0x17C0)]=0x00;
  RAM002[(0x17FF)-(0x17C0)]=0x1C;
  
  // Skip TTY timing =====
  // JMP $1C4F    4C 4F 1C
  //
  rom002_bin[(0x1C2A)-(0x1C00)]= 0x4C;
  rom002_bin[(0x1C2B)-(0x1C00)]= 0x4F;
  rom002_bin[(0x1C2C)-(0x1C00)]= 0x1C;

  // skip "1C57 BNE TTYKB" upon NMI/IRQ.
  rom002_bin[(0x1C57)-(0x1C00)]= 0xEA;    // NOP
  rom002_bin[(0x1C58)-(0x1C00)]= 0xEA;    // NOP


  // Modify OUTCH and GETCH to use Arduino Serial Port.
  // OUTCH = write A to reg002_B
  // GETCH = read A from reg002_A (use bit 7 to sync)

  // OUTCH ===============
  // STA $1742    8D 42 17 
  // RTS          60
  rom002_bin[(0x1EA4)-(0x1C00)]= 0x8D;
  rom002_bin[(0x1EA5)-(0x1C00)]= 0x42;
  rom002_bin[(0x1EA6)-(0x1C00)]= 0x17;
  rom002_bin[(0x1EA7)-(0x1C00)]= 0x60;

  // GETCH ===============
  // 1E5C LDY #$FFFF    A0 FF FF        ; for compatibility
  // WAIT LDA $1740     AD 40 17
  //      BPL WAIT      10 FB
  //      AND #$7F      29 7F
  //      STA $1740     8D 40 17
  //      RTS           60

  if (1)
  {
    // rom002_bin[(0x1E5C)-(0x1C00)]= 0xA0;
    // rom002_bin[(0x1E5D)-(0x1C00)]= 0xFF;
    // rom002_bin[(0x1E5E)-(0x1C00)]= 0xFF;    
    rom002_bin[(0x1E5C)-(0x1C00)]= 0xAD;
    rom002_bin[(0x1E5D)-(0x1C00)]= 0x40;
    rom002_bin[(0x1E5E)-(0x1C00)]= 0x17;
    rom002_bin[(0x1E5F)-(0x1C00)]= 0x10;
    rom002_bin[(0x1E60)-(0x1C00)]= 0xFB;
    rom002_bin[(0x1E61)-(0x1C00)]= 0x29;
    rom002_bin[(0x1E62)-(0x1C00)]= 0x7F;
    rom002_bin[(0x1E63)-(0x1C00)]= 0x8D;
    rom002_bin[(0x1E64)-(0x1C00)]= 0x40;
    rom002_bin[(0x1E65)-(0x1C00)]= 0x17;
    rom002_bin[(0x1E66)-(0x1C00)]= 0x60;
  }  
}

////////////////////////////////////////////////////////////////////
// Processor Control Loop
////////////////////////////////////////////////////////////////////
// This is where the action is.
// it reads processor control signals and acts accordingly.
//
word uP_ADDR;
byte uP_DATA;
byte DATA_OUT;
byte DATA_IN;


inline __attribute__((always_inline))
void cpu_tick()
{ 
  CLK_E_HIGH;           // E goes high   // digitalWrite(uP_E, HIGH);
  DELAY_FACTOR_H();

  // Ignore the top 3 bits of address bus (memory map replicated 8 times)
  uP_ADDR = ADDR() & 0b0001111111111111;
  
  if (STATE_RW_N)	  
  //////////////////////////////////////////////////////////////////
  // HIGH = READ
  {
    // change DATA port to output to uP:
    // DATA_DIR = DIR_OUT;
    xDATA_DIR_OUT();

    // Order the comparison from low to high memory for optimizations
    
    // ROM?
    if ( (ROM003_START <= uP_ADDR) && (uP_ADDR <= ROM003_END) )
      DATA_OUT = ROM003[(uP_ADDR - ROM003_START)];
    else
    if ( (ROM002_START <= uP_ADDR) && (uP_ADDR <= ROM002_END) )
      DATA_OUT = ROM002[(uP_ADDR - ROM002_START)];         // we modify ROM002
    else
    // RAM?
    if ( (uP_ADDR <= RAM_END) && (RAM_START <= uP_ADDR) )
      DATA_OUT = RAM[uP_ADDR - RAM_START];
    else
    // RAM002?
    if ( (uP_ADDR <= RAM002_END) && (RAM002_START <= uP_ADDR) )
      DATA_OUT = RAM002[uP_ADDR - RAM002_START];
    else
    // RAM003?
    if ( (uP_ADDR <= RAM003_END) && (RAM003_START <= uP_ADDR) )
      DATA_OUT = RAM003[uP_ADDR - RAM003_START];
    else
    // I/O Timer 003, I/O Timer 002
    if ( (uP_ADDR >= 0x1700) && (uP_ADDR <= 0x177F) )
      DATA_OUT = r6530_read(uP_ADDR);

    // I/O and timer of 6530-003, free for user   0x1700-0x173F
    // I/O and timer of 6530-002, used by KIM     0x1740-0x177F

    SET_DATA_OUT(DATA_OUT);
    DELAY_FOR_BUFFER();

#if (outputDEBUG)
    {
      char tmp[100];
      sprintf(tmp, "-- A=%04X D=%02X      %04X/%04X   %04X/%04X\n", 
        uP_ADDR, DATA_OUT,
        reg002_INT_TIMER, reg002_CLKRDT,
        reg003_INT_TIMER, reg003_CLKRDT);
      Serial.write(tmp);
    }
#endif

  } 
  else 
  //////////////////////////////////////////////////////////////////
  // R/W = LOW = WRITE
  {
    // Teensy to read data from 6502
    xDATA_DIR_IN();
    DELAY_FOR_BUFFER();       // should be unncessary since databus is already input when CLK_E = low.
    DATA_IN = xDATA_IN();

    // RAM?
    if ( (uP_ADDR <= RAM_END) && (RAM_START <= uP_ADDR) )
      RAM[uP_ADDR - RAM_START] = DATA_IN;
    else
    // RAM002?
    if ( (uP_ADDR <= RAM002_END) && (RAM002_START <= uP_ADDR) )
      RAM002[uP_ADDR - RAM002_START] = DATA_IN;
    else
    // RAM003?
    if ( (uP_ADDR <= RAM003_END) && (RAM003_START <= uP_ADDR) )
      RAM002[uP_ADDR - RAM003_START] = DATA_IN;
    else
    // I/O Timer 003
    if ( (uP_ADDR >= 0x1700) && (uP_ADDR <= 0x177F) )
      r6530_write(uP_ADDR, DATA_IN);
      
#if (outputDEBUG)
    {
      char tmp[100];
      sprintf(tmp, "WR A=%04X D=%02X      %04X/%04X   %04X/%04X\n", 
        uP_ADDR, DATA_IN, 
        reg002_INT_TIMER, reg002_CLKRDT,
        reg003_INT_TIMER, reg003_CLKRDT);
      Serial.write(tmp);
    }
#endif

  }

  // Wait until HIGH time is 500ns
  accurate_delay(NS_TO_TEENSY_CYCLE(500));   // wait 500ns  

  //////////////////////////////////////////////////////////////////
  // start next cycle
  CLK_E_LOW;          
  DELAY_FACTOR_L();  
  xDATA_DIR_IN();

  // Background tasks
  r6530_timer_tick();

  // Wait until LOW time is 500ns
  accurate_delay(NS_TO_TEENSY_CYCLE(500));   // wait 450ns  

#if outputDEBUG
  clock_cycle_count++;
#endif
}

////////////////////////////////////////////////////////////////////
// Serial Event
////////////////////////////////////////////////////////////////////

/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */

inline __attribute__((always_inline))
void serialEvent0() 
{
  if (Serial.available())
  {
    // [R]eset
    //
    if ((Serial.peek() == 'R') || (Serial.peek() == 'r'))
    {
      Serial.read();          // take 'r' out of the queue
      // Reset processor
      uP_assert_reset();
        for (int i=0; i<25; i++) cpu_tick();
        uP_release_reset();
    } 
    else
    // [S]top - NMI
    //
    if ((Serial.peek() == 'S') || (Serial.peek() == 's'))
    {
      Serial.read();          // take 's' out of the queue
      uP_assert_nmi();
        // Cycle uP to see NMI asserted.
        for (int i=0; i<25; i++) cpu_tick();
        uP_release_nmi();
    } 
    else
    if ((reg002_A & 0x80) == 0)         // reg002_A bit 8 is 0; we can push the char.
    {
      char ch = toupper(Serial.read());
      reg002_A = ch | 0x80;
      Serial.write(ch);
    }
  }
}

////////////////////////////////////////////////////////////////////
// Setup
////////////////////////////////////////////////////////////////////
void setup() 
{
  Serial.begin(0);
  while (!Serial);

  Serial.write(27);       // ESC command
  Serial.print("[2J");    // clear screen command
  Serial.write(27);
  Serial.print("[H");
  Serial.println("\n");
  Serial.println("Configuration:");
  Serial.println("==============");
  print_teensy_version();
  Serial.print("Debug:      "); Serial.println(outputDEBUG, HEX);
  Serial.print("SRAM Size:  "); Serial.print(RAM_END - RAM_START + 1, DEC); Serial.println(" Bytes");
  Serial.print("SRAM_START: 0x"); Serial.println(RAM_START, HEX); 
  Serial.print("SRAM_END:   0x"); Serial.println(RAM_END, HEX); 
  Serial.println("");
  Serial.println("=======================================================");
  Serial.println(">    KIM-1");
  Serial.println("> ");
  Serial.println(">    COPYRIGHT");
  Serial.println(">    MOS TECHNOLOGY, INC");
  Serial.println(">    DATE: OCT 18, 1975 REV-D");
  Serial.println("=======================================================");
  Serial.println("Command summary (pdf in doc/ folder):");
  Serial.println(" R             - Reset Board");  
  Serial.println(" S             - Assert NMI to stop program");  
  Serial.println(" [addr] SPACE  - Update address pointer to new address");  
  Serial.println(" [byte] .      - Store byte at current address pointer");  
  Serial.println(" CR            - Skip to next address");  
  Serial.println(" G             - Start executing at address pointer");  
  Serial.println(" Q             - Paper punch from current addr ptr to ($17f7 = END addr)");  
  Serial.println(" L             - Load paper tape.");  
  
   
  // Initialize processor GPIO's
  uP_init();
  r6530_init();
  kim_init();

  // Reset processor for 25 cycles
  uP_assert_reset();
  accurate_delay_init();
  for (int i=0; i<25; i++) cpu_tick();
  uP_release_reset();

}


////////////////////////////////////////////////////////////////////
// Loop()
////////////////////////////////////////////////////////////////////

void loop()
{
  byte i = 0;
  
  // Loop forever
  //  
  while(1)
  {

    cpu_tick();

    // Check serial events but not every cycle.
    // Watch out for clock mismatch (cpu tick vs serialEvent counters are /128)
    i++;
    if (i == 0) serialEvent0();
    if (i == 0) Serial.flush();
  }
}