#ifndef _6530_H
#define _6530_H

////////////////////////////////////////////////////////////////////
// 6530 Peripheral
// emulate just enough so keyboard/display works thru serial port.
////////////////////////////////////////////////////////////////////

// Keyboard/LED(1) or TTY(0).
// set this to 1 to use keyboard/display (NOT SUPPORTED)
#define USE_KEYBOARD_DISPLAY 0

byte reg002_A;
byte reg002_DIRA;
byte reg002_B;
byte reg002_DIRB;
byte reg002_CLKRDI;
byte reg002_CLKRDT;
byte reg002_ENABLE_IRQ;
word reg002_TIMER_TICK;
word reg002_INT_TIMER;

byte reg003_A;
byte reg003_DIRA;
byte reg003_B;
byte reg003_DIRB;
byte reg003_CLKRDI;
byte reg003_CLKRDT;
byte reg003_ENABLE_IRQ;
word reg003_TIMER_TICK;
word reg003_INT_TIMER;

#define CLK1T     1
#define CLK8T     8
#define CLK64T    64
#define CLK1024T  1024

inline __attribute__((always_inline))
void r6530_init()
{
  reg002_A      = 0x00 | USE_KEYBOARD_DISPLAY;
  reg002_DIRA   = 0x00;
  reg002_B      = 0x00;
  reg002_DIRB   = 0x00;
  reg002_CLKRDI = 0x00;
  reg002_CLKRDT = 0x00;
  
  reg002_ENABLE_IRQ = 0x00;
  reg002_TIMER_TICK = CLK1T;
  reg002_INT_TIMER  = CLK1T;

  reg003_A      = 0x00;
  reg003_DIRA   = 0x00;
  reg003_B      = 0x00;
  reg003_DIRB   = 0x00;
  reg003_CLKRDI = 0x00;
  reg003_CLKRDT = 0x00;

  reg003_ENABLE_IRQ = 0x00;
  reg003_TIMER_TICK = CLK1T;
  reg003_INT_TIMER  = CLK1T;
}

inline __attribute__((always_inline))
byte r6530_read(word addr)
{
  byte dout = 0;

  byte A3     = addr & 0b1000;
  byte A2A1A0 = addr & 0b0111;

  if (addr >= 0x1740)
  {
    // I/O Timer 002
    if  (A2A1A0 == 0)    dout = reg002_A;
    if  (A2A1A0 == 1)    dout = reg002_DIRA;
    if  (A2A1A0 == 2)    dout = reg002_B;
    if  (A2A1A0 == 3)    dout = reg002_DIRB;
    if ((A2A1A0 == 4) && (A2A1A0 == 6))   { dout = reg002_CLKRDT; reg002_ENABLE_IRQ = A3; }
    if ((A2A1A0 == 5) && (A2A1A0 == 7))     dout = reg002_CLKRDI;

  } else {
    // I/O Timer 003
    if  (A2A1A0 == 0)    dout = reg003_A;
    if  (A2A1A0 == 1)    dout = reg003_DIRA;
    if  (A2A1A0 == 2)    dout = reg003_B;
    if  (A2A1A0 == 3)    dout = reg003_DIRB;
    if ((A2A1A0 == 4) && (A2A1A0 == 6))   { dout = reg003_CLKRDT; reg003_ENABLE_IRQ = A3; }
    if ((A2A1A0 == 5) && (A2A1A0 == 7))     dout = reg003_CLKRDI;
  }

  if (outputDEBUG)
  {
    char tmp[100];
    sprintf(tmp, "      r6530_read(%04X) = %02X\n", addr, dout);
    Serial.write(tmp);
  }

  return dout;  
}

inline __attribute__((always_inline))
void r6530_write(word addr, byte din)
{
  // byte A0     = addr & 0b0001;
  // byte A2     = addr & 0b0100;
  byte A3     = addr & 0b1000;
  byte A2A1A0 = addr & 0b0111;

  if (outputDEBUG)
  {
    char tmp[100];
    sprintf(tmp, "      r6530_write(%04X, %02X)\n", addr, din);
    Serial.write(tmp);
  }

  if (addr >= 0x1740)
  {
    // I/O Timer 002
    if (A2A1A0 == 0)    reg002_A    = din;     // bit 7 used for serial port
    if (A2A1A0 == 1)    reg002_DIRA = din;
    if (A2A1A0 == 2)    { reg002_B    = din; Serial.write(din); }
    if (A2A1A0 == 3)    reg002_DIRB = din;
    // Following will start timer.  save count.  also initialize internal counters.
    if (A2A1A0 == 4)    { reg002_CLKRDT = din; reg002_INT_TIMER=reg002_TIMER_TICK=CLK1T;    reg002_ENABLE_IRQ = A3; }
    if (A2A1A0 == 5)    { reg002_CLKRDT = din; reg002_INT_TIMER=reg002_TIMER_TICK=CLK8T;    reg002_ENABLE_IRQ = A3; }
    if (A2A1A0 == 6)    { reg002_CLKRDT = din; reg002_INT_TIMER=reg002_TIMER_TICK=CLK64T;   reg002_ENABLE_IRQ = A3; }
    if (A2A1A0 == 7)    { reg002_CLKRDT = din; reg002_INT_TIMER=reg002_TIMER_TICK=CLK1024T; reg002_ENABLE_IRQ = A3; }

  } else {
    // I/O Timer 003
    if (A2A1A0 == 0)    reg003_A    = din;
    if (A2A1A0 == 1)    reg003_DIRA = din;
    if (A2A1A0 == 2)    reg003_B    = din;
    if (A2A1A0 == 3)    reg003_DIRB = din;
    // Following will start timer.  save count.  also initialize internal counters.
    if (A2A1A0 == 4)    { reg003_CLKRDT = din; reg003_INT_TIMER=reg003_TIMER_TICK=CLK1T;    reg003_ENABLE_IRQ = A3; }
    if (A2A1A0 == 5)    { reg003_CLKRDT = din; reg003_INT_TIMER=reg003_TIMER_TICK=CLK8T;    reg003_ENABLE_IRQ = A3; }
    if (A2A1A0 == 6)    { reg003_CLKRDT = din; reg003_INT_TIMER=reg003_TIMER_TICK=CLK64T;   reg003_ENABLE_IRQ = A3; }
    if (A2A1A0 == 7)    { reg003_CLKRDT = din; reg003_INT_TIMER=reg003_TIMER_TICK=CLK1024T; reg003_ENABLE_IRQ = A3; }

  }  
}

inline __attribute__((always_inline))
void r6530_timer_tick()
{
  // I/O TIMER 002
  //
  if (reg002_INT_TIMER == 0)
  {
    // This shouldnt happen, so just recover.
    // reload INT_TIMER
    reg002_INT_TIMER = reg002_TIMER_TICK;
    
  } else {
    reg002_INT_TIMER--;

    if (reg002_INT_TIMER == 0)
    {
      reg002_CLKRDT--;

      if (reg002_CLKRDT == 0) {
        // Main timer timed-out:
        reg002_TIMER_TICK = CLK1T;    // reset tick to 1T
        reg002_CLKRDI = 0xFF;         // Set Interrupt Flag
        if (reg002_ENABLE_IRQ != 0)   // Set PB7 if interrupt-enabled
          reg002_B |= 0x80;
      }
    }
  }

  // I/O TIMER 003
  //
  if (reg003_INT_TIMER == 0)
  {
    // This shouldnt happen, so just recover.
    // reload INT_TIMER
    reg003_INT_TIMER = reg003_TIMER_TICK;
    
  } else {
    reg003_INT_TIMER--;

    if (reg003_INT_TIMER == 0)
    {
      reg003_CLKRDT--;

      if (reg003_CLKRDT == 0) {
        // Main timer timed-out:
        reg003_TIMER_TICK = CLK1T;    // reset tick to 1T
        reg003_CLKRDI = 0xFF;         // Set Interrupt Flag
        if (reg003_ENABLE_IRQ != 0)   // Set PB7 if interrupt-enabled
          reg003_B |= 0x80;
      }
    }
  }
}

#endif // _6530_H