////////////////////////////////////////////////////////////////////
// RetroShield 6502 for Teensy 3.5
// Fig Forth
//
// 2024/04/11
// Version 0.1

// The MIT License (MIT)

// Copyright (c) 2019 Erturk Kocalar, 8Bitforce.com

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
// Date         Comments                                            Author
// -----------------------------------------------------------------------------
// 04/10/2024   Fig Forth v1.1 based.                               Erturk
// 08/29/2024   Update setup/hold times.                            Erturk

////////////////////////////////////////////////////////////////////
// Options
//   outputDEBUG: Print memory access debugging messages.
////////////////////////////////////////////////////////////////////
#define outputDEBUG     0
#define DEBUG_SPEW_DELAY  1000

////////////////////////////////////////////////////////////////////
// BOARD DEFINITIONS
////////////////////////////////////////////////////////////////////

#include "memorymap.h"      // Memory Map (ROM, RAM, PERIPHERALS)
#include "portmap.h"        // Pin mapping to cpu
#include "setuphold.h"      // Delays required to meet setup/hold
#include "terminal.h"       // Taliforth2 emulation I/O emulation
#include "buttons.h"        // Buttons P & C

unsigned long clock_cycle_count;
unsigned long clock_cycle_last;

word          uP_ADDR;
byte          uP_DATA;

void uP_assert_reset()
{
  // Drive RESET conditions
  digitalWriteFast(uP_RESET_N,  LOW);
  digitalWriteFast(uP_IRQ_N,    HIGH);
  digitalWriteFast(uP_NMI_N,    HIGH);
  digitalWriteFast(uP_RDY,      HIGH);
  digitalWriteFast(uP_SO_N,     HIGH);
}

void uP_release_reset()
{
  // Drive RESET conditions
  digitalWriteFast(uP_RESET_N,  HIGH);
}

void uP_init()
{
  // Set directions for ADDR & DATA Bus.
 
  configure_PINMODE_ADDR();
  configure_PINMODE_DATA();

  pinMode(uP_RESET_N, OUTPUT);
  pinMode(uP_RW_N,    INPUT_PULLUP);
  pinMode(uP_RDY,     OUTPUT);
  pinMode(uP_SO_N,    OUTPUT);
  pinMode(uP_IRQ_N,   OUTPUT);
  pinMode(uP_NMI_N,   OUTPUT);
  pinMode(uP_CLK_E,   OUTPUT);
  pinMode(uP_GPIO,    INPUT_PULLUP);
    
  digitalWriteFast(uP_CLK_E, LOW); 
  uP_assert_reset();

  clock_cycle_count = 0;
}


////////////////////////////////////////////////////////////////////
// Processor Control Loop
////////////////////////////////////////////////////////////////////
// This is where the action is.
// it reads processor control signals and acts accordingly.
//
byte DATA_OUT;
byte DATA_IN;

inline __attribute__((always_inline))
void cpu_tick()
{ 
  
  CLK_HIGH;    // E goes high

  DELAY_FACTOR_H();
  
  uP_ADDR = ADDR();


  if (STATE_RW_N)	  
  //////////////////////////////////////////////////////////////////
  // HIGH = READ
  {
    // change DATA port to output to uP:
    // DATA_DIR_OUT();
    xDATA_DIR_OUT();
        
    // Emulated terminal throu ROM region.
    if ( uP_ADDR == io_getc)
    {
      if (Serial.available())
        DATA_OUT = toupper( Serial.read() ) & 0x7F;
      else
        DATA_OUT = 0x00;
    }
    else    
    // ROM?
    if ( ROM_ENABLED && (ROM_START <= uP_ADDR) && (uP_ADDR <= ROM_END) )
      DATA_OUT = forth_rom_bin[ (uP_ADDR - ROM_START) ];
    else
    // RAM?
    if ( (RAM_START <= uP_ADDR) && (uP_ADDR <= RAM_END) )
      // Use Arduino RAM for stack/important stuff
      DATA_OUT = ( RAM[uP_ADDR - RAM_START] );
    else
    // Unknown???
    {
      DATA_OUT = 0x00;
    }

    // Start driving the databus out
    SET_DATA_OUT( DATA_OUT );
    DELAY_FOR_BUFFER();

    if (outputDEBUG)
    // if (clock_cycle_count > 0x3065A)
    {
      char tmp[150];
      sprintf(tmp, "%04lu: -- A=%04X D=%02X\n\r", clock_cycle_count, uP_ADDR, DATA_OUT);
      Serial.write(tmp);
    }

  } 
  else 
  //////////////////////////////////////////////////////////////////
  // R/W = LOW = WRITE
  {
    DATA_IN = xDATA_IN();
    
    // Emulated terminal throu ROM region.
    // #define io_cls	io_area + 0	// ; clear terminal window
    // #define io_putc	io_area + 1	// ; put char
    // #define io_putr	io_area + 2	// ; put raw char (doesn't interpret CR/LF)
    // #define io_puth	io_area + 3	// ; put as hex number

    if ( uP_ADDR == io_cls)   // CLS
    {
      for(int z=0; z<24; z++)
        Serial.println();
    }
    else
    if ( uP_ADDR == io_putc)  // Put char
    {
      Serial.write(DATA_IN);
      if (DATA_IN == '\n') Serial.write('\r');
    }
    else
    if ( uP_ADDR == io_putr)  // Put raw char
    {
      Serial.write(DATA_IN);
    }
    else
    if ( uP_ADDR == io_puth)  // Put hex number
    {
      Serial.print(DATA_IN, HEX);
    }
    else
    // RAM?
    if ( (RAM_START <= uP_ADDR) && (uP_ADDR <= RAM_END) )
      // Use Arduino RAM for stack/important stuff
      RAM[uP_ADDR - RAM_START] = DATA_IN;
    else
    // Unknown ???
    {
      Serial.print("Uknown write: 0x");
      Serial.print(uP_ADDR, HEX);
      Serial.print(" <= 0x");
      Serial.println(DATA_IN, HEX);
    } 
    
#if outputDEBUG
    if (1) // (clock_cycle_count > 0x3065A)
    {
      char tmp[150];
      sprintf(tmp, "%04lu: WR A=%04X D=%02X\n\r", clock_cycle_count, uP_ADDR, DATA_IN);
      Serial.write(tmp);
    }
#endif
  }

  // 6502 can be stopped when CLK=HIGH.
  // Go slow when debugging
#if outputDEBUG
  if (digitalReadFast(uP_RESET_N))
  {
    if (0) { delay(DEBUG_SPEW_DELAY); }    
    if (1) { while(!Serial.available()); Serial.read(); }
  }
#endif

  //////////////////////////////////////////////////////////////////
    
  // start next cycle
  CLK_LOW;    // E goes low
  DELAY_FACTOR_L();  

  xDATA_DIR_IN();
  
#if outputDEBUG
  clock_cycle_count++;
#endif
}



////////////////////////////////////////////////////////////////////
// Setup
////////////////////////////////////////////////////////////////////
void setup() 
{
  Serial.begin(0);
  while (!Serial);

  Serial.write(27);       // ESC command
  Serial.print("[2J");    // clear screen command
  Serial.write(27);
  Serial.print("[H");
  Serial.println("\n");
  Serial.println("Configuration:");
  Serial.println("==============");
  print_teensy_version();
  Serial.print("Debug:      "); Serial.println(outputDEBUG, HEX);
  Serial.print("SRAM Size:  "); Serial.print(RAM_END - RAM_START + 1, DEC); Serial.println(" Bytes");
  Serial.print("SRAM_START: 0x"); Serial.println(RAM_START, HEX); 
  Serial.print("SRAM_END:   0x"); Serial.println(RAM_END, HEX); 
  Serial.println("");
  Serial.println("================================================");
  Serial.println(">            Fig Forth v1.1 for the 6502        ");
  Serial.println(">                                               ");
  Serial.println(">;              Through the courtesy of         ");
  Serial.println(">;                                              ");
  Serial.println(">;               FORTH INTEREST GROUP           ");
  Serial.println(">;                  P.O. BOX  2154              ");
  Serial.println(">;               OAKLAND, CALIFORNIA            ");
  Serial.println(">;                      94621                   ");
  Serial.println(">;                                              ");
  Serial.println(">;                   Release 1.1                ");
  Serial.println(">;              with compiler security          ");
  Serial.println(">;                        and                   ");
  Serial.println(">;              variable length names           ");
  Serial.println("================================================");

   

  // Initialize processor GPIO's
  uP_init();

  // FIG FORTH TRACE OPTIONS
  //
  FIGFORTH_TRACE_DETAIL            = false;
  FIGFORTH_TRACE_NOWAITKEY         = false;
  FIGFORTH_TRACE_HIGHLEVELNESTING  = false;

  copy_rom_to_ram();      // !!!  uses above settings to modify image in RAM.

  // Reset processor for 25 cycles
  uP_assert_reset();
  Serial.println("RESET_N=0");  
  for (int i=0; i<15; i++) 
    cpu_tick();
  uP_release_reset();
  Serial.println("RESET_N=1");
}


////////////////////////////////////////////////////////////////////
// Loop()
////////////////////////////////////////////////////////////////////

void loop()
{
  byte i = 0;
  
  // Loop forever
  //  
  while(1)
  {
    cpu_tick();

    // Check serial events but not every cycle.
    // Watch out for clock mismatch (cpu tick vs serialEvent counters are /128)
    i++;
    // if (i == 0) serialEvent0();
    if (i == 0) Serial.flush();

    // Use Teensy buttons P & C to do something.
    // Warning - these slow down the cpu operating freq.
    // if (0) use_button_P_to_pulse_pin(uP_NMI_N,   LOW, HIGH);
    // if (0) use_button_P_to_pulse_pin(uP_IRQ_N,  LOW, HIGH);
    // if (0) use_button_C_to_pulse_pin(uP_RESET_N, LOW, HIGH);
    // if (0) use_button_P_to_single_step();

  }
}
