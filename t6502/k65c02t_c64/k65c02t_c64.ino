////////////////////////////////////////////////////////////////////
// RetroShield 6502 for Teensy 3.5
// Commodore 64 Basic
//
// 2019/08/27
// Version 0.1

// The MIT License (MIT)

// Copyright (c) 2019 Erturk Kocalar, 8Bitforce.com

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
// Date         Comments                                            Author
// -----------------------------------------------------------------------------
// 8/27/2019    Bring-up on Teensy 3.5.                             Erturk
// 9/13/2019    Teensy2RetroShield Adapter Board.                   Erturk
// 8/29/2024    Add teensy 4.1 support.                             Erturk

////////////////////////////////////////////////////////////////////
// Options
//   outputDEBUG: Print memory access debugging messages.
////////////////////////////////////////////////////////////////////
#define outputDEBUG       0
#define DEBUG_SPEW_DELAY  1000

////////////////////////////////////////////////////////////////////
// BOARD DEFINITIONS
////////////////////////////////////////////////////////////////////

#include "memorymap.h"      // Memory Map (ROM, RAM, PERIPHERALS)
#include "portmap.h"        // Pin mapping to cpu
#include "setuphold.h"      // Delays required to meet setup/hold
#include "terminal.h"       // terminal Emulation
#include "buttons.h"        // Buttons P & C

////////////////////////////////////////////////////////////////////
// 65C02 DEFINITIONS
////////////////////////////////////////////////////////////////////

// 65C02 HW CONSTRAINTS
// 1- RESET_N must be asserted at least 2 clock cycles.
// 2- CLK can not be low more than 5 microseconds.  Can be high indefinitely.
//


unsigned long clock_cycle_count;
unsigned long clock_cycle_last;

word          uP_ADDR;
byte          uP_DATA;

byte          regKBD = 0x00;


void uP_assert_reset()
{
  // Drive RESET conditions
  digitalWrite(uP_RESET_N,  LOW);
  digitalWrite(uP_IRQ_N,    HIGH);
  digitalWrite(uP_NMI_N,    HIGH);
  digitalWrite(uP_RDY,      HIGH);
  digitalWrite(uP_SO_N,     HIGH);
}

void uP_release_reset()
{
  // Drive RESET conditions
  digitalWrite(uP_RESET_N,  HIGH);
}

void uP_init()
{
  // Set directions
  // DATA_DIR_IN;
  // ADDR_H_DIR_IN;
  // ADDR_L_DIR_IN;
  
  configure_PINMODE_DATA();
  configure_PINMODE_ADDR();

  pinMode(uP_RESET_N, OUTPUT);
  pinMode(uP_RW_N,    INPUT_PULLUP);
  pinMode(uP_RDY,     OUTPUT);
  pinMode(uP_SO_N,    OUTPUT);
  pinMode(uP_IRQ_N,   OUTPUT);
  pinMode(uP_NMI_N,   OUTPUT);
  pinMode(uP_CLK_E,   OUTPUT);
  pinMode(uP_GPIO,    INPUT_PULLUP);  

  uP_assert_reset();
  digitalWrite(uP_CLK_E,  LOW); 
}


////////////////////////////////////////////////////////////////////
// Processor Control Loop
////////////////////////////////////////////////////////////////////
// This is where the action is.
// it reads processor control signals and acts accordingly.
//
volatile byte DATA_OUT;
volatile byte DATA_IN;


inline __attribute__((always_inline))
void cpu_tick()
{ 
  
  CLK_HIGH;    // E goes high
  DELAY_FACTOR_H();

  uP_ADDR = ADDR();
    
  if (STATE_RW_N)	  
  //////////////////////////////////////////////////////////////////
  // HIGH = READ
  {
    // change DATA port to output to uP:
    xDATA_DIR_OUT();
        
    // ROM?
    if ( (KERNAL_START <= uP_ADDR) && (uP_ADDR <= KERNAL_END) )
      DATA_OUT = ( kernal_bin[ (uP_ADDR - KERNAL_START) ] );
    else
    // Our Hack ROM
    if ( (CUSTOM_START <= uP_ADDR) && (uP_ADDR <= CUSTOM_END) )
    {
      // Serial.print("CUSTOM_ROM "); Serial.println(uP_ADDR, HEX);
      DATA_OUT = ( custom_bin[ (uP_ADDR - CUSTOM_START) ] );
    }
    else
    // check if reading from BASIC
    if ( (BASIC_START <= uP_ADDR) && (uP_ADDR <= BASIC_END) )
      DATA_OUT = ( basic_bin[ (uP_ADDR - BASIC_START) ] );
    else
    // RAM?
    if ( (uP_ADDR <= RAM_END) && (RAM_START <= uP_ADDR) )
      // Use Arduino RAM for stack/important stuff
      DATA_OUT = ( RAM[uP_ADDR - RAM_START] );
    else
    // Raster Line
    if ( uP_ADDR == 0xD012 )
      DATA_OUT = ( 0x00 );
    else
    if ( uP_ADDR == 0xDC0D )
      // Clear IRQ if cpu accesses DC0D
      digitalWrite(uP_IRQ_N, HIGH);
    else
    if ( uP_ADDR == 0xCFFF )
    {
      DATA_OUT = ( regKBD );
      if (regKBD != 0x00) 
      {
        char tmp[40];
        sprintf(tmp, "-- A=%04X D=%02X\n", uP_ADDR, regKBD);
        // Serial.write(tmp);

        regKBD = 0;
        // Serial.println("==> CONSUMED ");
      }      
    }    
    else
      DATA_OUT = ( 0xFF );

    // Start driving the databus out
    SET_DATA_OUT( DATA_OUT );
    DELAY_FOR_BUFFER();
      
#if outputDEBUG
    if (1)
    {
      char tmp[50];
      sprintf(tmp, "-- A=%04X D=%02X\n\r", uP_ADDR, DATA_OUT);
      Serial.write(tmp);
    }
#endif

  } 
  else 
  //////////////////////////////////////////////////////////////////
  // R/W = LOW = WRITE
  {
    DATA_IN = xDATA_IN();
    
    // RAM?
    if ( (RAM_START <= uP_ADDR) && (uP_ADDR <= RAM_END) )
      // Use Arduino RAM for stack/important stuff
      RAM[uP_ADDR - RAM_START] = DATA_IN;
    
    // Map C64_VRAM to VT100
    if ((0x0400 <= uP_ADDR) && (uP_ADDR < 0x0800))
    {
      byte c = DATA_IN;
      if (c < 0x20) {
        c += 0x40;
      }
      char y = 1+ (uP_ADDR - 0x400) / 40;
      char x = 1+ (uP_ADDR - 0x400) % 40;

      move_cursor(x, y);
      if (c >= 0x80)
      {
        // Serial.write(c & 0x7F);
        // left_cursor();
        c = c & 0x7F;
        if (c < 0x20) {
          c += 0x40;
        }
        Serial.write("\e[7m");
        Serial.write(c);
        Serial.write("\e[0m");
        Serial.write("\e[44m");     // Blue Background
        Serial.write("\e[?25l");    // Disable VT100 cursor, so we get C64 cursor.
      }
      else
        Serial.write(c);
    }

    // This will fee UART direct to USB without VT100 codes.
//    if ( uP_ADDR == 0xCFFF )
//    {
//      // Serial.write(DATA_IN);
//      screen_bsout(DATA_IN);
//      if (DATA_IN == 0x0D)
//      {
//        // Not needed for VT100
//        // Serial.write("\n");
//        // ngt.print("\n");
//      }      
//    }


#if outputDEBUG
    // if ((0x400 <= uP_ADDR) && (uP_ADDR < 0x800))
    {
      char tmp[50];
      sprintf(tmp, "WR A=%04X D=%02X\n\r", uP_ADDR, DATA_IN);
      Serial.write(tmp);
    }
#endif
  }

  // 6502 can be stopped when CLK=HIGH.
  // Go slow when debugging
#if outputDEBUG
  if (digitalReadFast(uP_RESET_N))
  {
    if (0) { delay(DEBUG_SPEW_DELAY); }    
    if (1) { while(!Serial.available()); Serial.read(); }
  }
#endif

  //////////////////////////////////////////////////////////////////

  // start next cycle
  CLK_LOW;    // E goes low
  DELAY_FACTOR_L();  

  xDATA_DIR_IN();

#if outputDEBUG
  clock_cycle_count++;
#endif
}

////////////////////////////////////////////////////////////////////
// Serial Event
////////////////////////////////////////////////////////////////////

/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */

bool char_waiting = false;

inline __attribute__((always_inline))
void serialEventC64() 
{
  if (regKBD !=0)
    return;
  else
  if (Serial.available())
  {
    regKBD = toupper(Serial.read());
    if (regKBD == '\e')
    {
      // Escape key received for up/down/right/left cursor
      // convert to PETSCII
      while (!Serial.available());
      regKBD = Serial.read();
      if (regKBD == '[')
      {
        // correct sequence
        while (!Serial.available());
        regKBD = Serial.read();
        if (regKBD == 'A')
          regKBD = 145;
        else
        if (regKBD == 'B')
          regKBD = 17;
        else
        if (regKBD == 'C')
          regKBD = 29;
        else
        if (regKBD == 'D')
          regKBD = 157;
      }
      else
      {
        Serial.print("ERR: \\e -> "); Serial.println(regKBD, HEX);
      }
    }
    else
    {
      // Serial.print("==> INCHAR "); Serial.println(regKBD, HEX);      
    }
  }    
  return;
}

////////////////////////////////////////////////////////////////////
// Setup
////////////////////////////////////////////////////////////////////

void setup() 
{
  Serial.begin(0);
  while (!Serial);

  Serial.write(27);       // ESC command
  Serial.print("[2J");    // clear screen command
  Serial.write(27);
  Serial.print("[H");
  Serial.println("\n");
  Serial.println("Configuration:");
  Serial.println("==============");
  print_teensy_version();
  Serial.print("Debug:      "); Serial.println(outputDEBUG, HEX);
  Serial.print("SRAM Size:  "); Serial.print(RAM_END - RAM_START + 1, DEC); Serial.println(" Bytes");
  Serial.print("SRAM_START: 0x"); Serial.println(RAM_START, HEX); 
  Serial.print("SRAM_END:   0x"); Serial.println(RAM_END, HEX); 
  Serial.println("");
  Serial.println("                                        |");
  Serial.println("                                        |");
  Serial.println("                                        |");
  Serial.println("                                        |");
  Serial.println("                                        |");
  Serial.println("                                        |");
  Serial.println("                                        |");
  Serial.println("                                        |");
  Serial.println("                                        |");
  Serial.println("                                        |");
  Serial.println("                                        |");
  Serial.println("                                        |");
  Serial.println("                                        |");
  Serial.println("                                        |");
  Serial.println("                                        |");
  Serial.println("                                        |");
  Serial.println("                                        |===============");
  Serial.println("                                        |Commodore 64");
  Serial.println("                                        |BASIC V2");
  Serial.println("                                        |");
  Serial.println("                                        |Credits:");
  Serial.println("                                        |Michael Steil for porting");
  Serial.println("========================================|===============");
  Serial.println("");
  Serial.println("");  
  Serial.println("");  
  Serial.println(""); 
  Serial.println(""); 
   
  // Initialize processor GPIO's
  uP_init();
  init_C64();
  
  // Reset processor for 25 cycles
  uP_assert_reset();
  for(int i=0; i<25; i++) {
    cpu_tick();
    // delay(100);
  }
  uP_release_reset();

  Serial.println("\n");
  Serial.write("\e[44m");     // Blue Background
  Serial.write("\e[?25l");    // Disable VT100 cursor, so we get C64 cursor.
}


////////////////////////////////////////////////////////////////////
// Loop()
////////////////////////////////////////////////////////////////////

void loop()
{
  byte i=0;
  word j=0;

  while(1)
  {
    
    cpu_tick();

    // Check serial events but not every cycle.
    // Watch out for clock mismatch (cpu tick vs serialEvent counters are /256)
    i++;
    if (i == 0) { 
      serialEventC64(); 
      Serial.flush(); 

    // interrupt for C64 cursor
    j++;
      if (j == 60)
      {
        digitalWrite(uP_IRQ_N, LOW);
        j = 0;
      }
    }
    

    // if (j++ > (900000/60))
    // {
    //   digitalWrite(uP_IRQ_N, LOW);
    //   // Serial.print(".");
    //   j = 0;
    // }
    
  }
}
