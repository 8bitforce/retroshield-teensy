#ifndef _BUTTONS_H
#define _BUTTONS_H

////////////////////////////////////////////////////////////
// These functions return real-time state (no debouncing)
////////////////////////////////////////////////////////////

#if (ARDUINO_TEENSY35 || ARDUINO_TEENSY36)

#define btn_P_state() (analogRead(A21) < 10 ? true : false)
#define btn_C_state() (analogRead(A22) < 10 ? true : false)

#elif (ARDUINO_TEENSY41)

#define btn_P_state() (analogRead(A16) < 10 ? true : false)
#define btn_C_state() (analogRead(A17) < 10 ? true : false)

#endif

////////////////////////////////////////////////////////////
// These functions are debounced.
////////////////////////////////////////////////////////////
unsigned long debounceDelay = 16;

bool btn_P_debounced() 
{
  static bool btn_P_prev_state = false;
  static unsigned long w = 0; 
  bool        btn_P = btn_P_state();

  if ( btn_P == btn_P_prev_state )
  {
    w = millis();
  }
  else
  if ((millis() - w) > debounceDelay)
  {
    return (btn_P_prev_state = btn_P_state());
  }
  return btn_P;
}

bool btn_C_debounced() 
{
  static bool btn_C_prev_state = false;
  static unsigned long w = 0; 
  bool        btn_C = btn_C_state();

  if ( btn_C == btn_C_prev_state )
  {
    w = millis();
  }
  else
  if ((millis() - w) > debounceDelay)
  {
    return (btn_C_prev_state = btn_C_state());
  }
  return btn_C;
}

#endif