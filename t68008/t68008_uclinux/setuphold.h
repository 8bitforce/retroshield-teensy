#ifndef _SETUPHOLD_H
#define _SETUPHOLD_H

// ##################################################
// Adjust setup/hold times based on Teensy
// Teensy 3.5 = 120Mhz  (1x)
// Teensy 3.6 = 180Mhz  (1.5x)
// Teensy 4.1 = 600Mhz  (5x)
// ##################################################
// ##################################################
#if (ARDUINO_AVR_MEGA2560)
// ##################################################
#error "Not implemented yet"

// ##################################################
#elif (ARDUINO_TEENSY35)
// ##################################################

  #define DELAY_UNIT_25NS()    asm volatile("nop\nnop\nnop\nnop\n")

  #define DELAY_UNIT_50NS()   {DELAY_UNIT_25NS(); DELAY_UNIT_25NS();}
  #define DELAY_UNIT_100NS()  {DELAY_UNIT_50NS(); DELAY_UNIT_50NS();}
  #define DELAY_UNIT_150NS()  {DELAY_UNIT_100NS(); DELAY_UNIT_50NS();}
  #define DELAY_UNIT_200NS()  {DELAY_UNIT_100NS(); DELAY_UNIT_100NS();}
  #define DELAY_UNIT_250NS()  {DELAY_UNIT_100NS(); DELAY_UNIT_100NS(); DELAY_UNIT_50NS();}

  #define BUFFER_DELAY()      {DELAY_UNIT_100NS();DELAY_UNIT_100NS();}

// ##################################################
#elif (ARDUINO_TEENSY36)
// ##################################################

  #define DELAY_UNIT_25NS()    asm volatile("nop\nnop\nnop\nnop\nnop\nnop\nnop\n")

  #define DELAY_UNIT_50NS()   {DELAY_UNIT_25NS(); DELAY_UNIT_25NS();}
  #define DELAY_UNIT_100NS()  {DELAY_UNIT_50NS(); DELAY_UNIT_50NS();}
  #define DELAY_UNIT_150NS()  {DELAY_UNIT_100NS(); DELAY_UNIT_50NS();}
  #define DELAY_UNIT_200NS()  {DELAY_UNIT_100NS(); DELAY_UNIT_100NS();}
  #define DELAY_UNIT_250NS()  {DELAY_UNIT_100NS(); DELAY_UNIT_100NS(); DELAY_UNIT_50NS();}

  #define BUFFER_DELAY()      {DELAY_UNIT_100NS();DELAY_UNIT_100NS();}

// ##################################################
#elif (ARDUINO_TEENSY41)
// ##################################################

  #define DELAY_UNIT_25NS() asm volatile("nop\nnop\nnop\nnop\nnop\nnop\n" \
                                         "nop\nnop\nnop\nnop\nnop\nnop\n" \
                                         "nop\nnop\nnop\nnop\nnop\nnop\n" \
                                         "nop\nnop\nnop\n" \
                                         "nop\nnop\nnop\nnop\n" \
                                         "nop\nnop\nnop\nnop\nnop\nnop\n")

  #define DELAY_UNIT_50NS()   {DELAY_UNIT_25NS(); DELAY_UNIT_25NS();}
  #define DELAY_UNIT_100NS()  {DELAY_UNIT_50NS(); DELAY_UNIT_50NS();}
  #define DELAY_UNIT_150NS()  {DELAY_UNIT_100NS(); DELAY_UNIT_50NS();}
  #define DELAY_UNIT_200NS()  {DELAY_UNIT_100NS(); DELAY_UNIT_100NS();}
  #define DELAY_UNIT_250NS()  {DELAY_UNIT_100NS(); DELAY_UNIT_100NS(); DELAY_UNIT_50NS();}

  #define BUFFER_DELAY()      {DELAY_UNIT_100NS();DELAY_UNIT_100NS();}
#endif

#endif  // _SETUPHOLD_H