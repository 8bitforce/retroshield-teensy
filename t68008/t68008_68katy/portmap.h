#ifndef _PORTMAP_H
#define _PORTMAP_H

// This is a complicated header file.
// it defines port macros based on Arduino or Teeny and which
// version of teensy. Fastest port access are done using 
// direct access to ports.


// ##################################################
#if (ARDUINO_AVR_MEGA2560)
// ##################################################
// DIO2 library required on MEGA for fast GPIO access.
#include <avr/pgmspace.h>
#include "pins2_arduino.h"
#include <DIO2.h>

// DIO2 library uses ..2 instead of ..Fast
#define digitalWriteFast(PORT,DATA)  digitalWrite2(PORT,DATA)
#define digitalReadFast(PORT)        digitalRead2(PORT)
#endif

// ##################################################
#if (ARDUINO_AVR_MEGA2560)
// ##################################################

#define MEGA_PD7  38
#define MEGA_PG0  41
#define MEGA_PG1  40
#define MEGA_PG2  39
#define MEGA_PB0  53
#define MEGA_PB1  52
#define MEGA_PB2  51
#define MEGA_PB3  50

#define MEGA_PC7  30
#define MEGA_PC6  31
#define MEGA_PC5  32
#define MEGA_PC4  33
#define MEGA_PC3  34
#define MEGA_PC2  35
#define MEGA_PC1  36
#define MEGA_PC0  37

#define MEGA_PL7  42
#define MEGA_PL6  43
#define MEGA_PL5  44
#define MEGA_PL4  45
#define MEGA_PL3  46
#define MEGA_PL2  47
#define MEGA_PL1  48
#define MEGA_PL0  49

#define MEGA_PA7  29
#define MEGA_PA6  28
#define MEGA_PA5  27
#define MEGA_PA4  26
#define MEGA_PA3  25
#define MEGA_PA2  24
#define MEGA_PA1  23
#define MEGA_PA0  22

// ##################################################
#elif (ARDUINO_TEENSY35 || ARDUINO_TEENSY36 || ARDUINO_TEENSY41)
// ##################################################

#define MEGA_PD7  24
#define MEGA_PG0  13
#define MEGA_PG1  16
#define MEGA_PG2  17
#define MEGA_PB0  28
#define MEGA_PB1  39
#define MEGA_PB2  29
#define MEGA_PB3  30

#define MEGA_PC7  27
#define MEGA_PC6  26
#define MEGA_PC5  4
#define MEGA_PC4  3
#define MEGA_PC3  38
#define MEGA_PC2  37
#define MEGA_PC1  36
#define MEGA_PC0  35

#define MEGA_PL7  5
#define MEGA_PL6  21
#define MEGA_PL5  20
#define MEGA_PL4  6
#define MEGA_PL3  8
#define MEGA_PL2  7
#define MEGA_PL1  14
#define MEGA_PL0  2

#define MEGA_PA7  12
#define MEGA_PA6  11
#define MEGA_PA5  25
#define MEGA_PA4  10
#define MEGA_PA3  9
#define MEGA_PA2  23
#define MEGA_PA1  22
#define MEGA_PA0  15

#endif

////////////////////////////////////////////////////////////////////
// 68008 Processor Control
////////////////////////////////////////////////////////////////////

#define uP_MEGA_D0        MEGA_PA0
#define uP_MEGA_D1        MEGA_PA1
#define uP_MEGA_D2        MEGA_PA2
#define uP_MEGA_D3        MEGA_PA3
#define uP_MEGA_D4        MEGA_PA4
#define uP_MEGA_D5        MEGA_PA5
#define uP_MEGA_D6        MEGA_PA6
#define uP_MEGA_D7        MEGA_PA7

#define uP_MEGA_CLK       MEGA_PL3
#define uP_RESET_N        MEGA_PC2
#define uP_HALT_N         MEGA_PC1
#define uP_IPL2_0_N       MEGA_PC5
#define uP_IPL1_N         MEGA_PC4
#define uP_BERR_N         MEGA_PC3
#define uP_DBUS_EN        MEGA_PD7
#define uP_MEGA_AS_INV_N  MEGA_PG1
#define uP_MEGA_RW_INV    MEGA_PC0
#define uP_MEGA_DS_INV    MEGA_PG2
#define uP_PULSE_DTACK    MEGA_PG0

#define uP_MEGA_A7        MEGA_PL7
#define uP_MEGA_A6        MEGA_PL6
#define uP_MEGA_A5        MEGA_PL5
#define uP_MEGA_A4        MEGA_PL4
#define uP_MEGA_A3        MEGA_PB3        // <<== A3 in another port due to CLK
#define uP_MEGA_A2        MEGA_PL2
#define uP_MEGA_A1        MEGA_PL1
#define uP_MEGA_A0        MEGA_PL0
#define uP_PEEK_ADDR_L    MEGA_PB2
#define uP_PEEK_ADDR_M    MEGA_PB1
#define uP_PEEK_ADDR_H    MEGA_PB0

#define LED_HDD          MEGA_PC7

////////////////////////////////////////////////////////////////////
// MACROS
////////////////////////////////////////////////////////////////////

#define LED_ACT1_ON()       digitalWriteFast(LED_ACT1, LOW)
#define LED_ACT1_OFF()      digitalWriteFast(LED_ACT1, HIGH)
#define LED_ACT1_TOGGLE()   digitalWriteFast(LED_ACT1, !digitalReadFast(LED_ACT1))

byte ADDRpinTable[] = {
  // 8-bit address port, used to mux in A19..A0
  // A0..A7
  uP_MEGA_A0,uP_MEGA_A1,uP_MEGA_A2,uP_MEGA_A3,uP_MEGA_A4,uP_MEGA_A5,uP_MEGA_A6,uP_MEGA_A7
};

byte DATApinTable[] = {
  // D0..D7
  uP_MEGA_D0,uP_MEGA_D1,uP_MEGA_D2,uP_MEGA_D3,uP_MEGA_D4,uP_MEGA_D5,uP_MEGA_D6,uP_MEGA_D7
};

void configure_PINMODE_ADDR()
{
  for (unsigned int i=0; i<sizeof(ADDRpinTable); i++)
  {
    pinMode(ADDRpinTable[i],INPUT);
  } 
}

void configure_PINMODE_DATA()
{
  for (unsigned int i=0; i<sizeof(DATApinTable); i++)
  {
    pinMode(DATApinTable[i],INPUT);
  } 
}

// ##################################################
#if (ARDUINO_AVR_MEGA2560)
// ##################################################

#define MEGA_DIR_IN  0x00
#define MEGA_DIR_OUT 0xFF

// Directions
#define MEGA_DATA_DIR        DDRA

// Data in/out
#define MEGA_ADDR_A3         ((PINB) & 0b00001000)
#define MEGA_ADDR_A7A0       ((PINL) & 0b11110111)
#define MEGA_DATA_IN         ((byte) PINA)
#define MEGA_DATA_OUT        (PORTA)

// read bits raw
#define xDATA_DIR_IN()    (MEGA_DATA_DIR = MEGA_DIR_IN)
#define xDATA_DIR_OUT()   (MEGA_DATA_DIR = MEGA_DIR_OUT)
#define SET_DATA_OUT(D)   (MEGA_DATA_OUT = (byte) D)
#define xDATA_IN()        ((byte) MEGA_DATA_IN)

// build ADDR
#define ADDR8()            ((byte) (MEGA_ADDR_A7A0 | MEGA_ADDR_A3))


// ##################################################
#elif ((ARDUINO_TEENSY35 || ARDUINO_TEENSY36) || (ARDUINO_TEENSY41))
// ##################################################
// Fast pinMode for Teensy.
#define xSET_PIN_INPUT(PIN)  (CORE_PIN##PIN##_DDRREG  = CORE_PIN##PIN##_DDRREG  & (~CORE_PIN##PIN##_BITMASK))
#define xSET_PIN_OUTPUT(PIN) (CORE_PIN##PIN##_DDRREG  = CORE_PIN##PIN##_DDRREG  |  (CORE_PIN##PIN##_BITMASK))

inline __attribute__((always_inline))
void xDATA_DIR_IN()
{
#if ((ARDUINO_TEENSY35 || ARDUINO_TEENSY36))  
  for (int i=7; i>=0; i--)
  {
    pinMode(DATApinTable[i],INPUT);
  } 
#elif (ARDUINO_TEENSY41)

  // xDATA_IN
  // uP_MEGA_D0,uP_MEGA_D1,uP_MEGA_D2,uP_MEGA_D3,uP_MEGA_D4,uP_MEGA_D5,uP_MEGA_D6,uP_MEGA_D7

  xSET_PIN_INPUT(15);
  xSET_PIN_INPUT(22);
  xSET_PIN_INPUT(23);
  xSET_PIN_INPUT(9);
  xSET_PIN_INPUT(10);
  xSET_PIN_INPUT(25);
  xSET_PIN_INPUT(11);
  xSET_PIN_INPUT(12);

#endif
}


inline __attribute__((always_inline))
void xDATA_DIR_OUT()
{
#if ((ARDUINO_TEENSY35 || ARDUINO_TEENSY36))    
  for (int i=7; i>=0; i--)
  {
    pinMode(DATApinTable[i],OUTPUT);
  } 
#elif (ARDUINO_TEENSY41)
  // xDATA_IN
  // uP_MEGA_D0,uP_MEGA_D1,uP_MEGA_D2,uP_MEGA_D3,uP_MEGA_D4,uP_MEGA_D5,uP_MEGA_D6,uP_MEGA_D7

  xSET_PIN_OUTPUT(15);
  xSET_PIN_OUTPUT(22);
  xSET_PIN_OUTPUT(23);
  xSET_PIN_OUTPUT(9);
  xSET_PIN_OUTPUT(10);
  xSET_PIN_OUTPUT(25);
  xSET_PIN_OUTPUT(11);
  xSET_PIN_OUTPUT(12);
#endif
}

inline __attribute__((always_inline))
void SET_DATA_OUT(byte b)
{
  digitalWriteFast(uP_MEGA_D0, (b & 1));
  b = b >> 1;
  digitalWriteFast(uP_MEGA_D1, (b & 1));
  b = b >> 1;
  digitalWriteFast(uP_MEGA_D2, (b & 1));
  b = b >> 1;
  digitalWriteFast(uP_MEGA_D3, (b & 1));
  b = b >> 1;
  digitalWriteFast(uP_MEGA_D4, (b & 1));
  b = b >> 1;
  digitalWriteFast(uP_MEGA_D5, (b & 1));
  b = b >> 1;
  digitalWriteFast(uP_MEGA_D6, (b & 1));
  b = b >> 1;
  digitalWriteFast(uP_MEGA_D7, (b & 1));
}

inline __attribute__((always_inline))
byte xDATA_IN()
{
  byte b = 0;

  b = b | digitalReadFast(uP_MEGA_D7);
  b = b << 1;
  b = b | digitalReadFast(uP_MEGA_D6);
  b = b << 1;
  b = b | digitalReadFast(uP_MEGA_D5);
  b = b << 1;
  b = b | digitalReadFast(uP_MEGA_D4);
  b = b << 1;
  b = b | digitalReadFast(uP_MEGA_D3);
  b = b << 1;
  b = b | digitalReadFast(uP_MEGA_D2);
  b = b << 1;
  b = b | digitalReadFast(uP_MEGA_D1);
  b = b << 1;
  b = b | digitalReadFast(uP_MEGA_D0);

  return b;
}


inline __attribute__((always_inline))
byte ADDR8()
{
  byte b = 0;

  b = b | digitalReadFast(uP_MEGA_A7);
  b = b << 1;
  b = b | digitalReadFast(uP_MEGA_A6);
  b = b << 1;
  b = b | digitalReadFast(uP_MEGA_A5);
  b = b << 1;
  b = b | digitalReadFast(uP_MEGA_A4);
  b = b << 1;
  b = b | digitalReadFast(uP_MEGA_A3);
  b = b << 1;
  b = b | digitalReadFast(uP_MEGA_A2);
  b = b << 1;
  b = b | digitalReadFast(uP_MEGA_A1);
  b = b << 1;
  b = b | digitalReadFast(uP_MEGA_A0);

  return b;
}

// ##################################################
#endif
// ##################################################

void print_teensy_version()
{
#if (ARDUINO_TEENSY35)
  Serial.println("Teensy:     3.5");
#elif (ARDUINO_TEENSY36)
  Serial.println("Teensy:     3.6");
#elif (ARDUINO_TEENSY41)
  Serial.println("Teensy:     4.1");
#endif
}

#endif    // _PORTMAP_H



// Reference
//
// #define GPIO?_PDOR    (*(volatile uint32_t *)0x400FF0C0) // Port Data Output Register
// #define GPIO?_PSOR    (*(volatile uint32_t *)0x400FF0C4) // Port Set Output Register
// #define GPIO?_PCOR    (*(volatile uint32_t *)0x400FF0C8) // Port Clear Output Register
// #define GPIO?_PTOR    (*(volatile uint32_t *)0x400FF0CC) // Port Toggle Output Register
// #define GPIO?_PDIR    (*(volatile uint32_t *)0x400FF0D0) // Port Data Input Register
// #define GPIO?_PDDR    (*(volatile uint32_t *)0x400FF0D4) // Port Data Direction Register
//

    // xDATA_IN
    // // 2,14,7,8,6,20,21,5
    // CORE_PIN2_DDRREG  = CORE_PIN2_DDRREG  & (~CORE_PIN2_BITMASK);
    // CORE_PIN14_DDRREG = CORE_PIN14_DDRREG & (~CORE_PIN14_BITMASK);
    // CORE_PIN7_DDRREG  = CORE_PIN7_DDRREG  & (~CORE_PIN7_BITMASK);
    // CORE_PIN8_DDRREG  = CORE_PIN8_DDRREG  & (~CORE_PIN8_BITMASK);
    // CORE_PIN6_DDRREG  = CORE_PIN6_DDRREG  & (~CORE_PIN6_BITMASK);
    // CORE_PIN20_DDRREG = CORE_PIN20_DDRREG & (~CORE_PIN20_BITMASK);
    // CORE_PIN21_DDRREG = CORE_PIN21_DDRREG & (~CORE_PIN21_BITMASK);
    // CORE_PIN5_DDRREG  = CORE_PIN5_DDRREG  & (~CORE_PIN5_BITMASK);

    // xDATA_OUT
    // // 2,14,7,8,6,20,21,5
    // CORE_PIN2_DDRREG  = CORE_PIN2_DDRREG  | (CORE_PIN2_BITMASK);
    // CORE_PIN14_DDRREG = CORE_PIN14_DDRREG | (CORE_PIN14_BITMASK);
    // CORE_PIN7_DDRREG  = CORE_PIN7_DDRREG  | (CORE_PIN7_BITMASK);
    // CORE_PIN8_DDRREG  = CORE_PIN8_DDRREG  | (CORE_PIN8_BITMASK);
    // CORE_PIN6_DDRREG  = CORE_PIN6_DDRREG  | (CORE_PIN6_BITMASK);
    // CORE_PIN20_DDRREG = CORE_PIN20_DDRREG | (CORE_PIN20_BITMASK);
    // CORE_PIN21_DDRREG = CORE_PIN21_DDRREG | (CORE_PIN21_BITMASK);
    // CORE_PIN5_DDRREG  = CORE_PIN5_DDRREG  | (CORE_PIN5_BITMASK);

// TEENSY4.1 
//
// CORE_PIN28_BIT
// CORE_PIN28_BITMASK
// CORE_PIN28_PORTREG
// CORE_PIN28_PORTSET
// CORE_PIN28_PORTCLEAR
// CORE_PIN28_PORTTOGGLE
// CORE_PIN28_DDRREG	(1=OUT, 0=IN)
// CORE_PIN28_PINREG

