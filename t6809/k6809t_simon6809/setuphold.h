#ifndef _SETUPHOLD_H
#define _SETUPHOLD_H

// ##################################################
// Adjust setup/hold times based on Teensy
// Teensy 3.5 = 120Mhz  (1x)
// Teensy 3.6 = 180Mhz  (1.5x)
// Teensy 4.1 = 600Mhz  (5x)
// ##################################################
#if (ARDUINO_TEENSY35)

  // Add more margin to meet mininums for regular 6809E (non-A, non-B, non-C) 
  #define DELAY_UNIT()         asm volatile("nop\nnop\nnop\nnop\nnop\nnop\n");   // ~50ns
  #define DELAY_FACTOR_QH()    {DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT();}    // ~250ns
  #define DELAY_FACTOR_QL()    {DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT();}    // ~250ns
  #define DELAY_FACTOR_EH()    {DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT();}    // ~250ns
  #define DELAY_FACTOR_EL()    {DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT();}    // ~250ns

  #define DELAY_FOR_BUFFER()  {DELAY_UNIT(); DELAY_UNIT(); }
  
#elif (ARDUINO_TEENSY36)

  // Add more margin to meet mininums for regular 6809E (non-A, non-B, non-C) 
  #define DELAY_UNIT()         asm volatile("nop\nnop\nnop\nnop\nnop\nnop\nnop\nnop\nnop\n");   // ~50ns
  #define DELAY_FACTOR_QH()    {DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT();}    // ~250ns
  #define DELAY_FACTOR_QL()    {DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT();}    // ~250ns
  #define DELAY_FACTOR_EH()    {DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT();}    // ~250ns
  #define DELAY_FACTOR_EL()    {DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT();}    // ~250ns

  #define DELAY_FOR_BUFFER()  {DELAY_UNIT(); DELAY_UNIT(); } // add more time for READ SETUP TIME. asm volatile("nop\nnop\nnop\nnop\nnop\n");

#elif (ARDUINO_TEENSY41)

  #define DELAY_UNIT()        asm volatile("nop\nnop\nnop\nnop\nnop\nnop\n" \
                                           "nop\nnop\nnop\nnop\nnop\nnop\n" \
                                           "nop\nnop\nnop\nnop\nnop\nnop\n" \
                                           "nop\nnop\nnop\nnop\nnop\nnop\n" \
                                           "nop\nnop\nnop\nnop\nnop\nnop\n" \
                                           "nop\nnop\nnop\nnop\nnop\nnop\n" \
                                           "nop\nnop\nnop\nnop\nnop\nnop\n" \
                                           "nop\nnop\nnop\nnop\nnop\nnop\n" \
                                           "nop\nnop\nnop\nnop\nnop\nnop\n" \
                                           "nop\nnop\nnop\nnop\nnop\nnop\n" \
                                           "nop\nnop\nnop\nnop\nnop\nnop\n" \
                                           "nop\nnop\nnop\nnop\nnop\nnop\n")      // ~ 50ns

  // Add more margin to meet mininums for regular 6809E (non-A, non-B, non-C) 
  #define DELAY_FACTOR_QH()    {DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); }
  #define DELAY_FACTOR_QL()    {DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); }
  #define DELAY_FACTOR_EH()    {DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); }
  #define DELAY_FACTOR_EL()    {DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); }

  #define DELAY_FOR_BUFFER()  {DELAY_UNIT(); DELAY_UNIT(); } // add more time for READ SETUP TIME. asm volatile("nop\nnop\nnop\nnop\nnop\nnop\nnop\nnop\nnop\nnop\n");

#endif

#endif  // _SETUPHOLD_H