#ifndef _K4004_BUSICOM_ROM_H
#define _K4004_BUSICOM_ROM_H

////////////////////////////////////////////////////////////////////
// Busicom 141-PF ROM Code
// downloaded from
// https://www.4004.com/
// https://www.4004.com/assets/busicom-141pf-simulator-w-flowchart-071113.zip
// https://www.4004.com/busicom-simulator-documentation.html
////////////////////////////////////////////////////////////////////


// Huge THANKS to following people for extracting rare ROM images
// and making them available for public.  Now we can have history
// running.

// Team Personnel (in alphabetical order)
//   Allen E. Armstrong, mechanical engineering
//   Fred Huettig, netlist extraction software, electrical engineering, FPGA programming
//   Lajos Kintli, reverse-engineering, software analysis, commenting, documentation
//   Tim McNerney, conceptual design, project leader, digital design
//   Barry Silverman, reverse-engineering, simulation software
//   Brian Silverman, reverse-engineering, simulation software
// Collaborators and advisors
//   Federico Faggin, CEO Foveon, 4004 chip designer
//   Sellam Ismail, proprietor, Vintage Tech
//   Tom Knight, MIT CSAIL, VLSI history expert
//   Tracey Mazur, Curator, Intel Museum
//   Dag Spicer, Curator, Computer History Museum
//   Rachel Stewart, former Intel archivist
// Other contributors
//   Christian Bassow, owner of a Busicom 141-PF, who kindly sent me photographs of the PCB.
//   Karen Joyce, electronic assembler extraordinaire
//   Matt McNerney, webmaster


////////////////////////////////////////////,
// busicode - Verilog memory list,
// created from ..\asm\busicode.hex by hex2code.pl,
//  StarBoard Design,

// ROM(s) (Monitor)
#define ROM_START   ((word) 0x0000)
#define ROM_END     ((word) (ROM_START+sizeof(rom_bin)-1))
#define rom_bin     rom_bin_BUSICOM
// #define rom_bin     rom_bin_DEBUG

PROGMEM const unsigned char rom_bin_BUSICOM[] = {  
  0xF0, 0x11, 0x01, 0x50, 0xB0, 0x51, 0x5F, 0xAD, 0xB1, 0xF0, 0x51, 0x5F, 0xAD, 0x1C, 0x29, 0x68,
  0x51, 0x73, 0x27, 0xEC, 0xF5, 0xB3, 0x68, 0xF0, 0x51, 0xA0, 0xF3, 0xB3, 0xF5, 0xE1, 0x66, 0x27,
  0xEA, 0xF5, 0xF7, 0x14, 0x00, 0x52, 0x46, 0x40, 0x00, 0xB0, 0xEC, 0xF8, 0xF8, 0xE4, 0x27, 0xEA,
  0xE7, 0x50, 0x64, 0x27, 0xEA, 0xE6, 0x34, 0x20, 0xA0, 0xA5, 0xB1, 0x30, 0x68, 0x51, 0x73, 0xD0,
  0xE1, 0xD1, 0xF3, 0xF5, 0xFC, 0x85, 0x1A, 0x00, 0xF0, 0x00, 0x00, 0x11, 0x4F, 0x50, 0xB0, 0x26,
  0x20, 0x28, 0x10, 0x53, 0x00, 0x51, 0x00, 0x71, 0x5A, 0x60, 0x14, 0x4B, 0xF7, 0x14, 0x57, 0x43,
  0x02, 0xD4, 0x40, 0xD4, 0xD3, 0x29, 0xE2, 0xD0, 0xE2, 0xC0, 0x6C, 0x22, 0x20, 0x23, 0xEA, 0xF6,
  0x73, 0x6D, 0x1A, 0x76, 0xF0, 0xBC, 0xC0, 0xA9, 0x14, 0xD9, 0x28, 0x00, 0xF0, 0x51, 0x4A, 0x40,
  0xF7, 0xBB, 0xC7, 0x63, 0x53, 0x19, 0x1A, 0x68, 0x58, 0x05, 0x41, 0x31, 0x18, 0x22, 0x12, 0x05,
  0x0C, 0x9D, 0x6D, 0x3D, 0xBD, 0x8D, 0x5D, 0x2D, 0x06, 0x7D, 0x4D, 0x1D, 0x0D, 0xAD, 0xA4, 0x0E,
  0xBF, 0x06, 0x91, 0x98, 0xF1, 0xCD, 0xD7, 0xFD, 0x8A, 0x05, 0x61, 0xF9, 0xD7, 0xD7, 0xCA, 0xC5,
  0x50, 0x6A, 0x28, 0x07, 0x50, 0x64, 0x79, 0xB4, 0x26, 0x18, 0x22, 0x00, 0xD1, 0x50, 0x65, 0x27,
  0xEA, 0xFC, 0xB9, 0xA2, 0xF5, 0xF7, 0x1C, 0x77, 0xA9, 0x79, 0xCD, 0x40, 0x7A, 0x14, 0x61, 0xB2,
  0xF5, 0xFA, 0xF6, 0xB2, 0x83, 0xB3, 0xD0, 0x82, 0xB2, 0x50, 0x64, 0x77, 0xBF, 0x29, 0xA2, 0xF5,
  0xF7, 0x14, 0xF8, 0xEF, 0xF2, 0xF7, 0x1C, 0xF7, 0xEC, 0xB9, 0x29, 0xA3, 0xE0, 0x69, 0x29, 0xE9,
  0x1C, 0x7A, 0xA2, 0xE0, 0x69, 0xA9, 0xE4, 0xDF, 0xE7, 0x28, 0x00, 0x26, 0x10, 0x19, 0xFD, 0xC0,
  0x33, 0xA5, 0xF2, 0xF2, 0x86, 0xB8, 0xB6, 0x41, 0x0E, 0x66, 0x66, 0x66, 0x66, 0x66, 0x27, 0xE9,
  0x29, 0xE0, 0x69, 0x77, 0x0E, 0x27, 0xEC, 0xB3, 0xED, 0x29, 0xE5, 0xB3, 0xE4, 0xC0, 0xD4, 0x85,
  0xB6, 0x29, 0xE9, 0x27, 0xEB, 0xFB, 0xE0, 0x69, 0x77, 0x21, 0xF1, 0xC0, 0xD4, 0x85, 0xB8, 0x41,
  0x33, 0xD4, 0x85, 0xB6, 0xFA, 0xF9, 0x29, 0xE8, 0xF1, 0x27, 0xEB, 0xFB, 0xE0, 0x69, 0x77, 0x35,
  0x1A, 0x43, 0x6D, 0xC1, 0x68, 0x68, 0x68, 0x68, 0x68, 0x68, 0x29, 0xE0, 0x79, 0x4A, 0xE4, 0xE5,
  0xC0, 0x68, 0x68, 0x29, 0xE9, 0xBD, 0xE0, 0x79, 0x53, 0xC0, 0xDE, 0xB9, 0xAD, 0x68, 0x68, 0xBD,
  0xA9, 0xF8, 0xF1, 0xB9, 0x29, 0xE9, 0xBD, 0xE0, 0xA9, 0x1C, 0x61, 0xC0, 0x68, 0x68, 0x29, 0xEE,
  0xF8, 0xF3, 0xC1, 0x68, 0x68, 0x68, 0x68, 0x29, 0xEE, 0xF6, 0xC1, 0x27, 0xEE, 0xF5, 0xC1, 0x66,
  0x66, 0x66, 0x27, 0xE6, 0xC0, 0x66, 0x66, 0xD1, 0x41, 0x81, 0xD8, 0x41, 0x81, 0xA4, 0x41, 0x82,
  0x27, 0xEE, 0xF5, 0xFA, 0xF6, 0xE6, 0xC0, 0xD4, 0x85, 0xB8, 0xDE, 0xB9, 0x41, 0xA2, 0xDE, 0xB9,
  0xBD, 0x68, 0x29, 0xDF, 0xEB, 0x79, 0xA2, 0xF3, 0xC1, 0xAD, 0xFB, 0xC1, 0xAD, 0xF8, 0xBD, 0xF3,
  0xC1, 0xD7, 0x95, 0xC1, 0xDC, 0x84, 0xC1, 0xA5, 0xF6, 0xB5, 0xF3, 0xC1, 0xA4, 0xF6, 0xC1, 0xD4,
  0x85, 0xB8, 0x29, 0xAB, 0xE5, 0xC0, 0x29, 0xED, 0xBB, 0xC0, 0x7B, 0xCD, 0x6A, 0xC0, 0xDD, 0x9B,
  0xF3, 0xD0, 0x9A, 0xC1, 0x42, 0xD3, 0x00, 0x42, 0x94, 0x42, 0xA3, 0x42, 0xAA, 0x42, 0xAE, 0x42,
  0xB3, 0x42, 0xB9, 0x42, 0xCA, 0x42, 0xDE, 0x42, 0xE7, 0x42, 0xEC, 0x42, 0x46, 0x44, 0x00, 0x51,
  0x80, 0x51, 0x81, 0x51, 0x81, 0x2A, 0x00, 0x40, 0x00, 0x6F, 0x6F, 0x6F, 0x6F, 0x6F, 0x6F, 0xBF,
  0xF4, 0xBF, 0x7F, 0x10, 0xDA, 0x51, 0x4A, 0x2E, 0xFF, 0xBA, 0xDF, 0xB9, 0x29, 0xE0, 0x42, 0x2C,
  0x7F, 0x17, 0xDF, 0xBF, 0xA5, 0x42, 0x26, 0xD1, 0x8F, 0xF7, 0x14, 0x25, 0xA4, 0xBF, 0xBE, 0xF6,
  0xF3, 0xDE, 0xF6, 0x42, 0x26, 0xA4, 0xBE, 0x29, 0xED, 0xBA, 0xED, 0xBB, 0x11, 0x2C, 0xD2, 0xBD,
  0xEC, 0xF6, 0xF7, 0xE1, 0x50, 0xB0, 0x68, 0x6B, 0xAB, 0xB9, 0x51, 0xA2, 0xF7, 0x14, 0x37, 0x11,
  0x3F, 0xF0, 0xE1, 0xE2, 0x7D, 0x53, 0x2A, 0x0C, 0x2E, 0x00, 0xD8, 0x11, 0x4B, 0xE1, 0x50, 0xB0,
  0x7B, 0x4B, 0xC0, 0x50, 0x6A, 0xB8, 0xDD, 0x9F, 0xF1, 0x1C, 0x5F, 0xBA, 0xDF, 0x42, 0x61, 0x27,
  0xE9, 0x77, 0x77, 0xAA, 0x1C, 0x68, 0x52, 0x8F, 0xAF, 0x52, 0x8A, 0xAE, 0x52, 0x8A, 0x19, 0x6E,
  0xD2, 0x29, 0xE1, 0x50, 0xB2, 0x42, 0x3F, 0x52, 0x8A, 0xAA, 0x14, 0x83, 0x97, 0xF1, 0x1C, 0x83,
  0xDA, 0x52, 0x8A, 0xA7, 0x9B, 0xF7, 0x14, 0x56, 0x42, 0x5C, 0x9C, 0xF1, 0x1C, 0x8F, 0xFA, 0xD1,
  0xF5, 0xF5, 0x40, 0x65, 0xA4, 0xBD, 0x26, 0x40, 0x27, 0xEE, 0x1C, 0xA1, 0xD8, 0xE6, 0xF0, 0x51,
  0x4A, 0x41, 0xC6, 0x51, 0x0A, 0x51, 0x46, 0x51, 0x49, 0xC0, 0xD3, 0xB5, 0xFA, 0xC1, 0xD1, 0xB3,
  0xBB, 0x42, 0xC2, 0x52, 0xF9, 0xEF, 0xE5, 0x42, 0xC2, 0x52, 0xF9, 0xDD, 0x9B, 0xF1, 0x82, 0xBA,
  0xF7, 0xBA, 0x93, 0xBB, 0xF3, 0xBA, 0x99, 0xBA, 0xF1, 0xC0, 0x52, 0xF9, 0xA3, 0x8B, 0x82, 0xBB,
  0xF7, 0xBA, 0xC0, 0xD4, 0x85, 0xB6, 0x27, 0xEC, 0xB2, 0x29, 0xEC, 0x82, 0xF6, 0xC1, 0x52, 0xD6,
  0xF7, 0x66, 0x27, 0xE4, 0xDF, 0xBD, 0xC0, 0x29, 0xEC, 0xF4, 0xE4, 0xC0, 0xDB, 0x8D, 0x1A, 0xF1,
  0x6E, 0xD0, 0x29, 0xEB, 0xFB, 0xE0, 0x79, 0xF1, 0xC0, 0x27, 0xED, 0xB2, 0x29, 0xED, 0xB3, 0xC0,
  0x32, 0xC0, 0x30, 0x40, 0x4B, 0xED, 0x6C, 0x14, 0x75, 0x0E, 0xD9, 0xFC, 0xA7, 0x0F, 0xFB, 0x8D,
  0x04, 0x02, 0x87, 0xEF, 0xFC, 0x6D, 0x0F, 0x7B, 0x0F, 0x76, 0x46, 0x8D, 0xA2, 0x3C, 0x48, 0xA0,
  0x73, 0xE1, 0x9E, 0x32, 0x9A, 0x36, 0xE5, 0x51, 0x51, 0x34, 0x29, 0x21, 0x51, 0xA9, 0x3F, 0x52,
  0xA7, 0x29, 0x52, 0xCA, 0xA7, 0x22, 0x53, 0xCF, 0x3C, 0xDD, 0xA7, 0x24, 0xFF, 0x85, 0xF1, 0x5D,
  0xCE, 0x73, 0xDD, 0x5D, 0xA7, 0x40, 0x8D, 0x03, 0xE3, 0xE5, 0x0E, 0x49, 0x52, 0x5E, 0x5A, 0xA9,
  0x56, 0xAC, 0x4D, 0x21, 0xA7, 0x51, 0xA0, 0x40, 0xCF, 0x3C, 0x5E, 0x5A, 0xDD, 0xA7, 0x56, 0x86,
  0xF3, 0xFE, 0xCA, 0xCA, 0xA7, 0x67, 0xFE, 0x7B, 0x6F, 0x90, 0x76, 0x47, 0x02, 0xA7, 0x1C, 0x04,
  0x0C, 0xA7, 0x6A, 0x0D, 0xC2, 0xB1, 0x10, 0xB4, 0x7B, 0x6E, 0x9E, 0xDF, 0xCF, 0x9A, 0xCE, 0x84,
  0xCA, 0x5F, 0xA7, 0x7C, 0xDD, 0x53, 0x9A, 0x7C, 0xA7, 0x3C, 0x6C, 0x66, 0x75, 0x66, 0xD9, 0xA7,
  0x98, 0x6C, 0x98, 0x75, 0x97, 0xA7, 0x98, 0x82, 0xAE, 0x7B, 0xB1, 0xAA, 0x77, 0xA3, 0xFD, 0xEB,
  0xB4, 0xA8, 0xF1, 0xE9, 0x9A, 0x9E, 0xA7, 0x3C, 0xDB, 0xAB, 0xFC, 0xC6, 0x03, 0xBC, 0xB0, 0xE7,
  0xD4, 0xB7, 0x1E, 0x97, 0xBD, 0x31, 0x3C, 0x31, 0xBD, 0x1E, 0x2C, 0xBC, 0x01, 0x0D, 0xBF, 0xB4,
  0xFF, 0xB7, 0xAC, 0x8A, 0xEF, 0x82, 0x49, 0xD9, 0x4A, 0xFC, 0x4A, 0x7F, 0xF1, 0x6C, 0xD5, 0x75,
  0xD3, 0x0B, 0x46, 0xFC, 0xEF, 0xFD, 0xF1, 0xD7, 0xA9, 0xDF, 0x53, 0x9A, 0xE3, 0x5F, 0xF3, 0xBC,
  0x5F, 0xE7, 0xF3, 0x74, 0xE8, 0xA7, 0xEE, 0x00, 0xCA, 0xCE, 0xED, 0xDD, 0x5F, 0xC2, 0xB7, 0xDA,
  0xF3, 0xFD, 0x02, 0x0E, 0x03, 0x0C, 0x04, 0x0D, 0xF1, 0x09, 0xFA, 0x44, 0xF1, 0x09, 0xFA, 0xF1,
  0x20, 0x28, 0x11, 0x06, 0x50, 0xB0, 0x26, 0x20, 0x28, 0x10, 0x32, 0xF0, 0x54, 0x50, 0x71, 0x11,
  0x60, 0x14, 0x02, 0xF7, 0x14, 0x0E, 0x30, 0x44, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x51, 0xA7, 0x53, 0x61, 0x3E, 0x65, 0x63, 0x44,
  0x9C, 0x5B, 0x55, 0x6A, 0x36, 0x58, 0x7A, 0x5D, 0x41, 0x5F, 0x85, 0x57, 0x98, 0x35, 0xA9, 0x5B,
  0x9F, 0x7A, 0x96, 0x36, 0x59, 0x93, 0x2E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x33, 0x41, 0xFE, 0x41, 0x48, 0x41, 0x4A, 0x68, 0x68, 0x41, 0x53, 0x41, 0x04, 0x41, 0x34, 0x41,
  0x21, 0x41, 0xA2, 0x41, 0x9A, 0xBD, 0x29, 0xED, 0xBB, 0xC0, 0x2E, 0x6D, 0xAB, 0xB7, 0xBA, 0xF6,
  0xAB, 0xF6, 0x8E, 0xBB, 0xF7, 0xBA, 0xB7, 0xF6, 0xF3, 0xC1, 0xAF, 0xB9, 0xFA, 0xD0, 0x29, 0xEB,
  0xFB, 0xE0, 0x79, 0x7D, 0xC0, 0xAF, 0xB9, 0xF3, 0x29, 0xE9, 0x97, 0x12, 0x8E, 0xD9, 0xE0, 0x79,
  0x87, 0xF0, 0xC0, 0x7B, 0x96, 0x6A, 0xFA, 0xC1, 0xAF, 0xF8, 0xBF, 0xC1, 0x41, 0x5F, 0x00, 0x27,
  0xE6, 0x20, 0x40, 0x26, 0x00, 0x40, 0x4B, 0x41, 0x02, 0x41, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

PROGMEM const unsigned char rom_bin_DEBUG[] = {
// 0000: START
// 0000:        FIM P0 $A2      20 A2
// 0002:        LD  R0          A0
// 0003:        ADD R1          81
// 0004:        XCH R1          B1
// 0005: DONE   
// 0005:        JUN START       40 00
  
  0x20, 0xA2, 0xA0, 0x81, 0xB1, 0x40, 0x00
};

// Disassembler output
// 
// 000: 20 FIM     ; Fetch immediate from ROM into reg pair 0_1
// 001: A2         ; ^^^^^^^^ ^^^^^^^^
// 002: A0 LD      ; load registter 0 into accumulator
// 003: 81 ADD CY   ; add register 1 and carry to accumulator
// 004: B1 XCH     ; exchange registter 1 with accumulator
// 005: 40 JUN     ; jump unconditional
// 006: 00         ; ^^^^^^^^ ^^^^^^^^

#endif
