////////////////////////////////////////////////////////////////////
// RetroShield 4004 - Empty Project
// 2024/11/06
// Version 0.9
// Erturk Kocalar

// The MIT License (MIT)
//
// Copyright (c) 2024 Erturk Kocalar, 8Bitforce.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
// Date         Comments                                            Author
// -----------------------------------------------------------------------------
// 11/06/2020   Initial Release.                                    E. Kocalar


////////////////////////////////////////////////////////////////////
//  READ ME
//      - This is a template for anybody who wants to run their
//        own 4004 assembly program.
//      - By default it spews disassembly through serial port.
//        set "outputDEBUG_dis" to 0 to disable it (see #define below)
//      - You will need to implement your own I/O  emulation hw
//        unless you interact w/ 4004 code via RAM contents/IO's.
//        (I felt busicom keyboard/printer is too complicated)
//      - TODO: I might implement TTY or simple char input/output
//      - NOTE: Too much Serial spew will  impact 4004 timings. FYI.
//
// k4004_rom.h
//      - Replace image in k4004_rom.h with yours.
//
// k4004_ram.h
//      - All possible RAM scenarios are implemented in k4004_ram.h.
//          byte RAM_CHARS  [RAM_BANKS][RAM_CHIPnREG]  [RAM_CHARS_PER_REG];
//          byte RAM_STATUS [RAM_BANKS][RAM_CHIPnREG] [RAM_STAT_PER_REG];
//          byte RAM_IO     [RAM_BANKS][RAM_CHIPnREG>>2];
//          byte ROM_IO     [ROM_BANKS];
//        You can access RAM using i.e. RAM_CHARS[0][0][3].
//        
// k4004_IO_process()
//      - if you want to implement your desired I/O, add it in
//        k4004_IO_process() which gets called every A1 cycle.
//        As an example, I left code for how the 4003 level shifter
//        is emulated.  When 4004 accesses ROM and RAM I/O ports,
//        they are done thru variables RAM_IO[][] and ROM_IO[].
//        You can  read/write them in k4004_IO_process() and do 
//        what you like.  
////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////
// Options
//   outputDEBUG: Print memory access debugging messages.
////////////////////////////////////////////////////////////////////
#define outputDEBUG       0             // hardware debugging
#define outputDEBUG_IO    0             // Print RAM & port I/O details
#define outputDEBUG_dis   1             // disassemble code as executed
bool    uP_IO_executed    = false;      // used by diagnostics output

////////////////////////////////////////////////////////////////////
// Busicom 141-PF ROM
////////////////////////////////////////////////////////////////////
#include "k4004_rom.h"
#include "k4004_ram.h"
#include "setuphold.h"
#include "portmap.h"

////////////////////////////////////////////////////////////////////
// 4004 Processor Control
////////////////////////////////////////////////////////////////////
//

enum CPU_STATE_T {
  CPU_RESET_AND_SYNC,         // Assert reset and look for SYNC
  CPU_A1, CPU_A2, CPU_A3,
  CPU_M1, CPU_M2,
  CPU_X1, CPU_X2, CPU_X3
} cpu_state, next_cpu_state;

word uP_ADDR;
byte uP_DATA;
int reset_cycles = (8*1000);
char tmp[200];      // used for debug purposes

////////////////////////////////////////////////////////////////////
inline __attribute__((always_inline))
void uP_init()
{
  // Arduino DataBus Buffer - Disable (CPU drives databus)
  pinMode(DRIVE_DBUS, OUTPUT);      digitalWrite(DRIVE_DBUS, LOW);

  pinMode(uP_RESET,   OUTPUT);      digitalWrite(uP_RESET,  LOW);
  pinMode(uP_PHI1,    OUTPUT);      digitalWrite(uP_PHI1,   HIGH);
  pinMode(uP_PHI2,    OUTPUT);      digitalWrite(uP_PHI2,   HIGH);
  pinMode(uP_TEST,    OUTPUT);      digitalWrite(uP_TEST,   HIGH);
  pinMode(uP_SYNC,    INPUT);
  pinMode(uP_CMRAM0,  INPUT);
  pinMode(uP_CMRAM1,  INPUT);
  pinMode(uP_CMRAM2,  INPUT);
  pinMode(uP_CMRAM3,  INPUT);
  pinMode(uP_CMROM,   INPUT);

  // CPU DATA OUT to Arduino
  pinMode(uP_D0 /*49*/, INPUT);  // D0
  pinMode(uP_D1 /*48*/, INPUT);  // D1
  pinMode(uP_D2 /*47*/, INPUT);  // D2
  pinMode(uP_D3 /*46*/, INPUT);  // D3
  
  // Arduino DATA OUT to CPU
  pinMode(ARDUINO_D0 /*37*/, OUTPUT); digitalWrite(ARDUINO_D0 /*37*/, HIGH); // D0
  pinMode(ARDUINO_D1 /*36*/, OUTPUT); digitalWrite(ARDUINO_D1 /*36*/, HIGH); // D1
  pinMode(ARDUINO_D2 /*35*/, OUTPUT); digitalWrite(ARDUINO_D2 /*35*/, HIGH); // D2
  pinMode(ARDUINO_D3 /*34*/, OUTPUT); digitalWrite(ARDUINO_D3 /*34*/, HIGH); // D3

  // RAMQ to Arduino
  pinMode(RAM_Q3 /*30*/, OUTPUT); digitalWrite(RAM_Q3,  LOW);
  pinMode(RAM_Q2 /*31*/, OUTPUT); digitalWrite(RAM_Q2,  LOW);
  pinMode(RAM_Q1 /*32*/, OUTPUT); digitalWrite(RAM_Q1,  LOW);
  pinMode(RAM_Q0 /*33*/, OUTPUT); digitalWrite(RAM_Q0,  LOW);

  uP_assert_reset();
}

////////////////////////////////////////////////////////////////////
inline __attribute__((always_inline))
void uP_assert_reset()
{
  digitalWrite(uP_RESET,  LOW);           // Assert RESET

  cpu_state       = CPU_RESET_AND_SYNC;   // CPU_WAIT_SYNC;
  reset_cycles    = (8*1000);
}

////////////////////////////////////////////////////////////////////
inline __attribute__((always_inline))
void uP_release_reset()
{
  digitalWrite(uP_RESET, HIGH);     // Release RESET
  
  reset_cycles = 0;
}

////////////////////////////////////////////////////////////////////
// 4003 Shifter chip
////////////////////////////////////////////////////////////////////
unsigned long kbd_i4003_shifter_output      = 0;
bool          kbd_i4003_shifter_output_prev = false;

unsigned long prt_i4003_shifter_output      = 0;
bool          prt_i4003_shifter_clk_prev    = false;

inline __attribute__((always_inline))
void k4004_IO_process()
{
  //////////////////////////////
  // Printer Shifter (2x 4003 10bit shifters conneccted in series)

  if ( (ROM_IO[0] & 0b0100) && ( !prt_i4003_shifter_clk_prev ))
  {   
    // Rising edge of ROM_IO[0].bit2: shift ROM_IO[0].bit1 into the shifter.
    prt_i4003_shifter_output = (prt_i4003_shifter_output << 1) | ( (ROM_IO[0] >> 1) & 0b0001);
    prt_i4003_shifter_output = (prt_i4003_shifter_output     ) & 0b11111111111111111111;

    if (outputDEBUG)
    {
      Serial.print("PRINTER_SHIFTER: "); Serial.print(prt_i4003_shifter_output, HEX); Serial.println("      "); 
    }
  }
  prt_i4003_shifter_clk_prev = ROM_IO[0] & 0b0100;

  //////////////////////////////
  // Keyboard (1x 4003 10bit shifter)

  if ( (ROM_IO[0] & 0b0001) && ( !kbd_i4003_shifter_output_prev ))
  {
    // rising edge of CP
    kbd_i4003_shifter_output = (kbd_i4003_shifter_output << 1) | ( (ROM_IO[0] >> 1) & 0b0001);
    kbd_i4003_shifter_output = (kbd_i4003_shifter_output     ) & 0b1111111111;

    if (outputDEBUG)
    { 
      Serial.print("KBD_SHIFTER: "); Serial.print(kbd_i4003_shifter_output, BIN); Serial.println(""); 
    }
  }
  kbd_i4003_shifter_output_prev = ROM_IO[0] & 0b0001;

// Print lots of debug details.
//
#if outputDEBUG_IO

  // Go back to screen corner so we see movement:
  if (uP_IO_executed)
  {
     Serial.write(27);
     Serial.print("[H");
  }

  //////////////////////////////
  // Print RAM and STATUS Contents

  if (uP_IO_executed)
  {
    Serial.write("\n    RAM:\n    ");
      for (int j=0; j<8; j++)
      {
        Serial.print(j, HEX); Serial.print(": ");
        for (int k=0; k<4; k++)
          {
            sprintf(tmp, "%01X", RAM_STATUS[0][j][3-k]); 
            Serial.write(tmp);
          }
        Serial.write(" ");
        for (int k=0; k<16; k++)
          {
            sprintf(tmp, "%01X", RAM_CHARS[0][j][15-k]); 
            Serial.write(tmp);
          }
        Serial.print("\n    ");
      }

    Serial.write('\n');
  }

  //////////////////////////////
  // Print I/O States
  
  int a;

  if (uP_IO_executed)
  {
    a=0; sprintf(tmp, "    RAM IO : %01X%01X%01X%01X", RAM_IO[a][0], RAM_IO[a][1], RAM_IO[a][2], RAM_IO[a][3]); Serial.print(tmp);
    a=1; sprintf(tmp, " %01X%01X%01X%01X", RAM_IO[a][0], RAM_IO[a][1], RAM_IO[a][2], RAM_IO[a][3]); Serial.print(tmp);
    a=2; sprintf(tmp, " %01X%01X%01X%01X", RAM_IO[a][0], RAM_IO[a][1], RAM_IO[a][2], RAM_IO[a][3]); Serial.print(tmp);
    a=3; sprintf(tmp, " %01X%01X%01X%01X", RAM_IO[a][0], RAM_IO[a][1], RAM_IO[a][2], RAM_IO[a][3]); Serial.print(tmp);
    a=4; sprintf(tmp, " %01X%01X%01X%01X", RAM_IO[a][0], RAM_IO[a][1], RAM_IO[a][2], RAM_IO[a][3]); Serial.print(tmp);
    a=5; sprintf(tmp, " %01X%01X%01X%01X", RAM_IO[a][0], RAM_IO[a][1], RAM_IO[a][2], RAM_IO[a][3]); Serial.print(tmp);
    a=6; sprintf(tmp, " %01X%01X%01X%01X", RAM_IO[a][0], RAM_IO[a][1], RAM_IO[a][2], RAM_IO[a][3]); Serial.print(tmp);
    a=7; sprintf(tmp, " %01X%01X%01X%01X \n", RAM_IO[a][0], RAM_IO[a][1], RAM_IO[a][2], RAM_IO[a][3]); Serial.print(tmp);    

    sprintf(tmp, "    ROM IO : %01X%01X%01X%01X %01X%01X%01X%01X ", 
      ROM_IO[0], ROM_IO[1], ROM_IO[2], ROM_IO[3], ROM_IO[4], ROM_IO[5], ROM_IO[6], ROM_IO[7] ); 
      Serial.print(tmp);
    sprintf(tmp, "%01X%01X%01X%01X %01X%01X%01X%01X \n", 
      ROM_IO[8], ROM_IO[9], ROM_IO[10], ROM_IO[11], ROM_IO[12], ROM_IO[13], ROM_IO[14], ROM_IO[15] ); 
      Serial.println(tmp);
  }
  
#endif
  
  //////////////////////////////
  // Done
  uP_IO_executed = false;
}

//##############################
// If you want to do anything special when 4004
// is executing a specific ROM address, you can
// process it here.
// 

void process_special_addresses()
{
  switch(uP_ADDR)
  {
    // ROM: ;A pressed button is found
    case 0x029:
                break;
    // ROM: Wait for PSYNC signal.
    case 0x001:
    case 0x04b:
    case 0x22c:
    case 0x23f:
    case 0x24b:
    case 0x402:
                break;
    // ROM: Wait for inverted PSYNC signal.
    case 0x0fd:
    case 0x26e:
                break;
    default:
                break;
  }
}

////////////////////////////////////////////////////////////////////
// Rudimentary Disassembler
//
// TODO: This code does not handle multiple bits in conditional
////////////////////////////////////////////////////////////////////

bool twobyte=false;

inline __attribute__((always_inline))
void k4004_disassemble(word adr, byte opr, byte opa)
{

  //////////////////////////////
  // TODO: This code does not
  // handle multiple conditions
  // etc.
  //////////////////////////////
  
  if (twobyte)
  {
    sprintf(tmp, "%03X: %01X%01X         ; ^^^^^^^^ ^^^^^^^^", adr, opr, opa);
    Serial.println(tmp);
    twobyte = false;
  }
  else
  {
    switch (opr)
    {
      case 0b0000:
        sprintf(tmp, "%03X: %01X%01X NOP", adr, opr, opa);
        Serial.println(tmp);
        break;      
        
      case 0b0001:
        sprintf(tmp, "%03X: %01X%01X JCN ", adr, opr, opa);
        Serial.print(tmp);
        // TODO: Check if these bits can be set simultaneously
        // Dwight: there are all zeros and invert-only interesting cases.
        //         invert-only = SKIP.
        //         all zeroes = JMP.
        switch(opa)
        {
          case 0b0001:
            Serial.println("TZ  ; test=0");
            break;
          case 0b0010:
            Serial.println("C1  ; cy=1");
            break;
          case 0b0100:
            Serial.println("AZ  ; accumulator=0");
            break;
          case 0b1001:
            Serial.println("TN  ; test=1");
            break;
          case 0b1010:
            Serial.println("C0  ; cy=0");
            break;
          case 0b1100:
            Serial.println("AN  ; accumulator!=0");
            break;            
        }
        twobyte = true;
        break;      
        
      case 0b0010:
        if ((opa & 0x01) == 0x00)
        {
          // FIM Instruction
          sprintf(tmp, "%03X: %01X%01X FIM", adr, opr, opa);
          Serial.print(tmp);
          Serial.print("     ; Fetch immediate from ROM into reg pair ");
          Serial.print(2*(opa >> 1), HEX); Serial.print("_");
          Serial.println(2*(opa >> 1)+1, HEX);
          twobyte = true;
        }
        else
        {
          // SRC
          sprintf(tmp, "%03X: %01X%01X SRC", adr, opr, opa);
          Serial.print(tmp);
          Serial.print("     ; Send Register Control ");
          Serial.print( 2*(opa >> 1), HEX); Serial.print("/");
          Serial.println(2*(opa >> 1)+1, HEX);      
        }
        break;      

      case 0b0011:
        if ((opa & 0x01) == 0x00)
        {
          // FIN Instruction
          sprintf(tmp, "%03X: %01X%01X FIN", adr, opr, opa);
          Serial.print(tmp);
          Serial.print("  ; Fetch indirect from ROM thru reg pair ");
          Serial.print(2*(opa >> 1), HEX); Serial.print("_");
          Serial.println(2*(opa >> 1)+1, HEX);
        }
        else
        {
          // JIN
          sprintf(tmp, "%03X: %01X%01X JIN", adr, opr, opa);
          Serial.print(tmp);
          Serial.print("  ; Jump indirect (8bit PC = register pair ");
          Serial.print(2*(opa >> 1), HEX); Serial.print("_");
          Serial.println(2*(opa >> 1)+1, HEX);      
        }
        break;      

      case 0b0100:
        sprintf(tmp, "%03X: %01X%01X JUN     ; jump unconditional", adr, opr, opa);
        Serial.println(tmp);
        twobyte = true;
        break;      
    
      case 0b0101:
        sprintf(tmp, "%03X: %01X%01X JMS    ; jump to subroutine", adr, opr, opa);
        Serial.println(tmp);
        twobyte = true;
        break;      

      case 0b0110:
        sprintf(tmp, "%03X: %01X%01X INC   ; increment register ", adr, opr, opa);
        Serial.print(tmp);
        Serial.println(opa, HEX);
        break;      

      case 0b0111:
        sprintf(tmp, "%03X: %01X%01X ISZ      ; increment register ", adr, opr, opa);
        Serial.print(tmp);
        Serial.print(opa, HEX);
        Serial.println(", jump to address if non-zero");
        twobyte = true;
        break;      

      case 0b1000:
        sprintf(tmp, "%03X: %01X%01X ADD CY   ; add register ", adr, opr, opa);
        Serial.print(tmp);
        Serial.print(opa, HEX);
        Serial.println(" and carry to accumulator");
        break;      

      case 0b1001:
        sprintf(tmp, "%03X: %01X%01X SUB CY   ; subtract register ", adr, opr, opa);
        Serial.print(tmp);
        Serial.print(opa, HEX);
        Serial.println(" and borrow from accumulator");
        break;      

      case 0b1010:
        sprintf(tmp, "%03X: %01X%01X LD      ; load registter ", adr, opr, opa);
        Serial.print(tmp);
        Serial.print(opa, HEX);
        Serial.println(" into accumulator");
        break;      

      case 0b1011:
        sprintf(tmp, "%03X: %01X%01X XCH     ; exchange registter ", adr, opr, opa);
        Serial.print(tmp);
        Serial.print(opa, HEX);
        Serial.println(" with accumulator");
        break;      

      case 0b1100:
        sprintf(tmp, "%03X: %01X%01X BBL     ; branch back and load data ", adr, opr, opa);
        Serial.print(tmp);
        Serial.print(opa, HEX);
        Serial.println(" to accumulator");
        break;      

      case 0b1101:
        sprintf(tmp, "%03X: %01X%01X LDM     ; load data ", adr, opr, opa);
        Serial.print(tmp);
        Serial.print(opa, HEX);
        Serial.println(" into accumulator");
        break;      

      case 0b1110:
        switch (opa) 
        {
          case 0b0000: // WRM - write ACC to memory
            sprintf(tmp, "%03X: %01X%01X WRM     ; write ACC to memory", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b0001: // WMP - write ACC to RAM output port
            sprintf(tmp, "%03X: %01X%01X WMP     ; write ACC to RAM output port", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b0010: // WRR - write ACC to ROM output port
            sprintf(tmp, "%03X: %01X%01X WRR     ; write ACC to ROM output port", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b0011: // WPM - write ACC to executable RAM (4008/4009 only)
            sprintf(tmp, "%03X: %01X%01X WPM     ; write ACC to executable RAM (4008/4009 only)", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b0100: // WR0 - write ACC to RAM status 0
            sprintf(tmp, "%03X: %01X%01X WR0     ; write ACC to RAM status 0", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b0101: // WR1 - write ACC to RAM status 1
            sprintf(tmp, "%03X: %01X%01X WR1     ; write ACC to RAM status 1", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b0110: // WR2 - write ACC to RAM status 2
            sprintf(tmp, "%03X: %01X%01X WR2     ; write ACC to RAM status 2", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b0111: // WR3 - write ACC to RAM status 3
            sprintf(tmp, "%03X: %01X%01X WR3     ; write ACC to RAM status 3", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b1000: // SBM - subtract RAM character from ACC
            sprintf(tmp, "%03X: %01X%01X SBM CY  ; subtract RAM character and borrow from ACC", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b1001: // RDM - read RAM char to ACC
            sprintf(tmp, "%03X: %01X%01X RDM     ; read RAM char to ACC", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b1010: // RDR - read ROM input port to ACC
            sprintf(tmp, "%03X: %01X%01X RDR     ; read ROM input port to ACC", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b1011: // ADM - add RAM char to ACC
            sprintf(tmp, "%03X: %01X%01X ADM CY  ; add RAM char and carry to ACC", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b1100: // RD0 - read RAM status 0 to ACC
            sprintf(tmp, "%03X: %01X%01X RD0     ; read RAM status 0 to ACC", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b1101: // RD1 - read RAM status 1 to ACC
            sprintf(tmp, "%03X: %01X%01X RD1     ; read RAM status 1 to ACC", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b1110: // RD2 - read RAM status 2 to ACC
            sprintf(tmp, "%03X: %01X%01X RD2     ; read RAM status 2 to ACC", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b1111: // RD3 - read RAM status 3 to ACC
            sprintf(tmp, "%03X: %01X%01X RD3     ; read RAM status 3 to ACC", adr, opr, opa);
            Serial.println(tmp);
            break;
          default: // Error
            Serial.write("ERROR - unknown I/O cmd OPA\n"); 
            break;
        }        
        break;      

      case 0b1111:
        // TODO: Check if these bits can be set simultaneously
        switch (opa) 
        {
          case 0b0000:
            sprintf(tmp, "%03X: %01X%01X CLB 0   ; clear both accumulator and carry", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b0001:
            sprintf(tmp, "%03X: %01X%01X CLC 0   ; clear carry", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b0010:
            sprintf(tmp, "%03X: %01X%01X IAC CY  ; increment accumulator", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b0011:
            sprintf(tmp, "%03X: %01X%01X CMC CY  ; complement carry", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b0100:
            sprintf(tmp, "%03X: %01X%01X CMA     ; complement accumulator", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b0101: 
            sprintf(tmp, "%03X: %01X%01X RAL CY  ; rotate accumulator left thru carry", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b0110:
            sprintf(tmp, "%03X: %01X%01X RAR CY  ; rotate accumulator right thru carry", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b0111: 
            sprintf(tmp, "%03X: %01X%01X TCC 0   ; transmit carry to accumulator and clear carry", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b1000: 
            sprintf(tmp, "%03X: %01X%01X DAC CY  ; decrement accumulator", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b1001: 
            sprintf(tmp, "%03X: %01X%01X TCS 0   ; transmit carry subtract and clear carry", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b1010: 
            sprintf(tmp, "%03X: %01X%01X STC 1   ; set carry", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b1011: 
            sprintf(tmp, "%03X: %01X%01X DAA CY  ; decimal adjust accumulator", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b1100: 
            sprintf(tmp, "%03X: %01X%01X KBP     ; keyboard process", adr, opr, opa);
            Serial.println(tmp);
            break;
          case 0b1101: 
            sprintf(tmp, "%03X: %01X%01X DCL     ; designate command line", adr, opr, opa);
            Serial.println(tmp);
            break;
          default: // Error
            Serial.write("ERROR - unknown OPA for 0b1111 cmd \n"); 
            break;
        }        
        break;
    } 
  }
}

////////////////////////////////////////////////////////////////////
// Processor Control Loop
////////////////////////////////////////////////////////////////////
// This is where the action is.
// it reads processor control signals and acts accordingly.
//

// Variables to keep track of IO operations
// SRC instructions sends 8bit during X2 & X3.
// I/O instructions use M1 4-bit to process 4-bit during M2.
//
byte OPR          = 0;
byte OPA          = 0;
byte M1_OPR       = 0;
byte M2_OPA       = 0;
byte M2_CMROM     = 0;
byte M2_CMRAM     = 0;
byte X2_CMROM     = 0;
byte X2_CMRAM     = 0;
byte X2_CHIPnREG  = 0;
byte X3_CHAR      = 0;
bool SRC_issued   = false;
bool IO_inst_exec = false;
bool bus_error = false;
bool wait_for_data_stabilize = false;

// Debug
byte count  = 0;


inline __attribute__((always_inline))
void cpu_tick()
{   
  //##################################################//
  // 4004 has 8 clock cycles in one instruction cycle
  // A1, A2, A3, M1, M2, X1, X3, X3
  // we start w/ WAIT_SYNC state to synchronize w/ 4004
  // then loop and verify SYNC continously.

  // Details are implemented based on the book
  // Microcomputers/Microprocessors: Hardware, Software, and Applications
  // John L. Hilburn, Paul M. Julich

  bus_error = false;    // Used to signal error conditions, which will reset the chip.

  switch(cpu_state)
  {
    // ##################################################
    // ##################################################
    // ##################################################

    case CPU_RESET_AND_SYNC:            //  assert reset and wait #reset_cycles.
      
      if (reset_cycles > 0)
      {
        digitalWrite(uP_RESET,  LOW);   // assert reset

        CLK1_LOW();                 
        CLK1_HIGH();                

        CLK2_LOW();                   
        CLK2_HIGH();                

        reset_cycles--; // reset_cycles++;
        // Init rest of board once.
        if (reset_cycles == 0)
        {
          // Do any reset initialization here if needed.
          k4004_IO_init();
          // printer_init();
          // key_registered = true;
        }

        if (0 && outputDEBUG)
        {
          sprintf(tmp, "R. SYNC=%01X ", STATE_SYNC);
          Serial.write(tmp);
          count++;
          if (count == 10)
          {
            count = 0;
            Serial.write("\n");
          }
        }
      }
      else
      {
        // Reset duration complete.
        // Look for SYNC (which happens during X3 cycle).
        digitalWrite(uP_RESET,  HIGH);    // release reset
        
        CLK1_LOW();                 
        CLK1_HIGH();                
        DELAY_MEGA_TEENSY_NS(180,180);

        if (outputDEBUG)
        {
          sprintf(tmp, "R. SYNC=%01X ", STATE_SYNC);
          Serial.write(tmp);
          count++;
          if (count == 10)
          {
            count = 0;
            Serial.write("\n");
          }
        }

        if (STATE_SYNC)
        {
          // Found SYNC signal, yay.
          if (outputDEBUG) Serial.write("S\n");
          // print_key_mappings();
          next_cpu_state = CPU_A1;
        }
        else
        {
          // keep waiting for sync
  #if (outputDEBUG)
          sprintf(tmp, "W. SYNC=%01X ", digitalRead(uP_SYNC));
          Serial.write(tmp);
          count++;
          if (count == 4)
          {
            count = 0;
            Serial.write("\n");
          }
  #endif
        }

        CLK2_LOW();      
        CLK2_HIGH();  
      }
      break;
            
    // ##################################################
    // ##################################################
    // ##################################################
    case CPU_A1:
      if (outputDEBUG) Serial.write("A1");

      // digitalWriteFast(MEGA_PA7, HIGH); DELAY_MEGA_TEENSY_NS(60,60); digitalWriteFast(MEGA_PA7, LOW); DELAY_MEGA_TEENSY_NS(60,60);

      CLK1_LOW();   
      CLK1_HIGH();  

      CLK2_LOW();  
      DELAY_MEGA_TEENSY_NS(180,180);  
      uP_ADDR = DATA4_IN();
      CLK2_HIGH();  

      k4004_IO_process();       // process I/O ports.
      // printer_rotate();         // rotate printer
      // k4004_keyboard_map();     // process keyboard

      next_cpu_state = CPU_A2;
      break;
 
    // ##################################################
    // ##################################################
    // ##################################################
    case CPU_A2:
      if (outputDEBUG) Serial.write("2");

      // digitalWriteFast(MEGA_PA7, HIGH); DELAY_MEGA_TEENSY_NS(60,60); digitalWriteFast(MEGA_PA7, LOW); DELAY_MEGA_TEENSY_NS(60,60);

      CLK1_LOW();   
      CLK1_HIGH();  

      CLK2_LOW();    
      DELAY_MEGA_TEENSY_NS(180,180);  
      uP_ADDR = uP_ADDR | (DATA4_IN() << 4);
      CLK2_HIGH();  

      // k4004_IO_process();       // process I/O ports.
      // k4004_keyboard_map();     // process keyboard
      // printer_rotate();         // rotate printer

      next_cpu_state = CPU_A3;
      break;

    // ##################################################
    // ##################################################
    // ##################################################
    case CPU_A3:
      if (outputDEBUG) Serial.write("3");

      // digitalWriteFast(MEGA_PA7, HIGH); DELAY_MEGA_TEENSY_NS(60,60); digitalWriteFast(MEGA_PA7, LOW); DELAY_MEGA_TEENSY_NS(60,60);

      CLK1_LOW();   
      CLK1_HIGH();  

      CLK2_LOW();   
      DELAY_MEGA_TEENSY_NS(180,180);  
      uP_ADDR = uP_ADDR | (DATA4_IN() << 8);

      if (outputDEBUG)
      {
        sprintf(tmp, " ADDR=%03X ", uP_ADDR);
        Serial.write(tmp);
        sprintf(tmp, " CMRO/A=%02X:%02X ", STATE_CMROM, STATE_CMRAM);
        Serial.write(tmp);
      }

      CLK2_HIGH();  

      // k4004_IO_process();       // process I/O ports.
      // k4004_keyboard_map();     // process keyboard
      // printer_rotate();         // rotate printer

      next_cpu_state = CPU_M1;
      break;

    // ##################################################
    // ##################################################
    // ##################################################
    case CPU_M1:
      if (outputDEBUG) Serial.write("M1");

      // digitalWriteFast(MEGA_PA7, HIGH); DELAY_MEGA_TEENSY_NS(60,60); digitalWriteFast(MEGA_PA7, LOW); DELAY_MEGA_TEENSY_NS(60,60);

      CLK1_LOW();  
      if ( (ROM_START <= uP_ADDR) && (uP_ADDR <= ROM_END) )
      {
        OPR = pgm_read_byte_near( rom_bin + (uP_ADDR - ROM_START) ) >> 4;      // (rom_bin [ (uP_ADDR - ROM_START) ]) >> 4;
        if (outputDEBUG) Serial.write("#");
      }
      else
      {
        OPR = 0x00;
        Serial.print("JUMP TO NON-EXISTENT ROM ADDRESS: ");
        Serial.print("--       RESETting             -- ");
        Serial.println(uP_ADDR, HEX);
        // while(1);        
        delay(1500);
        uP_assert_reset();
        bus_error = true;
      } 

      // Keep track for I/O operations.
      M1_OPR = OPR;

        DRIVE_DBUS_START;
        DATA4_OUT( OPR );

      CLK1_HIGH();  

      process_special_addresses();    // Optimize behavior for certain subroutines.

      CLK2_LOW();     
      while(!DATA4_OUT_matches_IN());   // Wait until level shifter output stabilizes

      CLK2_HIGH();  
        DELAY_MEGA_TEENSY_NS(180,180);    // Prevent race condition CLK2 vs DBUS
        DRIVE_DBUS_STOP;

      if (bus_error)
        next_cpu_state = CPU_RESET_AND_SYNC;
      else
        next_cpu_state = CPU_M2;
      break;

    // ##################################################
    // ##################################################
    // ##################################################
    case CPU_M2:
      if (outputDEBUG) Serial.write("2");        

      // digitalWriteFast(MEGA_PA7, HIGH); DELAY_MEGA_TEENSY_NS(60,60); digitalWriteFast(MEGA_PA7, LOW); DELAY_MEGA_TEENSY_NS(60,60);

      CLK1_LOW();               
      // Continue ROM read
      if ( (ROM_START <= uP_ADDR) && (uP_ADDR <= ROM_END) )
      {
        OPA = pgm_read_byte_near( rom_bin + (uP_ADDR - ROM_START) ) & 0x0F;     // (rom_bin [ (uP_ADDR - ROM_START) ]) & 0x0F;
        if (outputDEBUG) Serial.write("@");
      }
      else
      {
        OPA = 0x00;
        Serial.print("JUMP TO NON-EXISTENT ROM ADDRESS: ");
        Serial.print("--       RESETting             -- ");
        Serial.println(uP_ADDR, HEX);
        // while(1);
        delay(1500);
        uP_assert_reset();
        bus_error = true;
      }
      
      DRIVE_DBUS_START;   
      DATA4_OUT( OPA ); 

      CLK1_HIGH();             
      while(!DATA4_OUT_matches_IN());   // Wait until level shifter output stabilizes

      CLK2_LOW();  
      DELAY_MEGA_TEENSY_NS(180,180);
      if ( (STATE_CMRAM > 0) && (STATE_CMROM > 0) )       // 2024-1025: changed from || to && b/c both are asserted per datasheet.
      {
        // Need to latch this state because some instructions like FIN
        // take 2 instruction cycles.  I had a bug where IO WPM was 
        // getting executed by mistake.

        IO_inst_exec = true;

        // // Save in case of I/O for X2/X3.
        M2_OPA    = OPA;
        M2_CMROM  = STATE_CMROM;
        M2_CMRAM  = STATE_CMRAM_0321;
        // ERTURK: 2024/10/25 ?move to outside for debugger. ??why did I put it here?

#if outputDEBUG
        if (IO_inst_exec) // (M1_OPR == 0x0E)
        {
          sprintf(tmp, " IO CMRO/A=%01X:%01X  INSTR=%01X%01X ", STATE_CMROM, STATE_CMRAM_0321, M1_OPR, M2_OPA );
          Serial.println(tmp);
        }
#endif
      } 

      CLK2_HIGH();            
        DELAY_MEGA_TEENSY_NS(180,180);
        DRIVE_DBUS_STOP;

      if (outputDEBUG_dis)
        k4004_disassemble(uP_ADDR, OPR, OPA);

      if (bus_error)
        next_cpu_state = CPU_RESET_AND_SYNC;
      else
        next_cpu_state = CPU_X1;
      break;
 
    // ##################################################
    // ##################################################
    // ##################################################
    case CPU_X1:      
      if (outputDEBUG) Serial.write("X1");

      // digitalWriteFast(MEGA_PA7, HIGH); DELAY_MEGA_TEENSY_NS(60,60); digitalWriteFast(MEGA_PA7, LOW); DELAY_MEGA_TEENSY_NS(60,60);

      CLK1_LOW();   
      
      if (outputDEBUG)
      {
        sprintf(tmp, " OPA=%01X ", DATA4_IN() );
        Serial.write(tmp);
      }

      CLK1_HIGH();  

      CLK2_LOW();      
      CLK2_HIGH();  

      next_cpu_state = CPU_X2;
      break;
 
    // ##################################################
    // ##################################################
    // ##################################################
    case CPU_X2:
      if (outputDEBUG) Serial.print("2");

      // digitalWriteFast(MEGA_PA7, HIGH); DELAY_MEGA_TEENSY_NS(60,60); digitalWriteFast(MEGA_PA7, LOW); DELAY_MEGA_TEENSY_NS(60,60);

      CLK1_LOW();              

      #if (ARDUINO_AVR_MEGA2560)
        CLK1_HIGH();
      #elif ((ARDUINO_TEENSY35) || (ARDUINO_TEENSY36) || (ARDUINO_TEENSY41))
        // Teensy 4.1 needs to drive earlier.
      #endif

      //////////////////////////////////////////////////
      // 1/2: Check if this was I/O cycle
      //      We execute I/O instructions where 4004 reads from I/O here
      //      because we need to start driving the bus earlier.
      //
      if (IO_inst_exec)
      {
        IO_inst_exec = false;
        // ^^^ If not covered here, it will be set back to true in default: and retried in 2nd half.

#if outputDEBUG_IO
        uP_IO_executed = true;        // Used by debug functions to print I/O details.
#endif

        // We have to do what the OPA means:
        switch (M2_OPA) 
        {
          case 0b1000: // SBM - subtract RAM character from ACC
            DRIVE_DBUS_START;
            DATA4_OUT( RAM_CHARS[M2_CMRAM][ SRC_RAM_X2[M2_CMRAM] ][ SRC_RAM_X3[M2_CMRAM] ] );
            wait_for_data_stabilize = true;
            if (outputDEBUG) 
            {
              sprintf(tmp, " SBM %01X-%01X-%01X D:%01X", M2_CMRAM, SRC_RAM_X2[M2_CMRAM], SRC_RAM_X3[M2_CMRAM], RAM_CHARS[M2_CMRAM][SRC_RAM_X2[M2_CMRAM]][SRC_RAM_X3[M2_CMRAM]]);
              Serial.write(tmp);              
            }
            IO_inst_exec = false;
            break;
            
          case 0b1001: // RDM - read RAM char to ACC
            DRIVE_DBUS_START;
            DATA4_OUT( RAM_CHARS[M2_CMRAM][ SRC_RAM_X2[M2_CMRAM] ][ SRC_RAM_X3[M2_CMRAM] ] );
            wait_for_data_stabilize = true;
            if (outputDEBUG) 
            {
              sprintf(tmp, " RDM %01X-%01X-%01X D:%01X", M2_CMRAM, SRC_RAM_X2[M2_CMRAM], SRC_RAM_X3[M2_CMRAM], RAM_CHARS[M2_CMRAM][SRC_RAM_X2[M2_CMRAM]][SRC_RAM_X3[M2_CMRAM]]);
              Serial.write(tmp);              
            }
            IO_inst_exec = false;
            break;
            
          case 0b1010: // RDR - read ROM input port to ACC
            DRIVE_DBUS_START;
            DATA4_OUT( ROM_IO[ SRC_ROM_X2[M2_CMROM] ] );
            wait_for_data_stabilize = true;
            if (outputDEBUG) Serial.print(" RDR ");
            IO_inst_exec = false;
            break;
            
          case 0b1011: // ADM - add RAM char to ACC
            DRIVE_DBUS_START;
            DATA4_OUT( RAM_CHARS[M2_CMRAM][ SRC_RAM_X2[M2_CMRAM] ][ SRC_RAM_X3[M2_CMRAM] ] );
            wait_for_data_stabilize = true;
            if (outputDEBUG) 
            {
              sprintf(tmp, " ADM %01X-%01X-%01X D:%01X", M2_CMRAM, SRC_RAM_X2[M2_CMRAM], SRC_RAM_X3[M2_CMRAM], RAM_CHARS[M2_CMRAM][SRC_RAM_X2[M2_CMRAM]][SRC_RAM_X3[M2_CMRAM]]);
              Serial.write(tmp);              
            }
            IO_inst_exec = false;
            break;
            
          case 0b1100: // RD0 - read RAM status 0 to ACC
            DRIVE_DBUS_START;
            DATA4_OUT( RAM_STATUS[M2_CMRAM][ SRC_RAM_X2[M2_CMRAM] ][0] );
            wait_for_data_stabilize = true;
            if (outputDEBUG) 
            {
              sprintf(tmp, " RD0 %01X-%01X D:%01X", M2_CMRAM, SRC_RAM_X2[M2_CMRAM], RAM_STATUS[M2_CMRAM][SRC_RAM_X2[M2_CMRAM]][0]);
              Serial.write(tmp);              
            }
            IO_inst_exec = false;
            break;
            
          case 0b1101: // RD1 - read RAM status 1 to ACC
            DRIVE_DBUS_START;
            DATA4_OUT( RAM_STATUS[M2_CMRAM][ SRC_RAM_X2[M2_CMRAM] ][1] );
            wait_for_data_stabilize = true;
            if (outputDEBUG) 
            {
              sprintf(tmp, " RD1 %01X-%01X D:%01X", M2_CMRAM, SRC_RAM_X2[M2_CMRAM], RAM_STATUS[M2_CMRAM][SRC_RAM_X2[M2_CMRAM]][1]);
              Serial.write(tmp);              
            }
            IO_inst_exec = false;
            break;
            
          case 0b1110: // RD2 - read RAM status 2 to ACC
            DRIVE_DBUS_START;
            DATA4_OUT( RAM_STATUS[M2_CMRAM][ SRC_RAM_X2[M2_CMRAM] ][2] );
            wait_for_data_stabilize = true;
            if (outputDEBUG) 
            {
              sprintf(tmp, " RD2 %01X-%01X D:%01X", M2_CMRAM, SRC_RAM_X2[M2_CMRAM], RAM_STATUS[M2_CMRAM][SRC_RAM_X2[M2_CMRAM]][2]);
              Serial.write(tmp);              
            }
            IO_inst_exec = false;
            break;
            
          case 0b1111: // RD3 - read RAM status 3 to ACC
            DRIVE_DBUS_START;
            DATA4_OUT( RAM_STATUS[M2_CMRAM][ SRC_RAM_X2[M2_CMRAM] ][3] );
            wait_for_data_stabilize = true;
            if (outputDEBUG) 
            {
              sprintf(tmp, " RD3 %01X-%01X D:%01X", M2_CMRAM, SRC_RAM_X2[M2_CMRAM], RAM_STATUS[M2_CMRAM][SRC_RAM_X2[M2_CMRAM]][3]);
              Serial.write(tmp);
            }
            IO_inst_exec = false;
            break;
            
          default:
            IO_inst_exec = true;  // I/O instruction not covered here, try again in 2nd half.
            break;        
        }
      }

      #if (ARDUINO_AVR_MEGA2560)
        // Mega.      
      #elif ((ARDUINO_TEENSY35) || (ARDUINO_TEENSY36) || (ARDUINO_TEENSY41))
        CLK1_HIGH();
      #endif           

      CLK2_LOW();              
      DELAY_MEGA_TEENSY_NS(180,180);    // Delays dispersed in individual functions due to variable activity.

      //////////////////////////////////////////////////
      // Check if SRC instruction
      if ( (STATE_CMROM > 0) || (STATE_CMRAM > 0) )
      {
#if outputDEBUG
        sprintf(tmp, "  SRC CMRO/A=%01X:%01X  CHIPnREG=%01X ", STATE_CMROM, STATE_CMRAM_0321, DATA4_IN() );
        Serial.write(tmp);
#endif

        X2_CMROM    = STATE_CMROM;
        X2_CMRAM    = STATE_CMRAM_0321;      // >>1 required for bank0=0x08
        X2_CHIPnREG = DATA4_IN();

        // Selected bank ROM/RAM latches X2 & X3 during SRC
        SRC_RAM_X2[X2_CMRAM] = X2_CHIPnREG;    // Selected RAM latches X2 (Chip & Reg)
        SRC_ROM_X2[X2_CMROM] = X2_CHIPnREG;    // Selected ROM latches X2 (Chip 4bits)

        SRC_issued = true;

      }
      else
      //////////////////////////////////////////////////
      // 2/2: Check if this was I/O cycle
      //      We execute I/O instructions where 4004 writes to I/O here
      //      because data is valid when CLK2=low.
      //

      if (IO_inst_exec)
      {
        IO_inst_exec = false;

#if outputDEBUG_IO
        uP_IO_executed = true;        // Used by debug functions to print I/O details.
#endif

        // We have to do what the OPA means:
        switch (M2_OPA) 
        {
          case 0b0000: // WRM - write ACC to memory
            RAM_CHARS[M2_CMRAM][ SRC_RAM_X2[M2_CMRAM] ][ SRC_RAM_X3[M2_CMRAM] ] = DATA4_IN();
            if (outputDEBUG) 
            {
              sprintf(tmp, " WRM %01X-%01X-%01X D:%01X", M2_CMRAM, SRC_RAM_X2[M2_CMRAM], SRC_RAM_X3[M2_CMRAM], DATA4_IN());
              Serial.write(tmp);
            }           
            break;
            
          case 0b0001: // WMP - write ACC to RAM output port
            RAM_IO[M2_CMRAM][ SRC_RAM_X2[M2_CMRAM]>>2 ] = DATA4_IN();
            if (outputDEBUG) Serial.print(" WMP ");
            break;
            
          case 0b0010: // WRR - write ACC to ROM output port
            ROM_IO[ SRC_ROM_X2[M2_CMROM] ] = DATA4_IN();
            if (outputDEBUG) { Serial.print(" WRR ");Serial.print(X2_CHIPnREG, HEX);Serial.print(" "); }
            break;
            
          case 0b0011: // WPM - write ACC to executable RAM (4008/4009 only)
            if (1) 
            {
              Serial.println(" WPM NOT SUPPORTED YET !!! ");
              Serial.print  ("ADDR          : "); Serial.print(uP_ADDR, HEX); Serial.println("   ");
              Serial.print  ("M2_CMRAM      : "); Serial.print(M2_CMRAM, HEX); Serial.println("   ");
              Serial.print  ("SRC_X2_ChipReg: "); Serial.print(SRC_RAM_X2[M2_CMRAM], HEX); Serial.println("   ");
              Serial.print  ("SRC_X2_Char   : "); Serial.print(SRC_RAM_X3[M2_CMRAM], HEX); Serial.println("   ");
            }
            while(1);       // we should not see this w/ busicom
            break;
            
          case 0b0100: // WR0 - write ACC to RAM status 0
            RAM_STATUS[M2_CMRAM][ SRC_RAM_X2[M2_CMRAM] ][0] = DATA4_IN();
            if (outputDEBUG) 
            {
              sprintf(tmp, " WR0 %01X-%01X D:%01X", M2_CMRAM, SRC_RAM_X2[M2_CMRAM], DATA4_IN());
              Serial.write(tmp);
            }
            break;
            
          case 0b0101: // WR1 - write ACC to RAM status 1
            RAM_STATUS[M2_CMRAM][ SRC_RAM_X2[M2_CMRAM] ][1] = DATA4_IN();
            if (outputDEBUG) 
            {
              sprintf(tmp, " WR1 %01X-%01X D:%01X", M2_CMRAM, SRC_RAM_X2[M2_CMRAM], DATA4_IN());
              Serial.write(tmp);
            }
            break;
            
          case 0b0110: // WR2 - write ACC to RAM status 2
            RAM_STATUS[M2_CMRAM][ SRC_RAM_X2[M2_CMRAM] ][2] = DATA4_IN();
            if (outputDEBUG) 
            {
              sprintf(tmp, " WR2 %01X-%01X D:%01X", M2_CMRAM, SRC_RAM_X2[M2_CMRAM], DATA4_IN());
              Serial.write(tmp);              
            }
            break;
            
          case 0b0111: // WR3 - write ACC to RAM status 3
            RAM_STATUS[M2_CMRAM][ SRC_RAM_X2[M2_CMRAM] ][3] = DATA4_IN();
            if (outputDEBUG) 
            {
              sprintf(tmp, " WR3 %01X-%01X D:%01X", M2_CMRAM, SRC_RAM_X2[M2_CMRAM], DATA4_IN());
              Serial.write(tmp);              
            }
            break;
            
          default: // Error
            Serial.write("ERROR - unknown I/O cmd OPA\n"); 
            break;
        }
      }

      // If we are outputting DATA to 4004, wait for level shifter to stabilize.
      if (wait_for_data_stabilize)
      {
        while(!DATA4_OUT_matches_IN());
        wait_for_data_stabilize = false;
      }

      CLK2_HIGH();  
        DELAY_MEGA_TEENSY_NS(180,180);
        DRIVE_DBUS_STOP;

      next_cpu_state = CPU_X3;
      break;
 
    // ##################################################
    // ##################################################
    // ##################################################
    case CPU_X3:
      if (outputDEBUG) Serial.write("3");

      // digitalWriteFast(MEGA_PA7, HIGH); DELAY_MEGA_TEENSY_NS(60,60); digitalWriteFast(MEGA_PA7, LOW); DELAY_MEGA_TEENSY_NS(60,60);

      CLK1_LOW();   
      CLK1_HIGH();  

      CLK2_LOW();  
      DELAY_MEGA_TEENSY_NS(180,180);
      
      if (SRC_issued)
      {
        SRC_issued = false;

        X3_CHAR = DATA4_IN();
        SRC_RAM_X3[X2_CMRAM] = X3_CHAR;
        // ROM ignores X3.
          
        if (outputDEBUG) 
        {
          sprintf(tmp, " CHAR=%01X ", X3_CHAR );
          Serial.println(tmp);          
        }
      }
      
      if (!STATE_SYNC) 
      {
        // Error Missing SYNC
        Serial.write("MISSING SYNC\n");
      }
      else 
      {
        // Print SYNC Info
        if (outputDEBUG) Serial.write(" +SYNC\n");
      }

      CLK2_HIGH();  

      next_cpu_state = CPU_A1;
#if (outputDEBUG)
      count = 0;
#endif      
      break;
  }
    
  // ##################################################
  // ##################################################
  // ##################################################
  cpu_state = next_cpu_state;
}

////////////////////////////////////////////////////////////////////
// Setup
////////////////////////////////////////////////////////////////////
void setup() 
{

#if   (ARDUINO_AVR_MEGA2560)  
  Serial.begin(115200);
#elif (ARDUINO_TEENSY35 || ARDUINO_TEENSY36 || ARDUINO_TEENSY41) 
  Serial.begin(115200);
  while (!Serial);              // Wait for serial on Teensy.
#endif

  Serial.write("\n");
  Serial.write(27);       // ESC command
  Serial.print("[2J");    // clear screen command
  Serial.write(27);
  Serial.print("[H");
  Serial.println("\n");
  Serial.println("Configuration:");
  Serial.println("==============");
  print_teensy_version();  
  Serial.println("=======================================================");
  Serial.println("Retro Shield 4004 - Template");
  Serial.println("=======================================================");

  // Used to debug & setting delays accurately.
  // if (0)
  // {
  //   pinMode(MEGA_PA7, OUTPUT);
  //   while(0)
  //   {
  //     DELAY_UNIT_100NS();
  //     DELAY_UNIT_100NS();
  //     digitalWriteFast(MEGA_PA7, HIGH);
  //     DELAY_UNIT_100NS();
  //     DELAY_UNIT_100NS();
  //     digitalWriteFast(MEGA_PA7, LOW);
  //   }
  // }

  if (outputDEBUG_IO)
  {
    delay(1000);
    // Clear screen 
    Serial.write(27);       // ESC command
    Serial.print("[2J");    // clear screen command
    Serial.write(27);
    Serial.print("[H");    
  }
    
  // Initialize processor GPIO'
  uP_init();
  k4004_IO_init();
  // printer_init();
  Serial.flush();

  // Reset processor and sync
  Serial.println("Reseting CPU - for 8000 cycles.");
  uP_assert_reset();
  unsigned int wait_for_reset_complete = 60000;

  ////////////////////////////////////////////////////////////
  // Set operating frequency (Teensy 4.1/3.6/3.5)
  ////////////////////////////////////////////////////////////
  if (/*Teensy Speed*/ F_CPU > 500e6)   // 4004 Full speed if Teensy is running at 528/600MHz.
    set_clock_frequency_kHz(740000);    // 740kHz - 2% = 725kHz
  else
    set_clock_frequency_kHz(500000);    // Teensy 3.5 @120 MHz

  // 1) Due to component tolerances and level shifter delays on 
  // the board, 4004 may run a bit faster or slower than the 
  // above target frequency, hence I use 725kHz for some margin.
  // 2) Teensy 3.5 can't do faster than 500kHz.

  // Go, go, go
  next_edge_start();
  while(next_cpu_state != CPU_A1)
  {
    cpu_tick();

    wait_for_reset_complete--;
    if (wait_for_reset_complete == 0)
    {
      Serial.println("ERROR - Can not sync w/ CPU after 60,000 cycles");
      Serial.println("        check CPU and try again.");
      Serial.println();
      wait_for_reset_complete = 60000;
      delay(5000);
    }
  }
 
  // Go, go, go 
}

////////////////////////////////////////////////////////////////////
// Loop()
////////////////////////////////////////////////////////////////////
void loop()
{
  // Loop forever
  //
  while(1)
  {    
    cpu_tick();
  }
}
